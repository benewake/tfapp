#include "widgetcan.h"
#include "ui_widgetcan.h"
#include "canbusiothread.h"
#include <QMessageBox>
#include <QIntValidator>



WidgetCAN::WidgetCAN(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetCAN)
{
    ui->setupUi(this);

    QValidator *validator=new QIntValidator(0,10000,this);
    ui->etCANTxId->setValidator(validator);
    ui->etCANRxId->setValidator(validator);

}

WidgetCAN::~WidgetCAN()
{
    delete ui;
}

void WidgetCAN::SetCanbusIo(CanBusIoThread *tCanBusIoThread, QString productModal)
{
     this->mCanbusThread = tCanBusIoThread;
     this->mProductModal = productModal;

//    if(this->mProductModal=="TFmini-i"||this->mProductModal=="TF02-i")
//    {
//        ui->btnFrameType->setText(tr(u8"设置"));
//    }

}

uint WidgetCAN::GetCanTxId()
{
	return ui->etCANTxId->text().toUInt();
}



bool WidgetCAN::CheckComState()
{
    if(mCanbusThread==nullptr)
        return false;

    if(!mCanbusThread->isOpen())
    {
        QMessageBox::warning(NULL, "Error", tr(u8"未连接CAN"));
        return false;
    }

    return true;
}



void WidgetCAN::on_btnFrameFreq_clicked()
{
    if(!CheckComState()) return;

    if(mCanbusThread->canSetFrameFreq(ui->cbFrameFreq->currentText().toUShort()))
    {
       qDebug("帧率设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"帧率设定成功"));

    }else{
        qDebug("帧率设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"帧率设定失败"));
    }
//    bool ret = canbusThread->OpenCAN();

//    if(ret){
//        canbusThread->start();
//    }
}

void WidgetCAN::on_btnOutSwitch_clicked()
{
    if(!CheckComState()) return;

   int index = ui->cbOutputSwitch->currentIndex();
   bool switchOn;
   if(index==0){
       switchOn=true;
   }else {
       switchOn=false;
    }

    if(mCanbusThread->canOutputSwitch(switchOn))
    {
       qDebug("设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"输出启/禁用成功！"));
    }
    else
    {
       qDebug("设定失败");
       QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"输出启/禁用失败！"));
    }

}



void WidgetCAN::on_btnVersion_clicked()
{
    if(!CheckComState()) return;
     bool result;
     QString  s_version= mCanbusThread->canGetVersion(result);
     if(false == result)
     {
          ui->lbVersion->setText(tr(u8"版本号获取失败"));
     }else{
          ui->lbVersion->setText(s_version);
     }


}

void WidgetCAN::on_btnSaveConfig_clicked()
{
    if(!CheckComState()) return;

    QString cont;
    cont.append(tr(u8"保存配置"));
    ui->lbConfigResult->setText("");
    if(mCanbusThread->canSaveConfig())
    {
        ui->lbConfigResult->setText(tr(u8"配置保存成功！"));
    }else{
       ui->lbConfigResult->setText(tr(u8"配置保存失败！"));
    }

}

void WidgetCAN::on_btnSystemReset_clicked()
{
    if(!CheckComState()) return;
    QString cont;
    cont.append(tr(u8"复位"));
    QMessageBox  msgBox;
   if(mCanbusThread->canSystemReset())
   {
      qDebug("复位成功！");
      //QMessageBox::information(NULL, tr(u8"提示"), tr(u8"复位成功！"));
      ui->lbResetResult->setText(tr(u8"复位成功！"));


   }else{
      qDebug("复位失败");
      //QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"复位失败"));
       ui->lbResetResult->setText(tr(u8"复位失败！"));
   }
}

void WidgetCAN::on_btnFactoryReset_clicked()
{
    if(!CheckComState()) return;
     if(mCanbusThread->canFactoryReset())
     {
         ui->lbFactoryResetResult->setText(tr(u8"恢复出厂成功"));
     }else{
         ui->lbFactoryResetResult->setText(tr(u8"恢复出厂失败"));
     }
}



void WidgetCAN::on_btnCANParamSet_clicked()
{


    uint frameType = ui->cbFrameTyp->currentIndex();
    uint baudrate = ui->cbBaudrate->currentText().toInt();

    if(ui->etCANTxId->text()==""){

        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN发送ID是必填项"));
        return;
    }

    if(ui->etCANRxId->text()==""){

        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN接收ID是必填项"));
        return;
    }

    ulong txId = ui->etCANTxId->text().toULong();
    ulong rxId = ui->etCANRxId->text().toULong();

    if(!CheckComState()) return;

    if(mProductModal=="TF03")
    {
		bool bResult = false;
		bResult = mCanbusThread->canSetFrameType(frameType);

		if (!bResult) {

			QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN参数设置失败"));
			return;
		}

		bResult = mCanbusThread->canBaudrate(baudrate);

		if (!bResult) {

			QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN参数设置失败"));
			return;
		}


		bResult = mCanbusThread->canSetTxID(txId);

		if (!bResult) {

			QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN参数设置失败"));
			return;
		}

		bResult = mCanbusThread->canSetRxID(rxId);
		if (!bResult) {

			QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN参数设置失败"));
			return;
		}

		else
		{
			QMessageBox::information(NULL, tr(u8"提示"), tr(u8"CAN参数设置成功"));
		}
    }
    else 
	{
		bool bResult = mCanbusThread->canSetParam(frameType,baudrate,rxId,txId);

		if (!bResult) {

			QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"CAN参数设置失败"));
			return;
		}

		else
		{
			QMessageBox::information(NULL, tr(u8"提示"), tr(u8"CAN参数设置成功"));
		}
    }
}

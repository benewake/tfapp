#include "serialportio.h"

/* CRC 高位字节值表 */
const uint8_t auch_crc_high[CRC16_TABLE_LENGTH] = {
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
    0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
} ;

/* CRC 低位字节值表*/
const uint8_t auch_crc_low[CRC16_TABLE_LENGTH] = {
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
    0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
    0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
    0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
    0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
    0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
    0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
    0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
    0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
    0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
    0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
    0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
    0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
    0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
    0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
    0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
    0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
    0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
    0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
    0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
    0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
    0x43, 0x83, 0x41, 0x81, 0x80, 0x40
} ;



serialportio::serialportio(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QVector<uint16_t>>("QVector<uint16_t>");
    serialPort_ = new QSerialPort();
    timer_ = new QTimer();

    char header[2] = {0x59, 0x59};
    dataHeader_ = QByteArray(header, 2);

	RxFrameType = 0;

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    connect(this, SIGNAL(signalOpen(QString,int,bool&)), this, SLOT(slotOpen(QString,int,bool&)),
            Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalClose(bool&)), this, SLOT(slotClose(bool&)), Qt::BlockingQueuedConnection);

    //一个定时器算帧率
    connect(timer_, SIGNAL(timeout()), this, SLOT(slotCalcFreq()));

    connect(this, SIGNAL(signalReset(bool &)), this, SLOT(slot_do_cmd_id_soft_reset(bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalResetPro(bool &)), this, SLOT(slot_do_cmd_id_soft_reset_pro(bool &)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalIICReset(uint , bool &)), this, SLOT(slot_iic_soft_reset(uint,bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIICResetPro(uint , bool &)), this, SLOT(slot_iic_soft_reset_pro(uint,bool &)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signalVersion(unsigned char *,bool &)), this, SLOT(slot_do_cmd_id_version(unsigned char *,bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalVersionPro(unsigned char *,bool &)), this, SLOT(slot_do_cmd_id_version_pro(unsigned char *,bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalFullVersion(unsigned char *,bool &)), this, SLOT(slot_do_cmd_id_full_version(unsigned char *,bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalFrameRate(unsigned int,bool &)), this, SLOT(slot_do_cmd_id_frame_rate(unsigned int,bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalFrameRatePro(unsigned int,bool &)), this, SLOT(slot_do_cmd_id_frame_rate_pro(unsigned int,bool &)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalIICFrameRate(uint,unsigned int,bool &)), this, SLOT(slot_iic_frame_rate(uint,unsigned int,bool &)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIICFrameRatePro(uint,unsigned int,bool &)), this, SLOT(slot_iic_frame_rate_pro(uint,unsigned int,bool &)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signalSaveSetting(bool&)), this, SLOT(slot_do_cmd_id_save_settings(bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalSaveSettingPro(bool&)), this, SLOT(slot_do_cmd_id_save_settings_pro(bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalIICSaveSetting(uint,bool&)), this, SLOT(slot_iic_save_settings(uint,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIICSaveSettingPro(uint,bool&)), this, SLOT(slot_iic_save_settings_pro(uint,bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalBaudrate(unsigned int,bool&)), this, SLOT(slot_do_cmd_id_baud_rate(unsigned int,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalBaudratePro(unsigned int,bool&)), this, SLOT(slot_do_cmd_id_baud_rate_pro(unsigned int,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalCanBaudrate(unsigned int,bool&)), this, SLOT(slot_do_cmd_id_can_baudrate(unsigned int,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalCANFrameType(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_can_frame_type_config(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalCanRcvId(unsigned int,bool&)), this, SLOT(slot_do_cmd_id_can_rcv_id(unsigned int,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalCanSndId(unsigned int,bool&)), this, SLOT(slot_do_cmd_id_can_snd_id(unsigned int,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalChecksumEnabled(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_frame_checksum_en(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalCustom(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_Custom_choise(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalFogFeatureEnabled(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_fog_feature_enable(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalMapTypeSet(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_dac_map_type(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalGainAdjustEnabled(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_gain_adjust_enable(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIODelay(unsigned short,unsigned short,bool&)), this, SLOT(slot_do_cmd_id_io_delay(unsigned short,unsigned short,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIOThreshold(unsigned short,unsigned short,bool&)), this, SLOT(slot_do_cmd_id_dist_io_threshold(unsigned short,unsigned short,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIOValue(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_near_io_value(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalOffset(unsigned short,bool&)), this, SLOT(slot_do_cmd_id_offset_config(unsigned short,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalOutputEnabled(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_output_en(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalOutputEnabledPro(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_output_en_pro(unsigned char,bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalOutputFormat(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_output_format(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalOutputFormatPro(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_output_format_pro(unsigned char,bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalIICOutputFormat(uint,unsigned char,bool&)), this, SLOT(slot_iic_output_format(uint,unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIICOutputFormatPro(uint,unsigned char,bool&)), this, SLOT(slot_iic_output_format_pro(uint,unsigned char,bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalRestoreFactory(bool&)), this, SLOT(slot_do_cmd_id_restore_default(bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalRestoreFactoryPro(bool&)), this, SLOT(slot_do_cmd_id_restore_default_pro(bool&)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signalIICRestoreFactory(uint,bool&)), this, SLOT(slot_iic_restore_default(uint,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalIICRestoreFactoryPro(uint,bool&)), this, SLOT(slot_iic_restore_default_pro(uint,bool&)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signalSampleTrig(unsigned short&,bool&)), this, SLOT(slot_do_cmd_id_sample_trig(unsigned short&,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalTransType(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_trans_type_config(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalUpgrade(const QString, unsigned int)), this, SLOT(slot_do_cmd_id_upgrade(const QString, unsigned int)), Qt::AutoConnection);
    connect(this, SIGNAL(signal_modbus_enable(bool&)), this, SLOT(slot_do_cmd_id_modbus_enable(bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_modbus_dev_addr_set(unsigned char,bool&)), this, SLOT(slot_do_cmd_id_modbus_dev_addr_set(unsigned char,bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_mb_baudrage_set(unsigned char, unsigned int, bool&)), this, SLOT(slot_do_cmd_id_mb_baudrate_config(unsigned char, unsigned int, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_mb_dev_addr_set(unsigned char, unsigned char, bool&)), this, SLOT(slot_do_cmd_id_mb_dev_addr_config(unsigned char, unsigned char, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_mb_get_dist(unsigned char, unsigned short&, bool&)), this, SLOT(slot_do_cmd_id_mb_get_dist(unsigned char, unsigned short&, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_mb_modbus_disable(unsigned char, bool&)), this, SLOT(slot_do_cmd_id_mb_modbus_disable(unsigned char, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_mb_save_config(unsigned char, bool&)), this, SLOT(slot_do_cmd_id_mb_save_config(unsigned char, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_mb_get_signalquality(unsigned char, unsigned short&, bool&)), this, SLOT(slot_do_cmd_id_mb_get_signal(unsigned char, unsigned short&, bool&)), Qt::BlockingQueuedConnection);

    //new add
    connect(this, SIGNAL(signalLowConsimption(uint, bool&)), this, SLOT(slot_do_cmd_id_lowconsumption(uint, bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalIICLowConsimption(uint,uint, bool&)), this, SLOT(slot_iic_lowconsumption(uint,uint, bool&)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signal_getdistancemm(unsigned short&, bool&)), this, SLOT(slot_iic_querydistancecm(unsigned short&, bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signal_iic_querydistancecm(uint,bool&)), this, SLOT(slot_iic_querydistancecm(uint,bool&)), Qt::BlockingQueuedConnection);

     connect(this, SIGNAL(signal_iic_querydistancecm_pro(uint,bool&)), this, SLOT(slot_iic_querydistancecm_pro(uint,bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signal_iic_getdistancecm(uint,unsigned short&, bool&)), this, SLOT(slot_iic_getdistcm(uint,unsigned short&, bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signal_getdistancecm_pro(unsigned short&, bool&)), this, SLOT(slot_iic_getdist_9bytecm_pro(unsigned short&, bool&)), Qt::BlockingQueuedConnection);

     connect(this, SIGNAL(signal_iic_getdistancecm_pro(uint,unsigned short&, bool&)), this, SLOT(slot_iic_getdist_9bytecm_pro(uint,unsigned short&, bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signal_setiicaddr(uint, uint,bool&)), this, SLOT(slot_do_cmd_iic_setaddr(uint,uint, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_setiicaddr_pro(uint,uint, bool&)), this, SLOT(slot_iic_setaddr_pro(uint,uint, bool&)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signal_uart_setio(uint,uint,uint,uint,uint, bool&)), this, SLOT(slot_do_cmd_uart_setio(uint, uint, uint, uint, uint, bool&)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalSetWiperPeriodPro(unsigned int,bool &)), this, SLOT(slot_setwiperperiod_pro(unsigned int,bool &)), Qt::BlockingQueuedConnection);

	connect(this, SIGNAL(signalModbusSetWiperPeriodPro(uint, unsigned int, bool &)), this, SLOT(slot_setwiperperiod_modbus(uint,unsigned int, bool &)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalSetWiperCyclePro(unsigned int,bool &)), this, SLOT(slot_setwipercycle_pro(unsigned int,bool &)), Qt::BlockingQueuedConnection);

	connect(this, SIGNAL(signalModbusSetWiperCycle(uint,uint, bool &)), this, SLOT(slot_setwipercycle_modbus(uint,uint, bool &)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signalStartWiperPro(bool &)), this, SLOT(slot_startwiper_pro(bool &)), Qt::BlockingQueuedConnection);

	connect(this, SIGNAL(signalModbusStartWiperPro(uint,bool &)), this, SLOT(slot_startwiper_modbus(uint,bool &)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signalCanParamTFmini(int ,int ,long ,long , bool &)), this, SLOT(slot_can_paramTFmini(int ,int ,long ,long , bool &)), Qt::BlockingQueuedConnection);


    connect(this, SIGNAL(signal_uart_sendoneframe(QByteArray , bool &)), this, SLOT(slot_uart_sendoneframe(QByteArray , bool &)), Qt::BlockingQueuedConnection);

    connect(this, SIGNAL(signal_mb_getdistandsigal(uint, bool &)), this, SLOT(slot_mb_get_distandsignal(uint, bool &)), Qt::BlockingQueuedConnection);

     connect(this, SIGNAL(signal_analog_modeswitch(uint, bool &)), this, SLOT(slot_analog_modeswitch(uint, bool &)), Qt::BlockingQueuedConnection);
     connect(this, SIGNAL(signal_analog_setdistmin(uint, bool &)), this, SLOT(slot_analog_setdistmin(uint, bool &)), Qt::BlockingQueuedConnection);
     connect(this, SIGNAL(signal_analog_setdistmax(uint, bool &)), this, SLOT(slot_analog_setdistmax(uint, bool &)), Qt::BlockingQueuedConnection);

    max_dist_num = 10;
    freq_ = 0;
}

serialportio::~serialportio()
{

}

bool serialportio::open(const QString _serialSelected, int _baudRate)
{
    bool result;
    emit signalOpen(_serialSelected, _baudRate, result);
    return result;
}

void serialportio::run()
{
    emit signalRun();
}

bool serialportio::close()
{
    bool result;
    emit signalClose(result);
    return result;
}

bool serialportio::isOpen()
{
    return serialPort_->isOpen();
}

bool serialportio::send_cmd_id_soft_reset()
{
    bool result;
    emit signalReset(result);
    return result;
}

bool serialportio::send_iic_soft_reset(uint iicAddress)
{
    bool result;
    emit signalIICReset(iicAddress,result);
    return result;
}

bool serialportio::send_cmd_id_soft_reset_pro()
{
    bool result;
    emit signalResetPro(result);
    return result;
}

bool serialportio::send_cmd_id_sample_trig(unsigned short &dist)
{
    bool result;
    emit signalSampleTrig(dist, result);
    return result;
}

bool serialportio::send_cmd_id_output_format(E_OutputFormat_Type format)
{
    bool result;
    emit signalOutputFormat((unsigned char)format, result);
	//RxFrameType = 0;
	//if (format = E_OutputFormat_Type::FORMAT_ASCII) 
	//{
	//	if (result) {
	//		RxFrameType = 1;
	//	}
	//}
    return result;
}

bool serialportio::send_iic_output_format(uint iicAddress, E_OutputFormat_Type format)
{
    bool result;
    emit signalIICOutputFormat(iicAddress,(unsigned char)format, result);
    return result;
}

bool serialportio::send_iic_output_format_pro(uint iicAddress, E_OutputFormat_Type format)
{
    bool result;
    emit signalIICOutputFormatPro(iicAddress,(unsigned char)format, result);
    return result;
}

bool serialportio::send_cmd_id_output_format_pro(E_OutputFormat_Type format)
{
    bool result;
    emit signalOutputFormatPro((unsigned char)format, result);
    return result;
}

bool serialportio::send_cmd_id_baud_rate(unsigned int baudrate)
{
    bool result;
    emit signalBaudrate(baudrate, result);
//    if(result)
//    {
//       emit signalBaudrateChanged();
//    }
    return result;
}

bool serialportio::send_cmd_id_baud_rate_pro(unsigned int baudrate)
{
    bool result;
    emit signalBaudratePro(baudrate, result);
    if(result)
    {
       emit signalBaudrateChanged();
    }
    return result;
}

bool serialportio::send_cmd_id_output_en(E_Output_Enable_Type is_enabled)
{
    bool result;
    emit signalOutputEnabled((unsigned char)is_enabled, result);
    return result;
}

bool serialportio::send_cmd_id_output_en_pro(E_Output_Enable_Type is_enabled)
{
    bool result;
    emit signalOutputEnabledPro((unsigned char)is_enabled, result);
    return result;
}

bool serialportio::send_cmd_low_consumption(uint sampleRate)
{
    bool result;
    emit signalLowConsimption(sampleRate, result);
    return result;
}

bool serialportio::send_iic_low_consumption(uint iicAddress, uint sampleRate)
{
    bool result;
    emit signalIICLowConsimption(iicAddress,sampleRate, result);
    return result;
}

bool serialportio::send_cmd_id_modbus_getsignal(unsigned char dev_addr, unsigned short& signal)
{
    bool result;
    emit signal_mb_get_signalquality(dev_addr, signal, result);
    return result;
}

bool serialportio::send_cmd_id_getdistancemm(unsigned short& distance)
{
    bool result;
    emit signal_getdistancemm(distance, result);
    return result;
}

bool serialportio::send_iic_querydistancecm(uint iicAddress)
{
    bool result;
    emit signal_iic_querydistancecm(iicAddress,result);
    return result;
}

bool serialportio::send_iic_querydistance_pro(uint iicAddress)
{
    bool result;
    emit signal_iic_querydistancecm_pro(iicAddress,result);
    return result;
}

bool serialportio::send_iic_getdistancecm(uint iicAddress, unsigned short &distance)
{
    bool result;
    emit signal_iic_getdistancecm(iicAddress,distance, result);
    return result;
}


bool serialportio::send_cmd_id_getdistancecm_pro(unsigned short &distance)
{
    bool result;
    emit signal_getdistancecm_pro(distance, result);
    return result;
}

bool serialportio::send_iic_getdistancecm_pro(uint iicAddr, unsigned short &distance)
{
    bool result;
    emit signal_iic_getdistancecm_pro(iicAddr,distance, result);
    return result;
}

bool serialportio::send_iic_setaddr(uint iicAddr,uint iicNewaddr)
{
    bool result;
    emit signal_setiicaddr(iicAddr,iicNewaddr, result);
    return result;
}

bool serialportio::send_iic_setaddr_pro(uint iicaddr, uint iicNewAddr)
{
    bool result;
    emit signal_setiicaddr_pro(iicaddr,iicNewAddr, result);
    return result;
}

bool serialportio::send_cmd_id_frame_checksum_en(E_CheckSum_Type is_enabled)
{
    bool result;
    emit signalChecksumEnabled((unsigned char)is_enabled, result);
    return result;
}

bool serialportio::send_cmd_id_restore_default()
{
    bool result;
    emit signalRestoreFactory(result);
    return result;
}

bool serialportio::send_iic_restore_default(uint iicAddress)
{
    bool result;
    emit signalIICRestoreFactory(iicAddress,result);
    return result;
}

bool serialportio::send_cmd_id_restore_default_pro()
{
    bool result;
    emit signalRestoreFactoryPro(result);
    return result;
}

bool serialportio::send_cmd_id_uart_setio(uint ioMode, uint distVal, uint zoneVal, uint delayVal1, uint delayVal2)
{
    bool result;
    emit signal_uart_setio(ioMode,distVal,zoneVal,delayVal1,delayVal2,result);
    return result;
}

bool serialportio::send_cmd_id_save_settings()
{
    bool result;
    emit signalSaveSetting(result);
    return result;
}

bool serialportio::send_iic_save_settings(uint iicAddress)
{
    bool result;
    emit signalIICSaveSetting(iicAddress,result);
    return result;
}

bool serialportio::send_cmd_id_save_settings_pro()
{
    bool result;
    emit signalSaveSettingPro(result);
    return result;
}

bool serialportio::send_iic_save_settings_pro(uint iicAddr)
{
    bool result;
    emit signalIICSaveSettingPro(iicAddr,result);
    return result;
}

bool serialportio::send_cmd_id_trans_type_config(E_Trans_Type trans)
{
    bool result;
    emit signalTransType((unsigned char)trans, result);
    return result;
}

bool serialportio::send_cmd_id_gain_adjust_enable(E_GAIN_AJUST_Type enable)
{
    bool result;
    emit signalGainAdjustEnabled((unsigned char)enable, result);
    return result;
}

bool serialportio::send_cmd_id_can_snd_id(uint32_t value)
{
    bool result;
    emit signalCanSndId(value, result);
    return result;
}

bool serialportio::send_cmd_id_can_param_tfmini(int frameType,int baudRate,long canTxId,long canRxId)
{
    bool result;
    emit signalCanParamTFmini(frameType, baudRate, canTxId, canRxId, result);
    return result;
}

bool serialportio::send_uart_oneframe(QByteArray cmd)
{
    bool result;
    emit signal_uart_sendoneframe(cmd, result);
    return result;
}

bool serialportio::send_mb_distandsignal(uint devAddr)
{
     bool result;
     emit signal_mb_getdistandsigal(devAddr,result);
     return result;

}

bool serialportio::send_analog_modeswitch(uint mode)
{
    bool result;
    emit signal_analog_modeswitch(mode, result);
    return result;
}

bool serialportio::send_analog_setdistmin(uint mindist)
{
    bool result;
    emit signal_analog_setdistmin(mindist, result);
    return result;
}

bool serialportio::send_analog_setdistmax(uint maxdist)
{
    bool result;
    emit signal_analog_setdistmax(maxdist, result);
    return result;
}

bool serialportio::send_cmd_id_can_rcv_id(uint32_t value)
{
    bool result;
    emit signalCanRcvId(value, result);
    return result;
}

bool serialportio::send_cmd_id_can_baudrate(uint32_t value)
{
    bool result;
    emit signalCanBaudrate(value, result);
    return result;
}

QString serialportio::send_cmd_id_version(bool &result)
{
    QString s_version;
    s_version.clear();
    unsigned char version[3];
    emit signalVersion(version, result);
    s_version = QString::number(version[0]) + '.' + QString::number(version[1]) + '.' + QString::number(version[2]);
    return s_version;
}

QString serialportio::send_cmd_id_version_pro(bool &result)
{
    QString s_version;
    s_version.clear();
    unsigned char version[3];
    emit signalVersionPro(version, result);
    s_version = QString::number(version[0]) + '.' + QString::number(version[1]) + '.' + QString::number(version[2]);
    return s_version;
}

QString serialportio::send_cmd_id_full_version(bool &result)
{
    QString s_version;
    s_version.clear();
    unsigned char version[30] = {0};
    emit signalFullVersion(version, result);
    s_version.clear();

    s_version = QString((char *)version);
    return s_version;
}

bool serialportio::send_cmd_id_frame_rate(uint16_t fps)
{
    bool result;
    emit signalFrameRate(fps, result);
    return result;
}

bool serialportio::send_iic_frame_rate(uint iicAddress,uint16_t fps)
{
    bool result;
    emit signalIICFrameRate(iicAddress,fps, result);
    return result;
}


bool serialportio::send_cmd_id_frame_rate_pro(uint16_t fps)
{
    bool result;
    emit signalFrameRatePro(fps, result);
    return result;
}

bool serialportio::send_iic_frame_rate_pro(uint iicAddr, uint16_t fps)
{
    bool result;
    emit signalIICFrameRatePro(iicAddr,fps, result);
    return result;
}


bool serialportio::send_cmd_id_set_wiper_peroid(uint16_t periodMinutes)
{
    bool result;
    emit signalSetWiperPeriodPro(periodMinutes, result);
    return result;
}

bool serialportio::send_mobus_set_wiper_peroid(uint addr, uint periodMinutes)
{
	bool result;
	emit signalModbusSetWiperPeriodPro(addr,periodMinutes, result);
	return result;
}

bool serialportio::send_cmd_set_wipercycle(uint16_t cycleCnt)
{
    bool result;
    emit signalSetWiperCyclePro(cycleCnt, result);
    return result;
}

bool serialportio::send_modbus_set_wipercycle(uint addr, uint cycleCnt)
{
	bool result;
	emit signalModbusSetWiperCycle(addr, cycleCnt, result);
	return result;
}

bool serialportio::send_cmd_startwiper()
{
    bool result;
    emit signalStartWiperPro(result);
    return result;
}


bool serialportio::send_modbus_startwiper(uint addr)
{
	bool result;
	emit signalModbusStartWiperPro(addr,result);
	return result;
}

bool serialportio::send_cmd_id_Custom_choise(E_Custom_Type custom)
{
    bool result;
    emit signalCustom((unsigned char)custom, result);
    return result;
}

bool serialportio::send_cmd_id_can_frame_type_config(E_CAN_FRAME_Type type)
{
    bool result;
    emit signalCANFrameType((unsigned char)type, result);
    return result;
}

bool serialportio::send_cmd_id_dist_io_threshold(uint16_t value1, uint16_t value2)
{
    bool result;
    emit signalIOThreshold(value1, value2, result);
    return result;
}

bool serialportio::send_cmd_id_near_io_value(GPIO_PinState value)
{
    bool result;
    emit signalIOValue((unsigned char)value, result);
    return result;
}

bool serialportio::send_cmd_id_io_delay(uint16_t value1, uint16_t value2)
{
    bool result;
    emit signalIODelay(value1, value2, result);
    return result;
}

bool serialportio::send_cmd_id_fog_feature_enable(E_ENABLE_Type enable)
{
    bool result;
    emit signalFogFeatureEnabled((unsigned char)enable, result);
    return result;
}

bool serialportio::send_cmd_id_dac_map_type(E_MAP_Type type)
{
    bool result;
    emit signalMapTypeSet((unsigned char)type, result);
    return result;
}

bool serialportio::send_cmd_id_offset_config(uint16_t value)
{
    bool result;
    emit signalOffset(value, result);
    return result;
}

bool serialportio::send_cmd_id_modbus_enable()
{
    bool result;
    emit signal_modbus_enable(result);
    return result;
}

bool serialportio::send_cmd_id_modbus_dev_addr_set(unsigned char modbus_dev_addr)
{
    bool result;
    emit signal_modbus_dev_addr_set(modbus_dev_addr, result);
    return result;
}

/**Modbus设置波特率
 * @brief Modbus设置波特率
 * @param dev_addr
 * @param baudrate
 * @return
 */

bool serialportio::send_cmd_id_mb_baudrate_config(unsigned char dev_addr, unsigned int baudrate)
{
    bool result;
    emit signal_mb_baudrage_set(dev_addr, baudrate, result);
    return result;
}

/**设置Modbus地址
 * @brief 设置Modbus地址
 * @param dev_addr
 * @param mb_dev_addr
 * @return
 */

bool serialportio::send_cmd_id_mb_dev_addr_config(unsigned char dev_addr, unsigned int mb_dev_addr)
{
    bool result;
    emit signal_mb_dev_addr_set(dev_addr, mb_dev_addr, result);
    return result;
}

bool serialportio::send_cmd_id_mb_get_dist(unsigned char dev_addr, unsigned short &dist)
{
    bool result;
    emit signal_mb_get_dist(dev_addr, dist, result);
    return result;
}

/**关闭Modbus协议
 * @brief receiver::send_cmd_id_mb_modbus_disable
 * @param dev_addr
 * @return
 */
bool serialportio::send_cmd_id_mb_modbus_disable(unsigned char dev_addr)
{
    bool result;
    emit signal_mb_modbus_disable(dev_addr, result);
    return result;
}

bool serialportio::send_cmd_id_mb_save_config(unsigned char dev_addr)
{
    bool result;
    emit signal_mb_save_config(dev_addr, result);
    return result;
}



/**设置帧率 modbus协议
 * @brief receiver::send_cmd_id_modbus_set_fps
 * @param dev_addr
 * @param fps
 * @return
 */
bool serialportio::send_cmd_id_modbus_set_fps(unsigned char dev_addr, int fps)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    bool result;
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x86;
    cmd[4] = (fps >> 8) & 0xFF;
    cmd[5] = fps & 0xFF;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head(cmd);

    qDebug() << cmd.toHex() << ack_head.toHex();

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    return result;
}

bool serialportio::send_cmd_id_modbus_get_version(unsigned char dev_addr, QString &ver)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    bool result;
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x03;
    cmd[2] = 0x00;
    cmd[3] = 0x06;
    cmd[4] = 0x00;
    cmd[5] = 0x02;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    QByteArray ba_read;
    QByteArray all_read;
    int32_t index;

    ack_head.resize(3);
    ack_head[0] = dev_addr;
    ack_head[1] = 0x03;
    ack_head[2] = 0x04;

    serialPortWrite(cmd);

    while(1)
    {
        serialPort_->waitForReadyRead(2000);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        if(2 <= cnt++)
        {
            break;
        }
        all_read.append(ba_read);

        if(all_read.length() < 9)
        {
            continue;
        }

        index = all_read.indexOf(ack_head);

        if(0 > index)
        {
            continue;
        }

        if(all_read.right(all_read.length() - index).length() < 9)
        {
            all_read = all_read.right(all_read.length() - index);
            continue;
        }
        all_read = all_read.mid(index, 9);

        if(crc16_calc(all_read.left(7), 7) != (uint16_t)((unsigned char)all_read.at(7) + (unsigned short)(all_read.at(8) << 8)))
        {
            qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(7), 7) << ", ack:" << (uint16_t)((unsigned char)all_read.at(7) + (unsigned short)(all_read.at(8) << 8));
            cnt = 2;
            break;
        }

        ver = QString::number((uint8_t)all_read.at(4)) + "." + QString::number((uint8_t)all_read.at(5)) + "." +QString::number((uint8_t)all_read.at(6));
        qDebug() << "verion: " << ver;
        break;
    }

    if(cnt >= 2)
    {
        result = false;
    }else{
        result = true;

    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    return result;
}

bool serialportio::send_cmd_id_modbus_shutdown(unsigned char dev_addr)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    bool result;
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x81;
    cmd[4] = 0x00;
    cmd[5] = 0x00;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head(cmd);

    qDebug() << cmd.toHex() << ack_head.toHex();

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    return result;
}

bool serialportio::send_cmd_id_modbus_reboot(unsigned char dev_addr)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    bool result;
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x81;
    cmd[4] = 0x00;
    cmd[5] = 0x01;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head(cmd);

    qDebug() << cmd.toHex() << ack_head.toHex();

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    return result;
}

void serialportio::upgrade(const QString fullpath, unsigned int upgrade_frame_length)
{
    qDebug() << "receiver" << __FUNCTION__ << QThread::currentThreadId();
    emit signalUpgrade(fullpath, upgrade_frame_length);
}

void serialportio::slot_do_cmd_id_lowconsumption(uint sampleRate,bool& result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(6);
    cmd[0] = 0x5A;
    cmd[1] = 6;
    cmd[2] = ID_LOW_CONSUMPTION;
    cmd[3] = sampleRate & 0xFF;
    cmd[4] = (sampleRate >> 8) & 0xFF;
    cmd[5] = calc_checksum(cmd);

    int cnt = 0;
    result = false;
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(cmd);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_iic_lowconsumption(uint iicAddress, uint sampleRate,bool& result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    QByteArray cmd;

    cmd.resize(13);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x06;

    cmd[0] = 0x5A;
    cmd[1] = 0x06;
    cmd[2] = 0x35;
    cmd[3] = sampleRate & 0xFF;
    cmd[4] = (sampleRate >> 8) & 0xFF;
    cmd[5] = calc_checksum(cmd);

    cmd[8] = 0x50;


    cmd[9] = 0x53;
    cmd[10] = iicAddress *2 +1;
    cmd[11] = 0x06;
    cmd[12] = 0x50;



    int cnt = 0;
    result = false;



    qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');


    result = false;
    QByteArray ack_head;
    ack_head.resize(6);

    ack_head[0] = 0x5A;
    ack_head[1] = 0x06;
    ack_head[2] = 0x35;
    ack_head[3] = sampleRate & 0xFF;
    ack_head[4] = (sampleRate >> 8) & 0xFF;
    ack_head[5] = calc_checksum(cmd);

    while((false == result) && (cnt < 2))
    {
        int nSend = serialPortWrite(cmd);
        if(nSend>0)
        {
            result = true;
            break;
        }

        //result = read_ack(ack_head);
        cnt++;
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_mb_get_signal(unsigned char dev_addr, unsigned short &signal, bool &result)
{

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x03;
    cmd[2] = 0x00;
    cmd[3] = 0x00;
    cmd[4] = 0x00;
    cmd[5] = 0x02;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    QByteArray ba_read;
    QByteArray all_read;
    int32_t index;

    ack_head.resize(3);
    ack_head[0] = dev_addr;
    ack_head[1] = 0x03;
    ack_head[2] = 0x04;

    serialPortWrite(cmd);

    while(1)
    {
        serialPort_->waitForReadyRead(2000);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        if(2 <= cnt++)
        {
            break;
        }
        all_read.append(ba_read);

        if(all_read.length() < 9)
        {
            continue;
        }

        index = all_read.indexOf(ack_head);

        if(0 > index)
        {
            continue;
        }

        if(all_read.right(all_read.length() - index).length() < 9)
        {
            all_read = all_read.right(all_read.length() - index);
            continue;
        }
        all_read = all_read.mid(index, 9);

        if(crc16_calc(all_read.left(7), 7) != (uint16_t)((unsigned char)all_read.at(7) + (unsigned short)(all_read.at(8) << 8)))
        {
            qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(5), 5) << ", ack:" << (uint16_t)((unsigned char)all_read.at(5) + (unsigned short)(all_read.at(6) << 8));
            cnt = 2;
            break;
        }


        break;
    }

    if(cnt >= 2)
    {
        result = false;
    }else{
        result = true;

        signal = ((uint16_t)all_read.at(6) & 0x00ff | ((uint16_t)all_read.at(5) << 8) & 0xff00);
        qDebug() << "signal: " << signal;

        emit signalDataRcv(all_read);

    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

}

/**设置IIC地址
 * @brief receiver::slot_do_cmd_iic_setaddr
 * @param iic_addr
 * @param result
 */
void serialportio::slot_do_cmd_iic_setaddr(uint iicAddress, uint iicNewAddress, bool &result)
{

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));



    uint16_t crc;
    QByteArray cmd;

    cmd.resize(13);

    cmd[0] = 0x53;
    cmd[1] = iicAddress*2;
    cmd[2] = 0x05;

    cmd[3] = 0x5A;
    cmd[4] = 0x05;
    cmd[5] = 0x0B;
    cmd[6] = iicNewAddress;
    cmd[7] = calc_checksum(cmd);

    cmd[8] = 0x50;

    cmd[9] =  0x53;
    cmd[10] = iicAddress*2+1;
    cmd[11] = 0x05;
    cmd[12] = 0x50;

    result =false;
    int nSend = serialPortWrite(cmd);

    if(nSend>0){

        result = true;
    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

}


void serialportio::slot_can_paramTFmini(int frameType,int baudRate,long canTxId,long canRxId, bool & result)
{

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(13);
    cmd[0] = 0x5A;
    cmd[1] = 0x0E;
    cmd[2] = 0x51;
    cmd[3] = frameType;
    cmd[4] = baudRate;

    cmd[5] = canRxId>>24&0xFF;
    cmd[6] = canRxId>>16&0xFF;
    cmd[7] = canRxId>>8&0xFF;
    cmd[8] = canRxId&0xFF;

    cmd[9] = canTxId>>24&0xFF;
    cmd[10] = canTxId>>16&0xFF;
    cmd[11] = canTxId>>8&0xFF;
    cmd[12] = canTxId&0xFF;


    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();



    QByteArray ack_head(cmd);

    qDebug() << cmd.toHex() << ack_head.toHex();

    int cnt = 0;
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));


}


/**设置IIC地址
 * @brief receiver::slot_do_cmd_iic_setaddr
 * @param iic_addr
 * @param result
 */
void serialportio::slot_iic_setaddr_pro(uint iicAddress,uint newiicAddr, bool &result)
{

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_iic_pro_enter_settingmode(iicAddress);

    if(nRet)
    {
        uint16_t crc;
        QByteArray cmd;



        cmd.resize(13);

        cmd[0] = 0x53;
        //iic addr
        cmd[1] = iicAddress *2;
        cmd[2] = 0x05;

        cmd[3] = 0x5A;
        cmd[4] = 0x05;
        cmd[5] = 0x0B;
        cmd[6] = newiicAddr;

        cmd[7] = calc_checksum(cmd);

        cmd[8] = 0x50;


        cmd[9] = 0x53;
        cmd[10] = iicAddress *2 +1;
        cmd[11] = 0x05;
        cmd[12] = 0x50;


        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        result = false;
        QByteArray ack_head(cmd);

        qDebug() << cmd.toHex() << ack_head.toHex();

        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(ack_head);
            cnt++;
        }


        nRet=false;

        nRet = cmd_iic_pro_exit_settingmode(iicAddress);
        if(!nRet)
        {
            qWarning("退出设置模式失败");
        }

   }


   connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));



}


/**
 * @brief receiver::slot_do_cmd_uart_setio
 * @param ioMode
 * @param distVal
 * @param zoneVal
 * @param delayVal1
 * @param delayVal2
 * @param result
 */
void serialportio::slot_do_cmd_uart_setio(uint ioMode,uint distVal,uint zoneVal,uint delayVal1,uint delayVal2, bool &result)
{

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    uint16_t crc;
    QByteArray cmd;
    cmd.resize(13);
    cmd[0] = 0x5A;
    cmd[1] = 0x0D;
    cmd[2] = 0x3B;
    cmd[3] = ioMode;


    cmd[4] = distVal&0xFF;
    cmd[5] = distVal>>8&0xFF;


    cmd[6] = zoneVal&0xFF;
    cmd[7] = zoneVal>>8&0xFF;

    cmd[8] = delayVal1&0xFF;
    cmd[9] = delayVal1>>8&0xFF;

    cmd[10] = delayVal2&0xFF;
    cmd[11] = delayVal2>>8&0xFF;


    cmd[12] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head(cmd);

    qDebug() << cmd.toHex() << ack_head.toHex();

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));



}



bool serialportio::read_ack(QByteArray ack)
{
    QByteArray ba_read;
    bool status;
    int cnt = 0;

    qDebug() << "ack" << ack.toHex();

    while(1)
    {
        serialPort_->waitForReadyRead(500);
        ba_read.append(serialPort_->readAll());
        qDebug() << "read " << ba_read.toHex(' ');
        if(true == ba_read.contains(ack))
        {
            status = true;
            break;
        }
        ba_read = ba_read.right(ack.length());

        if(1 <= cnt++)
        {
            status = false;
            break;
        }
    }

    qDebug() << __FUNCTION__ << status;
    //接收到的数据帧
    emit signalDataContractRcv(ba_read);
    return status;
}

unsigned char serialportio::calc_checksum(QByteArray &cmd)
{
    unsigned int length = cmd.length();
    unsigned int sum = 0;

    for(unsigned int index = 0; index < length - 1; index++)
    {
        sum += cmd.at(index);
    }
    return (sum & 0xFF);
}



void serialportio::slot_iic_soft_reset(uint iicAddress, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd,ba_read;

    cmd.resize(12);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress * 2;
    cmd[2] = 0x04;

    cmd[3] = 0x5A;
    cmd[4] = 0x04;
    cmd[5] = 0x02;
    cmd[6] = calc_checksum(cmd);

    cmd[7] = 0x50;


    cmd[8] = 0x53;
    cmd[9] =  iicAddress * 2 + 1;
    cmd[10] = 0x09;
    cmd[11] = 0x50;


    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_SOFT_RESET;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);

    while((false == result) && (cnt < 2))
    {
        int nSend = serialPortWrite(cmd);

        if(nSend>0)
        {
            result = true;
            serialPort_->waitForReadyRead(500);
            ba_read.append(serialPort_->readAll());
            emit signalDataRcv(ba_read);

            break;
        }

        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_do_cmd_id_soft_reset(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(4);
    cmd[0] = 0x5A;
    cmd[1] = 4;
    cmd[2] = ID_SOFT_RESET;
    cmd[3] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_SOFT_RESET;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

/**系统复位，不需要进入配置模式
 * @brief receiver::slot_do_cmd_id_soft_reset_pro
 * @param result
 */
void serialportio::slot_do_cmd_id_soft_reset_pro(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));


    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = 0xAA;
    cmd[1] = 0x55;
    cmd[2] = 0xF0;
    cmd[3] = 0x00;
    cmd[4] = 0xFF;
    cmd[5] = 0xFF;
    cmd[6] = 0xFF;
    cmd[7] = 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(8);
    ack_head[0] = 0xAA;
    ack_head[1] = 0x55;
    ack_head[2] = 0xF0;
    ack_head[3] = 0x00;
    ack_head[4] = 0xFF;
    ack_head[5] = 0xFF;
    ack_head[6] = 0xFF;
    ack_head[7] = 0xFF;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }



    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_iic_getdistcm(uint iicAddress,unsigned short &dist, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray data, cmd;
    cmd.resize(13);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress * 2;
    cmd[2] = 0x05;

    cmd[3] = 0x5A;
    cmd[4] = 0x05;
    cmd[5] = 0x00;
    cmd[6] = 0x01;
    cmd[7] = 0x60;

    cmd[8] = 0x50;


    cmd[9] = 0x53;
    cmd[10] =  iicAddress * 2 + 1;
    cmd[11] = 0x09;
    cmd[12] = 0x50;

    int nSend = serialPortWrite(cmd);

    if(nSend>0){

        int cnt = 0;
        result = false;
        QByteArray ba_read;

        QByteArray ack_head;
        ack_head.resize(2);
        ack_head[0] = 0x59;
        ack_head[1] = 0x59;
        while(1)
        {
            serialPort_->waitForReadyRead(50);
            ba_read = serialPort_->readAll();
            qDebug() << ba_read.toHex();

            if(10 <= cnt++)
            {
                break;
            }
            data.append(ba_read);

            if(data.length() < 9)
            {
                continue;
            }

            if(false == data.contains(ack_head))
            {
                continue;
            }

            uint32_t index = data.indexOf(ack_head);
            if(data.right(data.length() - index).length() < 9)
            {
                data = data.right(data.length() - index);
                continue;
            }
            data = data.mid(index, 9);
            break;
        }

        if(cnt >= 10)
        {
            result = false;
        }else{
            result = true;
            dist = (uint8_t)data.at(2) + ((uint16_t)data.at(3) << 8);
            emit signalDataRcv(data);
        }
    }



    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


/**9字节 cm
 * @brief receiver::slot_do_cmd_iic_getdist
 * @param dist
 * @param result
 */
void serialportio::slot_iic_querydistancecm(uint iicAddress,bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray data, cmd;
    cmd.resize(13);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x05;

    cmd[3] = 0x5A;
    cmd[4] = 0x05;
    cmd[5] = 0x00;
    cmd[6] = 0x01;
    cmd[7] = 0x60;

    cmd[8] = 0x50;


    cmd[9] = 0x53;
    cmd[10] = iicAddress *2 +1;
    cmd[11] = 0x09;
    cmd[12] = 0x50;

    int nSend = serialPortWrite(cmd);

    if(nSend){
        QByteArray ba_read;

        QByteArray ack_head;
        ack_head.resize(2);
        ack_head[0] = 0x59;
        ack_head[1] = 0x59;
        int cnt = 0;

        while(1)
        {
            serialPort_->waitForReadyRead(20);
            ba_read = serialPort_->readAll();


            qDebug() << ba_read.toHex(' ');

            if(10 <= cnt++)
            {
                break;
            }
            data.append(ba_read);

            if(data.length() < 9)
            {
                continue;
            }

            if(false == data.contains(ack_head))
            {
                continue;
            }

            uint32_t index = data.indexOf(ack_head);
            if(data.right(data.length() - index).length() < 9)
            {
                data = data.right(data.length() - index);
                continue;
            }
            data = data.mid(index, 9);
            emit signalDataRcv(data);
            break;
        }

        if(cnt >= 10)
        {
            result = false;

        }else{
            result = true;

            int dist = (uint8_t)data.at(2) + ((uint16_t)data.at(3) << 8);
            int amp = (uint8_t)data.at(4) + ((uint16_t)data.at(5) << 8);
            passDist_.append(dist);
            passAmp_.append(amp);
            passRxFrame_.append(ba_read);

            //freq_ = 10;

            emit signalDistance(passDist_,passAmp_,passRxFrame_,iicFreq_);

            passDist_.clear();
            passAmp_.clear();

        }

    }

     connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

}

/**9字节 cm
 * @brief receiver::slot_do_cmd_iic_getdist
 * @param dist
 * @param result
 */
void serialportio::slot_iic_querydistancecm_pro(uint iicAddress,bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_iic_pro_enter_settingmode(iicAddress);

    if(nRet)
    {
        QByteArray data, cmd;
        cmd.resize(13);

        cmd[0] = 0x53;
        //iic addr
        cmd[1] = iicAddress *2;
        cmd[2] = 0x05;

        cmd[3] = 0x5A;
        cmd[4] = 0x05;
        cmd[5] = 0x00;
        cmd[6] = 0x01;
        cmd[7] = 0x60;

        cmd[8] = 0x50;


        cmd[9] = 0x53;
        cmd[10] = iicAddress *2 +1;
        cmd[11] = 0x09;
        cmd[12] = 0x50;

        int nSend = serialPortWrite(cmd);

        if(nSend){
            QByteArray ba_read;

            QByteArray ack_head;
            ack_head.resize(2);
            ack_head[0] = 0x59;
            ack_head[1] = 0x59;
            int cnt = 0;

            while(1)
            {
                serialPort_->waitForReadyRead(20);
                ba_read = serialPort_->readAll();


                qDebug() << ba_read.toHex(' ');

                if(10 <= cnt++)
                {
                    break;
                }
                data.append(ba_read);

                if(data.length() < 9)
                {
                    continue;
                }

                if(false == data.contains(ack_head))
                {
                    continue;
                }

                uint32_t index = data.indexOf(ack_head);
                if(data.right(data.length() - index).length() < 9)
                {
                    data = data.right(data.length() - index);
                    continue;
                }
                data = data.mid(index, 9);
                emit signalDataRcv(data);
                break;
            }

            if(cnt >= 10)
            {
                result = false;

            }else{
                result = true;

                int dist = (uint8_t)data.at(2) + ((uint16_t)data.at(3) << 8);
                int amp = (uint8_t)data.at(4) + ((uint16_t)data.at(5) << 8);
                passDist_.append(dist);
                passAmp_.append(amp);
                passRxFrame_.append(ba_read);

                //freq_ = 10;

                emit signalDistance(passDist_,passAmp_,passRxFrame_,iicFreq_);

                passDist_.clear();
                passAmp_.clear();

            }

        }


        nRet=false;

        nRet = cmd_iic_pro_exit_settingmode(iicAddress);
        if(!nRet)
        {
            qWarning("退出设置模式失败");
        }
    }



     connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

}



void serialportio::slot_iic_getdist_9bytecm_pro(uint iicAddress, unsigned short &dist, bool &result)
{
         disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

        bool nRet = cmd_iic_pro_enter_settingmode(iicAddress);

        if(nRet)
        {

            QByteArray data, cmd;
            cmd.resize(13);


            cmd[0] = 0x53;
            //iic addr
            cmd[1] = iicAddress * 2;
            cmd[2] = 0x05;

            cmd[3] = 0x5A;
            cmd[4] = 0x05;
            cmd[5] = 0x00;
            cmd[6] = 0x01;
            cmd[7] = 0x60;

            cmd[8] = 0x50;


            cmd[9] = 0x53;
            cmd[10] =  iicAddress * 2 + 1;
            cmd[11] = 0x09;
            cmd[12] = 0x50;

            int nSend = serialPortWrite(cmd);

            if(nSend>0){

                int cnt = 0;
                result = false;
                QByteArray ba_read;

                QByteArray ack_head;
                ack_head.resize(2);
                ack_head[0] = 0x59;
                ack_head[1] = 0x59;
                while(1)
                {
                    serialPort_->waitForReadyRead(50);
                    ba_read = serialPort_->readAll();
                    qDebug() << ba_read.toHex();

                    if(10 <= cnt++)
                    {
                        break;
                    }
                    data.append(ba_read);

                    if(data.length() < 9)
                    {
                        continue;
                    }

                    if(false == data.contains(ack_head))
                    {
                        continue;
                    }

                    uint32_t index = data.indexOf(ack_head);
                    if(data.right(data.length() - index).length() < 9)
                    {
                        data = data.right(data.length() - index);
                        continue;
                    }
                    data = data.mid(index, 9);
                    break;
                }

                if(cnt >= 10)
                {
                    result = false;
                }else{
                    result = true;
                    dist = (uint8_t)data.at(2) + ((uint16_t)data.at(3) << 8);
                    emit signalDataRcv(data);
            }

            nRet=false;

            nRet = cmd_iic_pro_exit_settingmode(iicAddress);
            if(!nRet)
            {
                qWarning("退出设置模式失败");
            }
          }


        connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    }
}





void serialportio::slot_do_cmd_id_sample_trig(unsigned short &dist, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray data, cmd;
    cmd.resize(4);
    cmd[0] = 0x5A;
    cmd[1] = 4;
    cmd[2] = ID_SAMPLE_TRIG;
    cmd[3] = calc_checksum(cmd);

    qDebug() << __FUNCTION__ << cmd.toHex();
    serialPortWrite(cmd);

    int cnt = 0;
    result = false;
    QByteArray ba_read;

    QByteArray ack_head;
    ack_head.resize(2);
    ack_head[0] = 0x59;
    ack_head[1] = 0x59;
    while(1)
    {
        serialPort_->waitForReadyRead(2000);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        if(10 <= cnt++)
        {
            break;
        }
        data.append(ba_read);

        if(data.length() < 9)
        {
            continue;
        }

        if(false == data.contains(ack_head))
        {
            continue;
        }

        uint32_t index = data.indexOf(ack_head);
        if(data.right(data.length() - index).length() < 9)
        {
            data = data.right(data.length() - index);
            continue;
        }
        data = data.mid(index, 9);
        break;
    }

    if(cnt >= 10)
    {
        result = false;
    }else{
        result = true;
        dist = (uint8_t)data.at(2) + ((uint16_t)data.at(3) << 8);
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_sample_freq(unsigned int freq, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(6);
    cmd[0] = 0x5A;
    cmd[1] = 6;
    cmd[2] = ID_SAMPLE_FREQ;
    cmd[3] = freq & 0xFF;
    cmd[4] = (freq >> 8) & 0xFF;
    cmd[5] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(cmd);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_output_format(unsigned char format, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_OUTPUT_FORMAT;
    cmd[3] = format;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_OUTPUT_FORMAT;
    ack_head[3] = format;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_iic_output_format(uint iicAddress, unsigned char format, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;

    cmd.resize(13);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x05;

    cmd[3] = 0x5A;
    cmd[4] = 0x05;
    cmd[5] = 0x05;
    cmd[6] = format;
    cmd[7] = calc_checksum(cmd);

    cmd[8] = 0x50;


    cmd[9] = 0x53;
    cmd[10] = iicAddress *2 +1;
    cmd[11] = 0x05;
    cmd[12] = 0x50;



    int cnt = 0;
    result = false;



    qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');


    result = false;
    QByteArray ack_head;
    ack_head.resize(5);

    ack_head[0] = 0x5A;
    ack_head[1] = 0x05;
    ack_head[2] = 0x05;
    ack_head[3] = format;
    ack_head[4] = calc_checksum(ack_head);

    while((false == result) && (cnt < 2))
    {
        int nSend = serialPortWrite(cmd);
        if(nSend<0)
        {
            continue;
        }

        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_iic_output_format_pro(uint iicAddress, unsigned char format, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_iic_pro_enter_settingmode(iicAddress);
    if(nRet)
    {
        QByteArray cmd;

        cmd.resize(13);

        cmd[0] = 0x53;
        //iic addr
        cmd[1] = iicAddress *2;
        cmd[2] = 0x05;

        cmd[3] = 0x5A;
        cmd[4] = 0x05;
        cmd[5] = 0x05;
        cmd[6] = format;
        cmd[7] = calc_checksum(cmd);

        cmd[8] = 0x50;


        cmd[9] = 0x53;
        cmd[10] = iicAddress *2 +1;
        cmd[11] = 0x05;
        cmd[12] = 0x50;



        int cnt = 0;
        result = false;



        qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');


        result = false;
        QByteArray ack_head;
        ack_head.resize(5);

        ack_head[0] = 0x5A;
        ack_head[1] = 0x05;
        ack_head[2] = 0x05;
        ack_head[3] = format;
        ack_head[4] = calc_checksum(ack_head);

        while((false == result) && (cnt < 2))
        {
            int nSend = serialPortWrite(cmd);
            if(nSend<0)
            {
                continue;
            }

            result = read_ack(ack_head);
            cnt++;
        }

        nRet=false;

        nRet = cmd_iic_pro_exit_settingmode(iicAddress);
        if(!nRet)
        {
            qWarning("退出设置模式失败");
        }

    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_output_format_pro(unsigned char format, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();
    if(nRet)
    {
        QByteArray cmd;
        cmd.resize(5);
        cmd[0] = 0x5A;
        cmd[1] = 5;
        cmd[2] = ID_OUTPUT_FORMAT;
        cmd[3] = format;
        cmd[4] = calc_checksum(cmd);

        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        result = false;
        QByteArray ack_head;
        ack_head.resize(5);
        ack_head[0] = 0x5A;
        ack_head[1] = 0x5;
        ack_head[2] = ID_OUTPUT_FORMAT;
        ack_head[3] = format;
        ack_head[4] = calc_checksum(ack_head);
        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(ack_head);
            cnt++;
        }

        nRet=false;

        nRet = cmd_uart_pro_exit_settingmode();
        if(!nRet)
        {
            qWarning("退出设置模式失败");
        }

    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_setwiperperiod_pro(uint wiperPeriod, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();
    if(nRet)
    {
        QByteArray cmd;
        cmd.resize(8);
        cmd[0] = 0xAA;
        cmd[1] = 0x55;
        cmd[2] = 0xF0;
        cmd[3] = 0x00;
        cmd[4] = wiperPeriod>>8&0xFF;
        cmd[5] = wiperPeriod&0xFF;
        cmd[6] = 0x00;
        cmd[7] = 0xB2;

        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        result = false;


        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);

            //发送和回复一致
            result = read_ack(cmd);
            cnt++;
        }

        nRet=false;

        nRet = cmd_uart_pro_exit_settingmode();
        if(!nRet)
        {
            qWarning("退出设置模式失败");
        }

    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_setwipercycle_pro(uint wiperCycle, bool &result)
{
	disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

	bool nRet = cmd_uart_pro_enter_settingmode();
	if (nRet)
	{
		QByteArray cmd;
		cmd.resize(8);
		cmd[0] = 0xAA;
		cmd[1] = 0x55;
		cmd[2] = 0xF0;
		cmd[3] = 0x00;
		cmd[4] = wiperCycle;
		cmd[5] = 0x00;
		cmd[6] = 0x00;
		cmd[7] = 0xB3;

		qDebug() << endl << __FUNCTION__ << cmd.toHex();

		int cnt = 0;
		result = false;


		while ((false == result) && (cnt < 2))
		{
			serialPortWrite(cmd);

			//发送和回复一致
			result = read_ack(cmd);
			cnt++;
		}

		nRet = false;

		nRet = cmd_uart_pro_exit_settingmode();
		if (!nRet)
		{
			qWarning("退出设置模式失败");
		}

	}


	connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_setwipercycle_modbus(uint dev_addr, uint cycleCnt, bool &result)
{
	disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
	uint16_t crc;
	QByteArray cmd, ba_read, all_read;
	cmd.resize(8);
	cmd[0] = dev_addr;
	cmd[1] = 0x06;
	cmd[2] = 0x00;
	cmd[3] = 0x8D;
	cmd[4] = (cycleCnt >> 8) & 0xFF;
	cmd[5] = cycleCnt & 0xFF;

	crc = crc16_calc(cmd, 6);
	cmd[6] = crc & 0xFF;
	cmd[7] = (crc >> 8) & 0xFF;

	qDebug() << endl << __FUNCTION__ << cmd.toHex();

	int cnt = 0;
	int index = 0;
	result = false;
	QByteArray ack_head(cmd);

	int nSendCnt = serialPortWrite(cmd);

	ack_head.resize(5);
	ack_head[0] = dev_addr;
	ack_head[1] = 0x06;
	ack_head[3] = 0x00;
	ack_head[4] = 0x8C;

	while (1)
	{
		serialPort_->waitForReadyRead(100);
		ba_read = serialPort_->readAll();
		qDebug() << ba_read.toHex();

		if (2 <= cnt++)
		{
			break;
		}
		all_read.append(ba_read);

		if (all_read.length() < 8)
		{
			continue;
		}

		index = all_read.indexOf(ack_head);

		if (0 > index)
		{
			continue;
		}

		if (all_read.right(all_read.length() - index).length() < 8)
		{
			all_read = all_read.right(all_read.length() - index);
			continue;
		}

		all_read = all_read.mid(index, 8);

		if (crc16_calc(all_read.left(6), 6) != (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8)))
		{
			qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(5), 5) << ", ack:" << (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8));
			cnt = 2;
			break;
		}

		break;

	}

	if (cnt >= 2)
	{
		result = false;
	}
	else {
		result = true;
		emit signalDataRcv(all_read);
	}

	connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}



void serialportio::slot_setwiperperiod_modbus(uint dev_addr, uint wiperPeriod, bool &result)
{
	disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
	uint16_t crc;
	QByteArray cmd, ba_read, all_read;
	cmd.resize(8);
	cmd[0] = dev_addr;
	cmd[1] = 0x06;
	cmd[2] = 0x00;
	cmd[3] = 0x8C;
	cmd[4] = (wiperPeriod >> 8) & 0xFF;
	cmd[5] = wiperPeriod & 0xFF;

	crc = crc16_calc(cmd, 6);
	cmd[6] = crc & 0xFF;
	cmd[7] = (crc >> 8) & 0xFF;

	qDebug() << endl << __FUNCTION__ << cmd.toHex();

	int cnt = 0;
	int index = 0;
	result = false;
	QByteArray ack_head(cmd);

	int nSendCnt = serialPortWrite(cmd);

	ack_head.resize(5);
	ack_head[0] = dev_addr;
	ack_head[1] = 0x06;
	ack_head[3] = 0x00;
	ack_head[4] = 0x8C;

	while (1)
	{
		serialPort_->waitForReadyRead(100);
		ba_read = serialPort_->readAll();
		qDebug() << ba_read.toHex();

		if (2 <= cnt++)
		{
			break;
		}
		all_read.append(ba_read);

		if (all_read.length() < 8)
		{
			continue;
		}

		index = all_read.indexOf(ack_head);

		if (0 > index)
		{
			continue;
		}

		if (all_read.right(all_read.length() - index).length() < 8)
		{
			all_read = all_read.right(all_read.length() - index);
			continue;
		}

		all_read = all_read.mid(index, 8);

		if (crc16_calc(all_read.left(6), 6) != (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8)))
		{
			qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(5), 5) << ", ack:" << (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8));
			cnt = 2;
			break;
		}

		break;

	}

	if (cnt >= 2)
	{
		result = false;
	}
	else {
		result = true;
		emit signalDataRcv(all_read);
	}

	connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_startwiper_modbus(uint dev_addr, bool &result)
{
	disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
	uint16_t crc;
	QByteArray cmd, ba_read, all_read;
	cmd.resize(8);
	cmd[0] = dev_addr;
	cmd[1] = 0x06;
	cmd[2] = 0x00;
	cmd[3] = 0x8A;
	cmd[4] = 0x00;
	cmd[5] = 0x00;

	crc = crc16_calc(cmd, 6);
	cmd[6] = crc & 0xFF;
	cmd[7] = (crc >> 8) & 0xFF;

	qDebug() << endl << __FUNCTION__ << cmd.toHex();

	int cnt = 0;
	int index = 0;
	result = false;
	QByteArray ack_head(cmd);

	int nSendCnt = serialPortWrite(cmd);

	ack_head.resize(5);
	ack_head[0] = dev_addr;
	ack_head[1] = 0x06;
	ack_head[3] = 0x00;
	ack_head[4] = 0x8A;

	while (1)
	{
		serialPort_->waitForReadyRead(100);
		ba_read = serialPort_->readAll();
		qDebug() << ba_read.toHex();

		if (2 <= cnt++)
		{
			break;
		}
		all_read.append(ba_read);

		if (all_read.length() < 8)
		{
			continue;
		}

		index = all_read.indexOf(ack_head);

		if (0 > index)
		{
			continue;
		}

		if (all_read.right(all_read.length() - index).length() < 8)
		{
			all_read = all_read.right(all_read.length() - index);
			continue;
		}

		all_read = all_read.mid(index, 8);

		if (crc16_calc(all_read.left(6), 6) != (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8)))
		{
			qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(5), 5) << ", ack:" << (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8));
			cnt = 2;
			break;
		}

		break;

	}

	if (cnt >= 2)
	{
		result = false;
	}
	else {
		result = true;
		emit signalDataRcv(all_read);
	}

	connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_startwiper_pro(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();
    if(nRet)
    {
        QByteArray cmd;
        cmd.resize(8);
        cmd[0] = 0xAA;
        cmd[1] = 0x55;
        cmd[2] = 0xF0;
        cmd[3] = 0x00;
        cmd[4] = 0x00;
        cmd[5] = 0x00;
        cmd[6] = 0x00;
        cmd[7] = 0xB0;

        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        result = false;


        while((false == result) && (cnt < 2))
        {
           int nWriteCnt = serialPortWrite(cmd);

            //发送和回复一致
            if(nWriteCnt>0){
                result = true;
                break;
            }

            cnt++;
        }

        nRet=false;

        nRet = cmd_uart_pro_exit_settingmode();
        if(!nRet)
        {
            qWarning("退出设置模式失败");
        }

    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_do_cmd_id_baud_rate(unsigned int baudrate, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = 0x5A;
    cmd[1] = 8;
    cmd[2] = ID_BAUD_RATE;
    cmd[3] = (unsigned char)(baudrate & 0xFF);
    cmd[4] = (unsigned char)(baudrate >> 8 & 0xFF);
    cmd[5] = (unsigned char)(baudrate >> 16 & 0xFF);
    cmd[6] = (unsigned char)(baudrate >> 24 & 0xFF);
    cmd[7] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    while((false == result) && (cnt < 2))
    {
        int nSendCnt = serialPortWrite(cmd);
        if(nSendCnt>0)
        {
            result = true;
            break;
        }

        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

/**
 * @brief receiver::slot_analog_modeswitch
 * @param mode 0 正向  1 逆向
 * @param result
 */
void serialportio::slot_analog_modeswitch(uint mode, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 0x05;
    cmd[2] = 0x71;

    if(mode==0){
        cmd[3] = 0x00;
    }else if(mode==1) {
        cmd[3] = 0x01;
    }

    cmd[4] = calc_checksum(cmd);


    qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');

    int cnt = 0;
    result = false;

    QByteArray ack_head;

    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x05;
    ack_head[2] = 0x71;
    ack_head[3] = 0x00;
    ack_head[4] = 0xD0;

    while((false == result) && (cnt < 2))
    {
        int nSendCnt = serialPortWrite(cmd);
        if(nSendCnt>0)
        {
            result = read_ack(ack_head);
            break;
        }

        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


/**设置下限距离值 4-20mA
 * @brief receiver::slot_analog_modeswitch
 * @param mode 0 正向  1 逆向
 * @param result
 */
void serialportio::slot_analog_setdistmin(uint mindist, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(6);
    cmd[0] = 0x5A;
    cmd[1] = 0x06;
    cmd[2] = 0x8C;
    cmd[3] = mindist<<8&0xFF;
    cmd[4] = mindist&0xFF;
    cmd[5] = calc_checksum(cmd);


    qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');

    int cnt = 0;
    result = false;

    QByteArray ack_head;

    ack_head.resize(1);
    ack_head[0] = 0x5A;
    //ack_head[1] = 0x05;
    //ack_head[2] = 0x8C;
    //ack_head[3] = 0x00;
    //ack_head[4] = 0xEB;

    while((false == result) && (cnt < 2))
    {
        int nSendCnt = serialPortWrite(cmd);
        if(nSendCnt>0)
        {
            result = read_ack(ack_head);
            break;
        }

        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}



void serialportio::slot_analog_setdistmax(uint maxdist, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(6);
    cmd[0] = 0x5A;
    cmd[1] = 0x06;
    cmd[2] = 0x4F;
    cmd[3] = maxdist<<8&0xFF;
    cmd[4] = maxdist&0xFF;
    cmd[5] = calc_checksum(cmd);


    qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');

    int cnt = 0;
    result = false;

    QByteArray ack_head;

    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x05;
    ack_head[2] = 0x4F;
    ack_head[3] = 0x00;
    ack_head[4] = 0xAE;

    while((false == result) && (cnt < 2))
    {
        int nSendCnt = serialPortWrite(cmd);
        if(nSendCnt>0)
        {
            result = read_ack(ack_head);
            break;
        }

        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


/**进入配置模式
 * @brief receiver::cmd_uart_pro_enter_settingmode
 * @return
 */
bool serialportio::cmd_uart_pro_enter_settingmode()
{

    QByteArray cmd;

    cmd.resize(8);
    cmd[0] = 0xAA;
    cmd[1] = 0x55;
    cmd[2] = 0xF0;
    cmd[3] = 0x00;
    cmd[4] = 0x01;
    cmd[5] = 0x00;
    cmd[6] = 0x00;
    cmd[7] = 0x02;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    bool result = false;

    QByteArray ack_head;
    ack_head.resize(8);
    ack_head[0] = 0xAA;
    ack_head[1] = 0x55;
    ack_head[2] = 0xF0;
    ack_head[3] = 0x00;
    ack_head[4] = 0x01;
    ack_head[5] = 0x00;
    ack_head[6] = 0x00;
    ack_head[7] = 0x02;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        if(result)
        {
            return result;
        }

        cnt++;
    }

}

bool serialportio::cmd_iic_pro_enter_settingmode(uint iicAddress)
{

    QByteArray cmd;

    cmd.resize(16);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x08;


    cmd[3] = 0xAA;
    cmd[4] = 0x55;
    cmd[5] = 0xF0;
    cmd[6] = 0x00;
    cmd[7] = 0x01;
    cmd[8] = 0x00;
    cmd[9] = 0x00;
    cmd[10] = 0x02;

    cmd[11] = 0x50;


    cmd[12] = 0x53;
    cmd[13] = iicAddress *2 +1;
    cmd[14] = 0x08;
    cmd[15] = 0x50;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    bool result = false;

    QByteArray ack_head;
    ack_head.resize(8);
    ack_head[0] = 0xAA;
    ack_head[1] = 0x55;
    ack_head[2] = 0xF0;
    ack_head[3] = 0x00;
    ack_head[4] = 0x01;
    ack_head[5] = 0x00;
    ack_head[6] = 0x00;
    ack_head[7] = 0x02;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        if(result)
        {
            return result;
        }

        cnt++;
    }

}

bool serialportio::cmd_uart_pro_exit_settingmode()
{

    QByteArray cmd;

    cmd.resize(8);
    cmd[0] = 0xAA;
    cmd[1] = 0x55;
    cmd[2] = 0xF0;
    cmd[3] = 0x00;
    cmd[4] = 0x00;
    cmd[5] = 0x00;
    cmd[6] = 0x00;
    cmd[7] = 0x02;


    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    bool result = false;

    QByteArray ack_head;
    ack_head.resize(8);
    ack_head[0] = 0xAA;
    ack_head[1] = 0x55;
    ack_head[2] = 0xF0;
    ack_head[3] = 0x00;
    ack_head[4] = 0x00;
    ack_head[5] = 0x00;
    ack_head[6] = 0x00;
    ack_head[7] = 0x02;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);

        if(result){
            return result;
        }

        cnt++;
    }


}

bool serialportio::cmd_iic_pro_exit_settingmode(uint iicAddress)
{

    QByteArray cmd;

    cmd.resize(8);



    cmd.resize(16);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x08;


    cmd[3] = 0xAA;
    cmd[4] = 0x55;
    cmd[5] = 0xF0;
    cmd[6] = 0x00;
    cmd[7] = 0x00;
    cmd[8] = 0x00;
    cmd[9] = 0x00;
    cmd[10] = 0x02;

    cmd[11] = 0x50;


    cmd[12] = 0x53;
    cmd[13] = iicAddress *2 +1;
    cmd[14] = 0x08;
    cmd[15] = 0x50;



    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    bool result = false;

    QByteArray ack_head;
    ack_head.resize(8);
    ack_head[0] = 0xAA;
    ack_head[1] = 0x55;
    ack_head[2] = 0xF0;
    ack_head[3] = 0x00;
    ack_head[4] = 0x00;
    ack_head[5] = 0x00;
    ack_head[6] = 0x00;
    ack_head[7] = 0x02;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);

        if(result){
            return result;
        }

        cnt++;
    }


}

void serialportio::slot_uart_sendoneframe(QByteArray cmd, bool &result)
{

      //disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
      result = false;
      int sendCnt = serialPortWrite(cmd);
      if(sendCnt>0)
      {
          result = true;
      }

//      serialPort_->waitForReadyRead(500);

//      QByteArray cmdRx = serialPort_->readAll();
//      emit signalDataRcv(cmdRx);


}


void serialportio::slot_do_cmd_id_baud_rate_pro(unsigned int baudrate, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();
    if(nRet)
    {
        QByteArray cmd;

        cmd.resize(8);
        cmd[0] = 0x5A;
        cmd[1] = 8;
        cmd[2] = ID_BAUD_RATE;
        cmd[3] = (unsigned char)(baudrate & 0xFF);
        cmd[4] = (unsigned char)(baudrate >> 8 & 0xFF);
        cmd[5] = (unsigned char)(baudrate >> 16 & 0xFF);
        cmd[6] = (unsigned char)(baudrate >> 24 & 0xFF);
        cmd[7] = calc_checksum(cmd);

        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        bool result = false;

        QByteArray ack_head;
        ack_head.resize(8);
        ack_head[0] = 0x5A;
        ack_head[1] = 8;
        ack_head[2] = ID_BAUD_RATE;
        ack_head[3] = (unsigned char)(baudrate & 0xFF);
        ack_head[4] = (unsigned char)(baudrate >> 8 & 0xFF);
        ack_head[5] = (unsigned char)(baudrate >> 16 & 0xFF);
        ack_head[6] = (unsigned char)(baudrate >> 24 & 0xFF);
        ack_head[7] = calc_checksum(cmd);

        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(ack_head);
            cnt++;
        }


    }

    nRet = false;

    nRet = cmd_uart_pro_exit_settingmode();

    if(nRet){
        qWarning("退出设定模式失败");
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_output_en(unsigned char is_enabled, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_OUTPUT_EN;
    cmd[3] = (unsigned char)is_enabled;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_OUTPUT_EN;
    ack_head[3] = (unsigned char)is_enabled;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_output_en_pro(unsigned char is_enabled, bool &result)
{

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();
    if(nRet)
    {
        QByteArray cmd;
        cmd.resize(5);
        cmd[0] = 0x5A;
        cmd[1] = 5;
        cmd[2] = 0x07;
        cmd[3] = (unsigned char)is_enabled;
        cmd[4] = calc_checksum(cmd);

        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        result = false;
        QByteArray ack_head;
        ack_head.resize(5);
        ack_head[0] = 0x5A;
        ack_head[1] = 0x5;
        ack_head[2] = 0x07;
        ack_head[3] = (unsigned char)is_enabled;
        ack_head[4] = calc_checksum(ack_head);
        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(ack_head);
            cnt++;
        }
    }


    nRet = false;

    nRet = cmd_uart_pro_exit_settingmode();

    if(nRet){
        qWarning("退出设定模式失败");
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_frame_checksum_en(unsigned char is_enabled, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_FRAME_CHECKSUM_EN;
    cmd[3] = (unsigned char)is_enabled;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_FRAME_CHECKSUM_EN;
    ack_head[3] = (unsigned char)is_enabled;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_restore_default(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(4);
    cmd[0] = 0x5A;
    cmd[1] = 4;
    cmd[2] = ID_RESTORE_DEFAULT;
    cmd[3] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_iic_restore_default(uint iicAddress, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd,ba_read;


    cmd.resize(12);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x04;


    cmd[3] = 0x5A;
    cmd[4] = 4;
    cmd[5] = 0x10;
    cmd[6] = calc_checksum(cmd);

    cmd[7] = 0x50;


    cmd[8] = 0x53;
    cmd[9] = iicAddress *2 +1;
    cmd[10] = 0x05;
    cmd[11] = 0x50;


    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_RESTORE_DEFAULT;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);

    while((false == result) && (cnt < 2))
    {
        int nSend = serialPortWrite(cmd);

        if(nSend>0)
        {
            result = true;
            serialPort_->waitForReadyRead(500);
            ba_read.append(serialPort_->readAll());
            break;
        }

        cnt++;
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_restore_default_pro(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();

    if(nRet)
    {
        QByteArray cmd;
        cmd.resize(4);
        cmd[0] = 0x5A;
        cmd[1] = 4;
        cmd[2] = 0x10;
        cmd[3] = calc_checksum(cmd);

        qDebug() << endl << __FUNCTION__ << cmd.toHex();

        int cnt = 0;
        result = false;
        QByteArray ack_head;
        ack_head.resize(5);
        ack_head[0] = 0x5A;
        ack_head[1] = 0x5;
        ack_head[2] = 0x10;
        ack_head[3] = 0;
        ack_head[4] = calc_checksum(ack_head);
        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(ack_head);
            cnt++;
        }

        nRet = false;

        nRet = cmd_uart_pro_exit_settingmode();

        if(nRet){
            qWarning("退出设定模式失败");
        }
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_save_settings(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(4);
    cmd[0] = 0x5A;
    cmd[1] = 4;
    cmd[2] = ID_SAVE_SETTINGS;
    cmd[3] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_SAVE_SETTINGS;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_iic_save_settings(uint iicAddress, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd,ba_read;

    qDebug() << endl << __FUNCTION__ << cmd.toHex(' ');

    cmd.resize(12);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x04;

    cmd[3] = 0x5A;
    cmd[4] = 0x04;
    cmd[5] = 0x11;
    cmd[6] = calc_checksum(cmd);

    cmd[7] = 0x50;


    cmd[8] = 0x53;
    cmd[9] = iicAddress *2 +1;
    cmd[10] = 0x05;
    cmd[11] = 0x50;


    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = 0x11;
    ack_head[3] = 0x00;
    ack_head[4] = calc_checksum(ack_head);

    while((false == result) && (cnt < 2))
    {
        int nSend = serialPortWrite(cmd);

        if(nSend>0)
        {
            result = true;
            serialPort_->waitForReadyRead(500);
            ba_read.append(serialPort_->readAll());
            break;
        }

        cnt++;
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


/**TODO
 * @brief receiver::slot_do_cmd_id_save_settings_pro
 * @param result
 */
void serialportio::slot_do_cmd_id_save_settings_pro(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();

    if(nRet)
    {

    }

    nRet = false;

    nRet = cmd_uart_pro_exit_settingmode();

    if(nRet){
        qWarning("退出设定模式失败");
    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

}

/**TODO
 * @brief receiver::slot_iic_save_settings_pro
 * @param iicAddr
 * @param result
 */
void serialportio::slot_iic_save_settings_pro(uint iicAddr, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_iic_pro_enter_settingmode(iicAddr);

    if(nRet)
    {



    }

    nRet = false;

    nRet = cmd_iic_pro_exit_settingmode(iicAddr);

    if(nRet){
        qWarning("退出设定模式失败");
    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

}

void serialportio::slot_do_cmd_id_trans_type_config(unsigned char trans, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_TRANS_TYPE_CONFIG;
    cmd[3] = trans;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_TRANS_TYPE_CONFIG;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_upgrade(const QString fullpath, unsigned int upgrade_frame_length)
{
    QFileInfo info(fullpath);
    QString path = fullpath;
    uint32_t effiect_data_length = upgrade_frame_length - CTRL_DATA_LENGTH;
    path = path.replace("/", "\\");

    qDebug() << info.fileName() << info.size() << path;

    int32_t length = info.size();
    int32_t frame_cnt = (length % effiect_data_length) ? (length / effiect_data_length + 1) : (length / effiect_data_length);

    qDebug() << "frame cnt" << frame_cnt;
    QFile file(path);
    if(false == file.open(QFile::ReadOnly))
    {
        qDebug() << "file open fail" << path;
        emit signalUpgradeResult(false);
        return;
    }

    qDebug() << info.fileName() << "open success!";

    QByteArray cont = file.readAll();
    qDebug() << "cont size" << cont.length();

    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    // 发送第0帧
    uint16_t index = 0;
    QByteArray data;
    data.resize(effiect_data_length);
    data[0] = length & 0xFF;
    data[1] = (length >> 8) & 0xFF;

    if(false == send_do_cmd_id_upgrade(upgrade_frame_length, 0, data, frame_cnt))
    {
        emit signalUpgradeResult(false);
        return;
    }

    uint32_t step = (frame_cnt > 10000) ? 100 : 10;
    for(index = 1; index <= frame_cnt; index++)
    {
        cont = cont.right(length - (index - 1) * effiect_data_length);

        if(false == send_do_cmd_id_upgrade(upgrade_frame_length, index, cont, frame_cnt))
        {
            emit signalUpgradeResult(false);
            return;
        }

        if(!(index % step) || (index + 10 >frame_cnt))
        {
            qDebug() << index << frame_cnt;
            emit sigUpdateProcessbar(index * 1.0 / frame_cnt);

        }
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    qDebug() << "upgrade success";
    file.close();
    emit signalUpgradeResult(true);
}

bool serialportio::send_do_cmd_id_upgrade(unsigned int upgrade_frame_length, unsigned short index, QByteArray data, unsigned short frame_cnt)
{
    QByteArray cmd;
    cmd.resize(upgrade_frame_length);
    cmd[0] = 0x5A;
    cmd[1] = upgrade_frame_length;
    cmd[2] = ID_UPGRADE;
    cmd[3] = index & 0xFF;
    cmd[4] = (index >> 8) & 0xFF;

    for(unsigned int index = 0; index < upgrade_frame_length - CTRL_DATA_LENGTH; index++)
    {
        cmd[index + 5] = data.at(index);
    }

    cmd[upgrade_frame_length - 1] = calc_checksum(cmd);

    serialPortWrite(cmd);

    if(index == frame_cnt)
    {
        return true;
    }

    int cnt = 0;
    QByteArray ack;
    QByteArray ba_read;
    int32_t start;
    while(1)
    {
        serialPort_->waitForReadyRead(1000);
        ba_read = serialPort_->readAll();
        cnt = (ba_read.length() == 0) ? (cnt + 1) : cnt;
        if(3 <= cnt)
        {
            //break;
        }
        ack.append(ba_read);

        if(ack.length() < 5)
        {
            continue;
        }
        for(start = 0; start < ack.length() - 3; start++)
        {
            if((ack.at(start) == 0x5A) && (ack.at(start + 1) == 5) && (ack.at(start + 2) == ID_UPGRADE) && (ack.at(start + 3) == 0))
            {
                return true;
            }
        }
    }

    if((3 == cnt) || (start == ack.length() - 3))
    {
        qDebug() << "rcv ack error" << ack.toHex();
        return false;
    }
    return true;
}

uint16_t serialportio::crc16_calc(QByteArray data, uint32_t length)
{
    uint8_t *start = (uint8_t *)data.data();
    uint32_t len = length;
    uint16_t index;                     // CRC 查找表的指针
    uint8_t crc_high = 0xFF;            // CRC 高字节的初始化
    uint8_t crc_low = 0xFF;             // CRC 低字节的初始化

    while (len--)
    {
        index = crc_low ^ *start++;     // 计算 CRC
        crc_low = crc_high ^ auch_crc_high[index];
        crc_high = auch_crc_low[index];
    }
    return(crc_high << 8 | crc_low);
}

void serialportio::slot_do_cmd_id_gain_adjust_enable(unsigned char enable, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_GAIN_ADJUST_ENABLE;
    cmd[3] = enable;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_GAIN_ADJUST_ENABLE;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_can_snd_id(unsigned int value, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd[0] = 0x5A;
    cmd[1] = 8;
    cmd[2] = ID_CAN_SND_ID;
    cmd[3] = value & 0xFF;
    cmd[4] = (value >> 8) & 0xFF;
    cmd[5] = (value >> 16) & 0xFF;
    cmd[6] = (value >> 24) & 0xFF;
    cmd[7] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_CAN_SND_ID;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_can_rcv_id(unsigned int value, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = 0x5A;
    cmd[1] = 8;
    cmd[2] = ID_CAN_RCV_ID;
    cmd[3] = value & 0xFF;
    cmd[4] = (value >> 8) & 0xFF;
    cmd[5] = (value >> 16) & 0xFF;
    cmd[6] = (value >> 24) & 0xFF;
    cmd[7] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_CAN_RCV_ID;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_can_baudrate(unsigned int value, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = 0x5A;
    cmd[1] = 8;
    cmd[2] = ID_CAN_BAUDRATE;
    cmd[3] = value & 0xFF;
    cmd[4] = (value >> 8) & 0xFF;
    cmd[5] = (value >> 16) & 0xFF;
    cmd[6] = (value >> 24) & 0xFF;
    cmd[7] = calc_checksum(cmd);


    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_CAN_BAUDRATE;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_version(unsigned char *ba_version, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray version, cmd;
    cmd.resize(4);
    cmd[0] = 0x5A;
    cmd[1] = 4;
    cmd[2] = ID_GET_VERSION;
    cmd[3] = calc_checksum(cmd);

    qDebug() << __FUNCTION__ << cmd.toHex();
    serialPortWrite(cmd);

    int cnt = 0;
    result = false;
    QByteArray ba_read;

    QByteArray ack_head;
    ack_head.resize(3);
    ack_head[0] = 0x5A;
    ack_head[1] = 7;
    ack_head[2] = ID_GET_VERSION;
    while(1)
    {
        serialPort_->waitForReadyRead(2000);
        ba_read = serialPort_->readAll();
        qDebug() <<"data rcv=>" << ba_read.toHex();

        if(2 <= cnt++)
        {
            break;
        }
        version.append(ba_read);

        if(version.length() < 7)
        {
            continue;
        }

        if(false == version.contains(ack_head))
        {
            continue;
        }

        uint32_t index = version.indexOf(ack_head);
        if(version.right(version.length() - index).length() < 7)
        {
            version = version.right(version.length() - index);
            continue;
        }
        version = version.mid(index, 7);
        break;
    }

    if(cnt >= 2)
    {
        result = false;
    }else{
        result = true;
        ba_version[0] = (uint8_t)version.at(5);
        ba_version[1] = (uint8_t)version.at(4);
        ba_version[2] = (uint8_t)version.at(3);
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_version_pro(unsigned char *ba_version, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();

    if(nRet)
    {
        QByteArray version, cmd;
        cmd.resize(8);
        cmd[0] = 0xAA;
        cmd[1] = 0x55;
        cmd[2] = 0xF0;
        cmd[3] = 0x00;
        cmd[4] = 0x00;
        cmd[5] = 0x00;
        cmd[6] = 0x00;
        cmd[7] = 0xA0;

        qDebug() << __FUNCTION__ << cmd.toHex();
        serialPortWrite(cmd);

        int cnt = 0;
        result = false;
        QByteArray ba_read;

        QByteArray ack_head;
        ack_head.resize(5);
        ack_head[0] = 0xAA;
        ack_head[1] = 0x55;
        ack_head[2] = 0xF0;
        ack_head[3] = 0x00;
        ack_head[4] = 0x00;

        while(1)
        {
            serialPort_->waitForReadyRead(2000);
            ba_read = serialPort_->readAll();
            qDebug() << ba_read.toHex();

            if(2 <= cnt++)
            {
                break;
            }
            version.append(ba_read);

            if(version.length() < 8)
            {
                continue;
            }

            if(false == version.contains(ack_head))
            {
                continue;
            }

            uint32_t index = version.indexOf(ack_head);
            if(version.right(version.length() - index).length() < 8)
            {
                version = version.right(version.length() - index);
                continue;
            }
            version = version.mid(index, 8);
            break;
        }

        if(cnt >= 2)
        {
            result = false;
        }else{
            result = true;
            ba_version[0] = (uint8_t)version.at(5);
            ba_version[1] = (uint8_t)version.at(6);
            ba_version[2] = (uint8_t)version.at(7);
        }

        nRet = false;

        nRet = cmd_uart_pro_exit_settingmode();

        if(nRet){
            qWarning("退出设定模式失败");
        }


    }



    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_full_version(unsigned char *ba_version, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray version, cmd;
    cmd.resize(4);
    cmd[0] = 0x5A;
    cmd[1] = 4;
    cmd[2] = ID_GET_VERSION_FULL;
    cmd[3] = calc_checksum(cmd);

    qDebug() << __FUNCTION__ << cmd.toHex();
    serialPortWrite(cmd);

    int cnt = 0;
    result = false;
    QByteArray ba_read;

    QByteArray ack_head;
    ack_head.resize(3);
    ack_head[0] = 0x5A;
    ack_head[1] = RES_LEN_ID_FULL_V;
    ack_head[2] = ID_GET_VERSION_FULL;
    while(1)
    {
        serialPort_->waitForReadyRead(2000);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        if(2 <= cnt++)
        {
            break;
        }
        version.append(ba_read);

        if(version.length() < RES_LEN_ID_FULL_V)
        {
            continue;
        }

        if(false == version.contains(ack_head))
        {
            continue;
        }

        uint32_t index = version.indexOf(ack_head);
        if(version.right(version.length() - index).length() < RES_LEN_ID_FULL_V)
        {
            version = version.right(version.length() - index);
            continue;
        }
        version = version.mid(index, RES_LEN_ID_FULL_V);
        break;
    }

    if(cnt >= 2)
    {
        result = false;
    }else{
        result = true;
        for(uint32_t i = 0; i < 26; i++)
        {
            ba_version[i] = (uint8_t)version.at(i + 3);
        }
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_iic_frame_rate(uint iicAddress, unsigned int fps, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd,ackhead;

    cmd.resize(14);

    cmd[0] = 0x53;
    //iic addr
    cmd[1] = iicAddress *2;
    cmd[2] = 0x06;

    cmd[3] = 0x5A;
    cmd[4] = 6;
    cmd[5] = 0x03;
    cmd[6] = fps & 0xFF;
    cmd[7] = (fps >> 8) & 0xFF;
    cmd[8] = calc_checksum(cmd);

    cmd[9] = 0x50;


    cmd[10] = 0x53;
    cmd[11] = iicAddress *2 +1;
    cmd[12] = 0x09;
    cmd[13] = 0x50;



    int cnt = 0;
    result = false;

    ackhead.resize(6);

    ackhead[0] = 0x5A;
    ackhead[1] = 0x06;
    ackhead[2] = 0x03;
    ackhead[3] = 0x00;
    ackhead[4] = 0x00;
    ackhead[5] = 0x63;

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ackhead);

        if(result)
        {
            break;
        }

        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_iic_frame_rate_pro(uint iicAddress, unsigned int fps, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_iic_pro_enter_settingmode(iicAddress);

    if(nRet>0)
    {
        QByteArray cmd,ackhead;

        cmd.resize(14);

        cmd[0] = 0x53;
        //iic addr
        cmd[1] = iicAddress *2;
        cmd[2] = 0x06;

        cmd[3] = 0x5A;
        cmd[4] = 6;
        cmd[5] = 0x03;
        cmd[6] = fps & 0xFF;
        cmd[7] = (fps >> 8) & 0xFF;
        cmd[8] = calc_checksum(cmd);

        cmd[9] = 0x50;


        cmd[10] = 0x53;
        cmd[11] = iicAddress *2 +1;
        cmd[12] = 0x09;
        cmd[13] = 0x50;



        int cnt = 0;
        result = false;

        ackhead.resize(6);

        ackhead[0] = 0x5A;
        ackhead[1] = 0x06;
        ackhead[2] = 0x03;
        ackhead[3] = 0x00;
        ackhead[4] = 0x00;
        ackhead[5] = 0x63;

        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(ackhead);

            if(result)
            {
                break;
            }

            cnt++;
        }

        nRet = false;

        nRet = cmd_iic_pro_exit_settingmode(iicAddress);

        if(nRet){
            qWarning("退出设定模式失败");
        }
    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_frame_rate(unsigned int fps, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(6);
    cmd[0] = 0x5A;
    cmd[1] = 6;
    cmd[2] = ID_SAMPLE_FREQ;
    cmd[3] = fps & 0xFF;
    cmd[4] = (fps >> 8) & 0xFF;
    cmd[5] = calc_checksum(cmd);

    int cnt = 0;
    result = false;
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(cmd);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_frame_rate_pro(unsigned int fps, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    bool nRet = cmd_uart_pro_enter_settingmode();

    if(nRet)
    {
        QByteArray cmd;
        cmd.resize(6);
        cmd[0] = 0x5A;
        cmd[1] = 6;
        cmd[2] = 0x03;
        cmd[3] = fps & 0xFF;
        cmd[4] = (fps >> 8) & 0xFF;
        cmd[5] = calc_checksum(cmd);

        int cnt = 0;
        result = false;
        while((false == result) && (cnt < 2))
        {
            serialPortWrite(cmd);
            result = read_ack(cmd);
            cnt++;
        }
    }


    nRet = false;

    nRet = cmd_uart_pro_exit_settingmode();

    if(nRet){
        qWarning("退出设定模式失败");
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_Custom_choise(unsigned char custom, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_CUSTOM_CHOSE;
    cmd[3] = custom;
    cmd[4] = calc_checksum(cmd);


    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_CUSTOM_CHOSE;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_can_frame_type_config(unsigned char type, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_CAN_FRAME_TYPE;
    cmd[3] = type;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_CAN_FRAME_TYPE;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_dist_io_threshold(unsigned short value1, unsigned short value2, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = 0x5A;
    cmd[1] = 8;
    cmd[2] = ID_IO_DIST_THRESHOLD;
    cmd[3] = value1 & 0xFF;
    cmd[4] = (value1 >> 8) & 0xFF;
    cmd[5] = value2 & 0xFF;
    cmd[6] = (value2 >> 8) & 0xFF;
    cmd[7] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_IO_DIST_THRESHOLD;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_near_io_value(unsigned char value, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_IO_LEVEL_NEAR;
    cmd[3] = (uint8_t)value;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_IO_LEVEL_NEAR;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_io_delay(unsigned short value1, unsigned short value2, bool &result)
{
    qDebug() << value1 << value2;
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = 0x5A;
    cmd[1] = 8;
    cmd[2] = ID_IO_OUTPUT_DELAY;
    cmd[3] = value1 & 0xFF;
    cmd[4] = (value1 >> 8) & 0xFF;
    cmd[5] = value2 & 0xFF;
    cmd[6] = (value2 >> 8) & 0xFF;
    cmd[7] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_IO_OUTPUT_DELAY;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_fog_feature_enable(unsigned char enable, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_FOG_FEATURE_ENABLE;
    cmd[3] = enable;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_FOG_FEATURE_ENABLE;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_offset_config(unsigned short value, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(6);
    cmd[0] = 0x5A;
    cmd[1] = 6;
    cmd[2] = ID_OFFSET_CALI_CONF;
    cmd[3] = value & 0xFF;
    cmd[4] = (value >> 8) & 0xFF;
    cmd[5] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_OFFSET_CALI_CONF;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

/**设置波特率
 * @brief receiver::slot_do_cmd_id_mb_baudrate_config
 * @param dev_addr
 * @param baudrate
 * @param result
 */
void serialportio::slot_do_cmd_id_mb_baudrate_config(unsigned char dev_addr, unsigned int baudrate, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    QByteArray cmd2;

    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x83;
    cmd[4] = (baudrate >> 24) & 0xFF;
    cmd[5] = (baudrate >> 16) & 0xFF;
    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    cmd2.resize(8);
    cmd2[0] = dev_addr;
    cmd2[1] = 0x06;
    cmd2[2] = 0x00;
    cmd2[3] = 0x84;
    cmd2[4] =(baudrate >> 8) & 0xFF;
    cmd2[5] =  baudrate & 0xFF;
    uint16_t crc2 = crc16_calc(cmd2, 6);
    cmd2[6] = crc2 & 0xFF;
    cmd2[7] = (crc2 >> 8) & 0xFF;


    int cnt = 0;
    result = false;
    while((false == result) && (cnt < 2))
    {
        int nSend = serialPortWrite(cmd);
        int nSend2 = serialPortWrite(cmd2);

        if(nSend>0&&nSend2>0)
        {
             result = true;
             break;
        }

        cnt++;
    }


    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

/**
 * @brief receiver::slot_do_cmd_id_mb_dev_addr_config
 * @param dev_addr
 * @param mb_dev_addr
 * @param result
 */
void serialportio::slot_do_cmd_id_mb_dev_addr_config(unsigned char dev_addr, unsigned char mb_dev_addr, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x85;
    cmd[4] = (mb_dev_addr >> 8) & 0xFF;
    cmd[5] = mb_dev_addr & 0xFF;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head(cmd);

    qDebug() << cmd.toHex() << ack_head.toHex();

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

/**获取距离
 * @brief receiver::slot_do_cmd_id_mb_get_dist
 * @param dev_addr
 * @param dist
 * @param result
 */
void serialportio::slot_do_cmd_id_mb_get_dist(unsigned char dev_addr, unsigned short &dist, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x03;
    cmd[2] = 0x00;
    cmd[3] = 0x00;
    cmd[4] = 0x00;
    cmd[5] = 0x01;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    QByteArray ba_read;
    QByteArray all_read;
    int32_t index;

    ack_head.resize(3);
    ack_head[0] = dev_addr;
    ack_head[1] = 0x03;
    ack_head[2] = 0x02;

    serialPortWrite(cmd);

    while(1)
    {
        serialPort_->waitForReadyRead(100);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        if(2 <= cnt++)
        {
            break;
        }
        all_read.append(ba_read);

        if(all_read.length() < 7)
        {
            continue;
        }

        index = all_read.indexOf(ack_head);

        if(0 > index)
        {
            continue;
        }

        if(all_read.right(all_read.length() - index).length() < 7)
        {
            all_read = all_read.right(all_read.length() - index);
            continue;
        }

        all_read = all_read.mid(index, 7);

        if(crc16_calc(all_read.left(5), 5) != (uint16_t)((unsigned char)all_read.at(5) + (unsigned short)(all_read.at(6) << 8)))
        {
            qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(5), 5) << ", ack:" << (uint16_t)((unsigned char)all_read.at(5) + (unsigned short)(all_read.at(6) << 8));
            cnt = 2;
            break;
        }

        break;

    }

    if(cnt >= 2)
    {
        result = false;
    }else{
        result = true;
        emit signalDataRcv(all_read);
        dist = ((uint16_t)all_read.at(4) & 0x00ff | ((uint16_t)all_read.at(3) << 8) & 0xff00);
        qDebug() << "dist: " << dist;

    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


/**发送获取距离和信号的数据
 * @brief receiver::slot_do_cmd_id_mb_get_dist
 * @param dev_addr
 * @param dist
 * @param result
 */
void serialportio::slot_mb_get_distandsignal (uint dev_addr, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x03;
    cmd[2] = 0x00;
    cmd[3] = 0x00;
    cmd[4] = 0x00;
    cmd[5] = 0x02;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;

    QByteArray ack_head;
    QByteArray ba_read;
    QByteArray all_read;
    int32_t index;

    ack_head.resize(3);
    ack_head[0] = dev_addr;
    ack_head[1] = 0x03;
    ack_head[2] = 0x04;

    int nSendCnt = serialPortWrite(cmd);

    while(1)
    {
        serialPort_->waitForReadyRead(100);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        //循环2次
        if(10 <= cnt++)
        {
            break;
        }

        all_read.append(ba_read);

        if(all_read.length() < 9)
        {
            continue;
        }

        index = all_read.indexOf(ack_head);

        if(0 > index)
        {
            continue;
        }

        if(all_read.right(all_read.length() - index).length() < 9)
        {
            all_read = all_read.right(all_read.length() - index);
            continue;
        }

        all_read = all_read.mid(index, 9);

        if(crc16_calc(all_read.left(7), 7) != (uint16_t)((unsigned char)all_read.at(7) + (unsigned short)(all_read.at(8) << 8)))
        {
            qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(7), 7) << ", ack:" << (uint16_t)((unsigned char)all_read.at(7) + (unsigned short)(all_read.at(8) << 8));
            cnt = 10;
            break;
        }


        //合格的一帧数据
        emit signalDataRcv(all_read);



        break;
    }

    if(cnt >= 10)
    {
        result = false;

    }
    else
    {
        result = true;

        uint dist = ((uint16_t)all_read.at(4) & 0x00ff | ((uint16_t)all_read.at(3) << 8) & 0xff00);
        int amp = (uint16_t)all_read.at(6) + ((uint16_t)all_read.at(5) << 8);

        passDist_.append(dist);
        passAmp_.append(amp);
        passRxFrame_.append(all_read);

        //freq_ = 10;

        emit signalDistance(passDist_,passAmp_,passRxFrame_,iicFreq_);

        passDist_.clear();
        passAmp_.clear();


    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


void serialportio::slot_do_cmd_id_mb_modbus_disable(unsigned char dev_addr, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x82;
    cmd[4] = 0x00;
    cmd[5] = 0x01;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head(cmd);

    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_mb_save_config(unsigned char dev_addr, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    uint16_t crc;
    QByteArray cmd,ba_read,all_read;
    cmd.resize(8);
    cmd[0] = dev_addr;
    cmd[1] = 0x06;
    cmd[2] = 0x00;
    cmd[3] = 0x80;
    cmd[4] = 0x00;
    cmd[5] = 0x00;

    crc = crc16_calc(cmd, 6);
    cmd[6] = crc & 0xFF;
    cmd[7] = (crc >> 8) & 0xFF;

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    int index = 0;
    result = false;
    QByteArray ack_head(cmd);

    int nSendCnt = serialPortWrite(cmd);

//    ack_head.resize(5);
//    ack_head[0] = dev_addr;
//    ack_head[1] = 0x06;
//    ack_head[3] = 0x00;
//    ack_head[4] = 0x80;

    while(1)
    {
        serialPort_->waitForReadyRead(100);
        ba_read = serialPort_->readAll();
        qDebug() << ba_read.toHex();

        if(2 <= cnt++)
        {
            break;
        }
        all_read.append(ba_read);

        if(all_read.length() < 8)
        {
            continue;
        }

        index = all_read.indexOf(ack_head);

        if(0 > index)
        {
            continue;
        }

        if(all_read.right(all_read.length() - index).length() < 8)
        {
            all_read = all_read.right(all_read.length() - index);
            continue;
        }

        all_read = all_read.mid(index, 8);

        if(crc16_calc(all_read.left(6), 6) != (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8)))
        {
            qDebug() << "error: crc error! calc:" << crc16_calc(all_read.left(5), 5) << ", ack:" << (uint16_t)((unsigned char)all_read.at(6) + (unsigned short)(all_read.at(7) << 8));
            cnt = 2;
            break;
        }

        break;

    }

    if(cnt >= 2)
    {
        result = false;
    }else{
        result = true;
        emit signalDataRcv(all_read);
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_dac_map_type(unsigned char type, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = ID_DAC_MAP_TYPE;
    cmd[3] = type;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    int cnt = 0;
    result = false;
    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_DAC_MAP_TYPE;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 2))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}


int serialportio::serialPortWrite(QByteArray cmd)
{
     int bytesSendCnt = serialPort_->write(cmd);

     if(bytesSendCnt>0)
     {
         emit signalSerialCmdSend(cmd);
     }

     return bytesSendCnt;

}

void serialportio::slot_do_cmd_id_modbus_enable(bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 5;
    cmd[2] = 0x6F;
    cmd[3] = 00;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    result = false;
    int cnt = 0;

    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x05;
    ack_head[2] = 0x6F;
    ack_head[3] = 0x00;
    ack_head[4] = calc_checksum(ack_head);
    while((false == result) && (cnt < 3))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }


    if(result)
    {
        if(ack_head.at(3)!=0){
            result = false;
        }
        else {
            result = true;
        }
    }

    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slot_do_cmd_id_modbus_dev_addr_set(unsigned char addr, bool &result)
{
    disconnect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));

    QByteArray cmd;
    cmd.resize(5);
    cmd[0] = 0x5A;
    cmd[1] = 0x05;
    cmd[2] = ID_MODBUS_ADDR_CONF;
    cmd[3] = addr & 0xFF;
    cmd[4] = calc_checksum(cmd);

    qDebug() << endl << __FUNCTION__ << cmd.toHex();

    result = false;
    int cnt = 0;

    QByteArray ack_head;
    ack_head.resize(5);
    ack_head[0] = 0x5A;
    ack_head[1] = 0x5;
    ack_head[2] = ID_MODBUS_ADDR_CONF;
    ack_head[3] = 0;
    ack_head[4] = calc_checksum(ack_head);

    while((false == result) && (cnt < 5))
    {
        serialPortWrite(cmd);
        result = read_ack(ack_head);
        cnt++;
    }
    connect(serialPort_, SIGNAL(readyRead()), this, SLOT(slotReceiver()));
}

void serialportio::slotOpen(QString _serialSelected, int _baudRate, bool &_result)
{
    if(serialPort_->isOpen())
        serialPort_->close();

    serialPort_->setPortName(_serialSelected);
    serialPort_->setBaudRate(_baudRate);
    serialPort_->setReadBufferSize(0);
    serialPort_->setDataBits(QSerialPort::DataBits::Data8);
    serialPort_->setParity(QSerialPort::Parity::NoParity);
    serialPort_->setStopBits(QSerialPort::StopBits::OneStop);
    serialPort_->setFlowControl(QSerialPort::FlowControl::NoFlowControl);
    serialPort_->open(QIODevice::ReadWrite);

    if(serialPort_->isOpen())
    {
        qDebug() << "serial opened success ,frame frequency timer will start ";
        timer_->start(1000);
        numFrame_ = 0;
        _result = true;
    }
    else
    {
        qDebug() << "opened fail";
        serialPort_->close();
        _result = false;
    }
}

void serialportio::slotClose(bool &_result)
{
    try
    {
        serialPort_->close();
        _result = true;
    }
    catch (QString *_excep)
    {
        _result = false;
    }
}

void serialportio::slotReceiver()
{
    int16_t dist;
    //信号强度
    int16_t amp;
    int32_t idx = 0;


    QByteArray cmd = serialPort_->readAll();
    QByteArray localAppend = dataBuffer_.append(cmd);

    //过滤帧
    int idindex = cmd.indexOf(dataHeader_, 0);
    int idindex1 = cmd.indexOf(0x5A, 0);

    if(idindex<0&&idindex1<0){
        qDebug("useless frame, drop it =>" + cmd.toHex(' '));
        return;
    }

    //emit signalDataRcv(cmd);

    if(idindex>=0) //59 59 9字节
    {
        for (; (idx = dataBuffer_.indexOf(dataHeader_, idx)) >= 0 && (idx + 10) < dataBuffer_.length(); idx++)
        {
            char chk = 0x59 + 0x59;
            for (int i = 2; i < 8; i++)
            {
                chk += dataBuffer_.at(idx + i);
            }

            if (chk == dataBuffer_.at(idx + 8))
            {
                dist = (uint16_t)((dataBuffer_.at(idx + 3) & 0xff) << 8 | (dataBuffer_.at(idx + 2) & 0xff));
                amp = (uint16_t)((dataBuffer_.at(idx + 5) & 0xff) << 8 | (dataBuffer_.at(idx + 4) & 0xff));
                passDist_.push_back(dist);
                passAmp_.push_back(amp);

                QByteArray rxFrame ;

                for (int i = 0;i<9;i++) {
                      rxFrame[i] = dataBuffer_[idx+i];
                }
                passRxFrame_.push_back(rxFrame.toHex(' '));
                numFrame_++;
            }
        }
    }


    //存满10=》max_dist_num 帧，发送一次信号
    if (passDist_.length() > max_dist_num){
        emit signalDistance(passDist_,passAmp_,passRxFrame_,freq_);
		passAmp_.clear();
		passRxFrame_.clear();
        passDist_.clear();
    }

    dataBuffer_.remove(0, idx);
}

void serialportio::slotCalcFreq()
{
    freq_ = numFrame_;
    numFrame_ = 0;
}

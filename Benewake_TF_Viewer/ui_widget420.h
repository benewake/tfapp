/********************************************************************************
** Form generated from reading UI file 'widget420.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET420_H
#define UI_WIDGET420_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget420
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QPushButton *btnMaxValSet;
    QLineEdit *etMaxVal;
    QLabel *label_2;
    QLabel *label_4;
    QLineEdit *etMinVal;
    QPushButton *btnMinValSet;
    QPushButton *btnSwitchMode;
    QComboBox *cbMode;
    QLabel *label;
    QGridLayout *layoutUart;
    QPushButton *btnSaveConfig;
    QComboBox *cbBaudrate;
    QPushButton *btnFrameFreq;
    QLabel *lbFactoryResetResult;
    QComboBox *cbLowConsumption;
    QPushButton *btnOutputFormat;
    QLabel *lbConfigResult;
    QLabel *lbResetResult;
    QComboBox *cbOutSwtich;
    QComboBox *cbOutputFormat;
    QPushButton *btnBaudrate;
    QLabel *lbVersion;
    QComboBox *cbFrameFreq;
    QPushButton *btnFallbackToFactory;
    QPushButton *btnOutSwitch;
    QPushButton *btnSystemReset;
    QPushButton *btnGetVersion;
    QPushButton *btnLowConsumption;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *Widget420)
    {
        if (Widget420->objectName().isEmpty())
            Widget420->setObjectName(QString::fromUtf8("Widget420"));
        Widget420->resize(486, 542);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        Widget420->setFont(font);
        gridLayout = new QGridLayout(Widget420);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(0);
        gridLayout->setVerticalSpacing(10);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(Widget420);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setFamily(QString::fromUtf8("Arial"));
        font1.setPointSize(9);
        groupBox->setFont(font1);
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(8);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(8, 8, 8, 8);
        btnMaxValSet = new QPushButton(groupBox);
        btnMaxValSet->setObjectName(QString::fromUtf8("btnMaxValSet"));
        btnMaxValSet->setMinimumSize(QSize(0, 30));
        btnMaxValSet->setFont(font1);

        gridLayout_2->addWidget(btnMaxValSet, 3, 2, 1, 1);

        etMaxVal = new QLineEdit(groupBox);
        etMaxVal->setObjectName(QString::fromUtf8("etMaxVal"));
        etMaxVal->setMinimumSize(QSize(0, 30));
        etMaxVal->setMaximumSize(QSize(150, 16777215));
        etMaxVal->setFont(font1);

        gridLayout_2->addWidget(etMaxVal, 3, 1, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);
        label_2->setFont(font1);
        label_2->setLayoutDirection(Qt::LeftToRight);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_2, 2, 0, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy1.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy1);
        label_4->setFont(font1);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_4, 3, 0, 1, 1);

        etMinVal = new QLineEdit(groupBox);
        etMinVal->setObjectName(QString::fromUtf8("etMinVal"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(etMinVal->sizePolicy().hasHeightForWidth());
        etMinVal->setSizePolicy(sizePolicy2);
        etMinVal->setMinimumSize(QSize(100, 30));
        etMinVal->setMaximumSize(QSize(150, 16777215));
        etMinVal->setFont(font1);

        gridLayout_2->addWidget(etMinVal, 2, 1, 1, 1);

        btnMinValSet = new QPushButton(groupBox);
        btnMinValSet->setObjectName(QString::fromUtf8("btnMinValSet"));
        btnMinValSet->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnMinValSet, 2, 2, 1, 1);

        btnSwitchMode = new QPushButton(groupBox);
        btnSwitchMode->setObjectName(QString::fromUtf8("btnSwitchMode"));
        btnSwitchMode->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSwitchMode, 1, 2, 1, 1);

        cbMode = new QComboBox(groupBox);
        cbMode->addItem(QString());
        cbMode->addItem(QString());
        cbMode->setObjectName(QString::fromUtf8("cbMode"));
        sizePolicy2.setHeightForWidth(cbMode->sizePolicy().hasHeightForWidth());
        cbMode->setSizePolicy(sizePolicy2);
        cbMode->setMinimumSize(QSize(0, 30));
        cbMode->setFont(font1);

        gridLayout_2->addWidget(cbMode, 1, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setFont(font1);
        label->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox, 1, 0, 1, 1);

        layoutUart = new QGridLayout();
        layoutUart->setObjectName(QString::fromUtf8("layoutUart"));
        layoutUart->setHorizontalSpacing(10);
        layoutUart->setVerticalSpacing(20);
        layoutUart->setContentsMargins(5, 5, 5, 5);
        btnSaveConfig = new QPushButton(Widget420);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnSaveConfig, 2, 2, 1, 1);

        cbBaudrate = new QComboBox(Widget420);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbBaudrate, 0, 0, 1, 1);

        btnFrameFreq = new QPushButton(Widget420);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnFrameFreq, 1, 1, 1, 1);

        lbFactoryResetResult = new QLabel(Widget420);
        lbFactoryResetResult->setObjectName(QString::fromUtf8("lbFactoryResetResult"));
        sizePolicy1.setHeightForWidth(lbFactoryResetResult->sizePolicy().hasHeightForWidth());
        lbFactoryResetResult->setSizePolicy(sizePolicy1);
        lbFactoryResetResult->setMinimumSize(QSize(0, 30));
        lbFactoryResetResult->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbFactoryResetResult, 5, 0, 1, 1);

        cbLowConsumption = new QComboBox(Widget420);
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->setObjectName(QString::fromUtf8("cbLowConsumption"));
        cbLowConsumption->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbLowConsumption, 4, 0, 1, 1);

        btnOutputFormat = new QPushButton(Widget420);
        btnOutputFormat->setObjectName(QString::fromUtf8("btnOutputFormat"));
        btnOutputFormat->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnOutputFormat, 3, 1, 1, 1);

        lbConfigResult = new QLabel(Widget420);
        lbConfigResult->setObjectName(QString::fromUtf8("lbConfigResult"));
        lbConfigResult->setMinimumSize(QSize(0, 30));
        lbConfigResult->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbConfigResult, 3, 2, 1, 1);

        lbResetResult = new QLabel(Widget420);
        lbResetResult->setObjectName(QString::fromUtf8("lbResetResult"));
        lbResetResult->setMinimumSize(QSize(0, 30));
        lbResetResult->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbResetResult, 5, 2, 1, 1);

        cbOutSwtich = new QComboBox(Widget420);
        cbOutSwtich->addItem(QString());
        cbOutSwtich->addItem(QString());
        cbOutSwtich->setObjectName(QString::fromUtf8("cbOutSwtich"));
        cbOutSwtich->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbOutSwtich, 2, 0, 1, 1);

        cbOutputFormat = new QComboBox(Widget420);
        cbOutputFormat->addItem(QString());
        cbOutputFormat->setObjectName(QString::fromUtf8("cbOutputFormat"));
        cbOutputFormat->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbOutputFormat, 3, 0, 1, 1);

        btnBaudrate = new QPushButton(Widget420);
        btnBaudrate->setObjectName(QString::fromUtf8("btnBaudrate"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnBaudrate->sizePolicy().hasHeightForWidth());
        btnBaudrate->setSizePolicy(sizePolicy3);
        btnBaudrate->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnBaudrate, 0, 1, 1, 1);

        lbVersion = new QLabel(Widget420);
        lbVersion->setObjectName(QString::fromUtf8("lbVersion"));
        lbVersion->setMinimumSize(QSize(0, 30));
        lbVersion->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbVersion, 1, 2, 1, 1);

        cbFrameFreq = new QComboBox(Widget420);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbFrameFreq, 1, 0, 1, 1);

        btnFallbackToFactory = new QPushButton(Widget420);
        btnFallbackToFactory->setObjectName(QString::fromUtf8("btnFallbackToFactory"));
        btnFallbackToFactory->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnFallbackToFactory, 5, 1, 1, 1);

        btnOutSwitch = new QPushButton(Widget420);
        btnOutSwitch->setObjectName(QString::fromUtf8("btnOutSwitch"));
        btnOutSwitch->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnOutSwitch, 2, 1, 1, 1);

        btnSystemReset = new QPushButton(Widget420);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnSystemReset, 4, 2, 1, 1);

        btnGetVersion = new QPushButton(Widget420);
        btnGetVersion->setObjectName(QString::fromUtf8("btnGetVersion"));
        btnGetVersion->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnGetVersion, 0, 2, 1, 1);

        btnLowConsumption = new QPushButton(Widget420);
        btnLowConsumption->setObjectName(QString::fromUtf8("btnLowConsumption"));
        btnLowConsumption->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnLowConsumption, 4, 1, 1, 1);


        gridLayout->addLayout(layoutUart, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 2, 0, 1, 1);


        retranslateUi(Widget420);

        QMetaObject::connectSlotsByName(Widget420);
    } // setupUi

    void retranslateUi(QWidget *Widget420)
    {
        Widget420->setWindowTitle(QCoreApplication::translate("Widget420", "Form", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Widget420", "\346\250\241\346\213\237\351\207\217\345\217\202\346\225\260\350\256\276\347\275\256", nullptr));
        btnMaxValSet->setText(QCoreApplication::translate("Widget420", "\350\256\276\345\256\232\344\270\212\351\231\220\350\267\235\347\246\273\345\200\274/cm", nullptr));
        etMaxVal->setPlaceholderText(QCoreApplication::translate("Widget420", "\350\257\267\350\276\223\345\205\245\344\270\212\351\231\220\350\267\235\347\246\273\345\200\274cm", nullptr));
        label_2->setText(QCoreApplication::translate("Widget420", "\344\270\213\351\231\220\345\200\274", nullptr));
        label_4->setText(QCoreApplication::translate("Widget420", "\344\270\212\351\231\220\345\200\274", nullptr));
        etMinVal->setPlaceholderText(QCoreApplication::translate("Widget420", "\350\257\267\350\276\223\345\205\245\344\270\213\351\231\220\350\267\235\347\246\273\345\200\274cm", nullptr));
        btnMinValSet->setText(QCoreApplication::translate("Widget420", "\350\256\276\345\256\232\344\270\213\351\231\220\350\267\235\347\246\273\345\200\274/cm", nullptr));
        btnSwitchMode->setText(QCoreApplication::translate("Widget420", "\345\210\207\346\215\242\346\250\241\345\274\217", nullptr));
        cbMode->setItemText(0, QCoreApplication::translate("Widget420", "\346\255\243\345\220\221", nullptr));
        cbMode->setItemText(1, QCoreApplication::translate("Widget420", "\345\217\215\345\220\221", nullptr));

        label->setText(QCoreApplication::translate("Widget420", "\346\250\241\345\274\217\345\210\207\346\215\242", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("Widget420", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("Widget420", "9600", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("Widget420", "14400", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("Widget420", "19200", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("Widget420", "38400", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("Widget420", "56000", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("Widget420", "115200", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("Widget420", "128000", nullptr));
        cbBaudrate->setItemText(7, QCoreApplication::translate("Widget420", "230400", nullptr));
        cbBaudrate->setItemText(8, QCoreApplication::translate("Widget420", "256000", nullptr));
        cbBaudrate->setItemText(9, QCoreApplication::translate("Widget420", "460800", nullptr));
        cbBaudrate->setItemText(10, QCoreApplication::translate("Widget420", "512000", nullptr));
        cbBaudrate->setItemText(11, QCoreApplication::translate("Widget420", "750000", nullptr));
        cbBaudrate->setItemText(12, QCoreApplication::translate("Widget420", "921600", nullptr));

        btnFrameFreq->setText(QCoreApplication::translate("Widget420", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        lbFactoryResetResult->setText(QCoreApplication::translate("Widget420", "\346\230\276\347\244\272\346\230\257\345\220\246\346\201\242\345\244\215", nullptr));
        cbLowConsumption->setItemText(0, QCoreApplication::translate("Widget420", "1", nullptr));
        cbLowConsumption->setItemText(1, QCoreApplication::translate("Widget420", "2", nullptr));
        cbLowConsumption->setItemText(2, QCoreApplication::translate("Widget420", "3", nullptr));
        cbLowConsumption->setItemText(3, QCoreApplication::translate("Widget420", "4", nullptr));
        cbLowConsumption->setItemText(4, QCoreApplication::translate("Widget420", "5", nullptr));
        cbLowConsumption->setItemText(5, QCoreApplication::translate("Widget420", "6", nullptr));
        cbLowConsumption->setItemText(6, QCoreApplication::translate("Widget420", "7", nullptr));
        cbLowConsumption->setItemText(7, QCoreApplication::translate("Widget420", "8", nullptr));
        cbLowConsumption->setItemText(8, QCoreApplication::translate("Widget420", "9", nullptr));
        cbLowConsumption->setItemText(9, QCoreApplication::translate("Widget420", "10", nullptr));

        btnOutputFormat->setText(QCoreApplication::translate("Widget420", "\350\276\223\345\207\272\346\240\274\345\274\217", nullptr));
        lbConfigResult->setText(QCoreApplication::translate("Widget420", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        lbResetResult->setText(QCoreApplication::translate("Widget420", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        cbOutSwtich->setItemText(0, QCoreApplication::translate("Widget420", "\344\275\277\350\203\275", nullptr));
        cbOutSwtich->setItemText(1, QCoreApplication::translate("Widget420", "\345\205\263\351\227\255", nullptr));

        cbOutputFormat->setItemText(0, QCoreApplication::translate("Widget420", "9Byte(mm)", nullptr));

        btnBaudrate->setText(QCoreApplication::translate("Widget420", "\350\256\276\347\275\256\346\263\242\347\211\271\347\216\207", nullptr));
        lbVersion->setText(QCoreApplication::translate("Widget420", "\347\211\210\346\234\254\345\217\267\357\274\232", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("Widget420", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("Widget420", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("Widget420", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("Widget420", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("Widget420", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("Widget420", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("Widget420", "1000", nullptr));

        btnFallbackToFactory->setText(QCoreApplication::translate("Widget420", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        btnOutSwitch->setText(QCoreApplication::translate("Widget420", "\350\276\223\345\207\272\345\274\200\345\205\263", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("Widget420", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        btnGetVersion->setText(QCoreApplication::translate("Widget420", "\350\216\267\345\217\226\347\211\210\346\234\254\345\217\267", nullptr));
        btnLowConsumption->setText(QCoreApplication::translate("Widget420", "\344\275\216\345\212\237\350\200\227\350\256\276\347\275\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget420: public Ui_Widget420 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET420_H

#include "widgetsteeringuart.h"
#include "ui_widgetsteeringuart.h"

#include <QMessageBox>

WidgetSteeringUart::WidgetSteeringUart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetSteeringUart)
{
    ui->setupUi(this);
}

WidgetSteeringUart::~WidgetSteeringUart()
{
    delete ui;
}

void WidgetSteeringUart::SetUartRecerver(serialportio *tReceiver, QString productModal)
{
    this->sensorRecver_ = tReceiver;
    this->mProductModal = productModal;
}

bool WidgetSteeringUart::CheckComState()
{
    if(sensorRecver_==nullptr)
        return false;

    if(!sensorRecver_->isOpen())
    {
        QMessageBox::warning(NULL, "Error", tr(u8"未连接Com口"));
        return false;
    }

    return true;
}

void WidgetSteeringUart::on_btnBaudrate_clicked()
{
    if(!CheckComState()) return;

     uint32_t baudrate;
     QString cont;

     baudrate = ui->cbBaudrate->currentText().toUInt();
     cont.append(tr(u8"配置波特率") + ui->cbBaudrate->currentText());

     if(sensorRecver_->send_cmd_id_baud_rate_pro(baudrate))
     {
         qDebug("波特率设定成功！");

     }else{
         qDebug("波特率设定失败！");
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"波特率设定失败！"));
     }
}

void WidgetSteeringUart::on_btnVersion_clicked()
{

    if(!CheckComState()) return;
     bool result;
     QString s_version;

     s_version = sensorRecver_->send_cmd_id_version_pro(result);
     if(false == result)
     {
         ui->lbVersion->setText(tr(u8"版本号获取失败"));
     }else{
          ui->lbVersion->setText(tr(u8"版本号：")+s_version);
     }
}


void WidgetSteeringUart::on_btnFrameFreq_clicked()
{
    if(!CheckComState()) return;

   if(sensorRecver_->send_cmd_id_frame_rate_pro(ui->cbFrameFreq->currentText().toUShort()))
   {
      qDebug("帧率设定成功！");
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"帧率设定成功"));

   }else{
       qDebug("帧率设定失败！");
       QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"帧率设定失败"));
   }
}


void WidgetSteeringUart::on_btnOutSwitch_clicked()
{
    if(!CheckComState()) return;

    int index = ui->cbOutSwitch->currentIndex();
    E_Output_Enable_Type type;
    if(index==0)
    {
        type = E_Output_Enable_Type::OUTPUT_Enable;
    }
    else
    {
        type = E_Output_Enable_Type::OUTPUT_Disable;
    }


   if(sensorRecver_->send_cmd_id_output_en_pro(type))
   {
      qDebug("设定成功！");
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));
   }
   else
   {
      qDebug("设定失败");
      QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
   }
}

void WidgetSteeringUart::on_btnOutputFormat_clicked()
{
    if(!CheckComState()) return;

    E_OutputFormat_Type outputFormat;

    if(ui->cbOutputFormat->currentIndex()==0)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_9BYTECM;
    }
    else if(ui->cbOutputFormat->currentIndex()==1)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_9BYTEMM;
    }
    else if(ui->cbOutputFormat->currentIndex()==2)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_PIX;
    }


    if(sensorRecver_->send_cmd_id_output_format_pro(outputFormat))
    {
       qDebug("设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));


    }else{
       qDebug("设定失败");
       QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
    }
}



void WidgetSteeringUart::on_btnWiperPeriod_clicked()
{
    if(!CheckComState()) return;

    uint wiperPeriod = ui->etWiperPeriod->text().toUInt();
    if(ui->cbWiperPeriodUnit->currentIndex()==1)
    {
          wiperPeriod = 60 * wiperPeriod;
    }


    if(sensorRecver_->send_cmd_id_set_wiper_peroid(wiperPeriod))
    {
       qDebug("雨刷周期设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷周期设定成功"));

    }else{
        qDebug("雨刷周期设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷周期设定失败"));
    }
}

void WidgetSteeringUart::on_btnCycleCnt_clicked()
{
    if(!CheckComState()) return;

    uint cycleCnt = ui->etCycleCnt->text().toUInt();

    if(cycleCnt>10)
    {
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷往返次数不能超过10次！"));
       return;
    }

    if(sensorRecver_->send_cmd_set_wipercycle(cycleCnt))
    {
       qDebug("雨刷往返次数设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷往返次数设定成功！"));

    }else{
        qDebug("雨刷往返次数设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷往返次数设定失败！"));
    }
}

void WidgetSteeringUart::on_btnStartWiper_clicked()
{

    if(!CheckComState()) return;

    if(sensorRecver_->send_cmd_startwiper())
    {
       qDebug("雨刷启动成功！");
       ui->lbStartResult->setText(tr(u8"雨刷启动成功！"));
      // QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷启动成功！"));

    }else{
        qDebug("雨刷启动失败！");
         ui->lbStartResult->setText(tr(u8"雨刷启动失败！"));
       // QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷启动失败！"));
    }



}

void WidgetSteeringUart::on_btnSaveConfig_clicked()
{
    if(!CheckComState()) return;

    QString cont;
    cont.append(tr(u8"保存配置"));
    ui->lbSaveConfig->setText("");
    if(sensorRecver_->send_cmd_id_save_settings_pro())
    {
        ui->lbSaveConfig->setText(tr(u8"配置保存成功！"));
    }else{
       ui->lbSaveConfig->setText(tr(u8"配置保存失败！"));
    }
}

void WidgetSteeringUart::on_btnSystemReset_clicked()
{
    if(!CheckComState()) return;

    ui->lbSystemReset->setText(tr(u8""));
   if(sensorRecver_->send_cmd_id_soft_reset_pro())
   {
      qDebug("系统复位成功！");
      ui->lbSystemReset->setText(tr(u8"系统复位成功！"));


   }else{
      qDebug("系统复位失败");
      ui->lbSystemReset->setText(tr(u8"系统复位失败！"));
   }

}


void WidgetSteeringUart::on_btnFactoryReset_clicked()
{
    if(!CheckComState()) return;
     if(sensorRecver_->send_cmd_id_restore_default_pro())
     {
         ui->lbFactoryResult->setText(tr(u8"恢复出厂成功"));
     }else{
         ui->lbFactoryResult->setText(tr(u8"恢复出厂失败"));
     }
}

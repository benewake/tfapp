/********************************************************************************
** Form generated from reading UI file 'failconndialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FAILCONNDIALOG_H
#define UI_FAILCONNDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FailConnDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *widgetContent;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QLabel *label_2;
    QLabel *label;
    QPushButton *btnClose;

    void setupUi(QDialog *FailConnDialog)
    {
        if (FailConnDialog->objectName().isEmpty())
            FailConnDialog->setObjectName(QString::fromUtf8("FailConnDialog"));
        FailConnDialog->resize(780, 550);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FailConnDialog->sizePolicy().hasHeightForWidth());
        FailConnDialog->setSizePolicy(sizePolicy);
        FailConnDialog->setModal(false);
        verticalLayout_2 = new QVBoxLayout(FailConnDialog);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        widgetContent = new QWidget(FailConnDialog);
        widgetContent->setObjectName(QString::fromUtf8("widgetContent"));
        verticalLayout = new QVBoxLayout(widgetContent);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(widgetContent);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("QFrame#frame{\n"
"\n"
"  background:#FFFFFF\n"
"\n"
"}"));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Plain);
        frame->setLineWidth(0);
        frame->setMidLineWidth(0);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(280, 260, 221, 21));
        QFont font;
        font.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font.setPointSize(12);
        label_2->setFont(font);
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(250, 160, 291, 51));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font1.setPointSize(24);
        label->setFont(font1);
        btnClose = new QPushButton(frame);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));
        btnClose->setGeometry(QRect(740, 10, 36, 32));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnClose->sizePolicy().hasHeightForWidth());
        btnClose->setSizePolicy(sizePolicy1);
        btnClose->setMinimumSize(QSize(32, 32));
        btnClose->setLayoutDirection(Qt::LeftToRight);
        btnClose->setStyleSheet(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnClose->setIcon(icon);
        btnClose->setIconSize(QSize(24, 24));
        btnClose->setFlat(true);

        verticalLayout->addWidget(frame);


        verticalLayout_2->addWidget(widgetContent);


        retranslateUi(FailConnDialog);

        QMetaObject::connectSlotsByName(FailConnDialog);
    } // setupUi

    void retranslateUi(QDialog *FailConnDialog)
    {
        FailConnDialog->setWindowTitle(QCoreApplication::translate("FailConnDialog", "Connection Fail", nullptr));
        label_2->setText(QCoreApplication::translate("FailConnDialog", "\346\234\252\346\243\200\346\265\213\345\210\260\351\233\267\350\276\276\357\274\214\350\257\267\350\277\236\346\216\245\350\207\263\350\256\276\345\244\207", nullptr));
        label->setText(QCoreApplication::translate("FailConnDialog", "\346\254\242\350\277\216\344\275\277\347\224\250\345\214\227\351\206\222\344\270\212\344\275\215\346\234\272", nullptr));
        btnClose->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class FailConnDialog: public Ui_FailConnDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FAILCONNDIALOG_H

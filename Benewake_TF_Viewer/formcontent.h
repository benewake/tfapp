#ifndef FORMCONTENT_H
#define FORMCONTENT_H

#include <QSerialPortInfo>
#include <QWidget>

#include <QtCharts/QtCharts>
#include <QMessageBox>
#include <QLineSeries>
#include <QTextStream>
#include <QValueAxis>
#include <QChartView>
#include <QDateTime>
#include <QPointF>
#include <QList>
#include <QFile>
#include <QDir>

#include "serialportio.h"
#include "canbusiothread.h"

QT_CHARTS_USE_NAMESPACE

#define MAXDISPTIME         (10)
#define TIM_FREQ_TIMEOUT    (200)
#define FREQ_VEC_LENGTH     (1000 / TIM_FREQ_TIMEOUT * 2)   // 保存2s以内计算的帧率，肯定能得到非0帧率

namespace Ui {
class FormContent;
}

class FormContent : public QWidget
{
    Q_OBJECT

public:
    explicit FormContent(QWidget *parent = nullptr);
    ~FormContent();

    void InitSerialSlots();

     void InitCanbusSlots();

    QString __file_browse();
private:


        Ui::FormContent *ui;

        QWidget* widgetFunc = nullptr;

        QList<QSerialPortInfo> serialList_;

        QString serialSelected_;

        serialportio *sensorRecver_;

        CanBusIoThread* canbusIo;

        QThread *workThread_;

        QChartView *chartView_;

        QChart *displayChart_;

        QLineSeries *dataSeries_;

        QValueAxis *axisX, *axisY;

        QVector<QPointF> oldPoints_, points_, pointsTemp_;

        QTimer *distUpdateTimer_;

        QTimer *distTimerModbus;

        QTimer *distTimerIIC;

        QTimer *timerSerialPortSearch;

        int baudRate_;

        double currentTime_ = 0;

        double maxDistAxis_ = 0.0, dist_ = 0.0;

        int freq_ = 0;

        //huhao add
        double amp_ = 0.0;

        QString bytesRxHex;


        QTime periodTest_;

        QDir pathDir_;

        QFile saveFile_;

        QTextStream outStream_;

        QString filePath_;

        bool recordFlg_ = false;

        //是否加时间戳
        bool recordTimeStampFlg_ = false;

        //记录格式
        bool recordFormat = 0;

        int maxDispTime_ = 10;

        bool suspend_ = false;

        QLabel *label_display;

        uint32_t cmd_cnt;

        uint32_t upgrade_frame_length;

        uint32_t effiect_data_length;

        QTranslator *qtTranslator;

        QVector<uint32_t> v_freq;


        uint32_t v_index;   // v_freq当前的index

        QMap<QString,QList<QString>> mapDatas;




    public slots:
        void on_serialComboBox_currentIndexChanged(const QString &_selectedName);

        void InitDatas();
        void on_cbProductModal_currentIndexChanged(const QString &s);

        //采集显示的slot
        void slotDisplay(QVector<uint16_t> _dist, QVector<uint16_t> _amp, const StringVector& _hexRxFrames,int _freq);


        void slotDistUpdate();
        void slotUpdateProcessbar(float value);
        void slotUpgradeResult(bool result);
        //void slotUpdateProcessbar(float value);
private slots:
        void on_btnConnTF_clicked();
        void on_cbCommContract_currentIndexChanged(const QString &arg1);

        void on_btnOpenFirmware_clicked();
        void on_btnUpgrade_clicked();

        void on_btnSave_clicked();
        void on_btnFileSel_clicked();
        void on_btnSend_clicked();

        void slotUartDataRcv(QByteArray cmdRx);
        void slotUartDataContractRcv(QByteArray cmdRx);

        void on_ckTimeStamp_stateChanged(int arg1);
        void on_rbHex_toggled(bool checked);
        void on_rbAscii_toggled(bool checked);

        void slotUartDataSend(QByteArray cmd);
        void slotBaudrateChanged();

        void on_btnReload_clicked();
        void slotSerialPortUpdate();

		void reboot();




		void on_rbCN_clicked();

		void on_rbEN_clicked();


};

#endif // FORMCONTENT_H

/********************************************************************************
** Form generated from reading UI file 'widgetsteeringiic.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETSTEERINGIIC_H
#define UI_WIDGETSTEERINGIIC_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetSteeringIIC
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QLabel *lbFactoryReset;
    QPushButton *btnFrameFreq;
    QLabel *lbConfigSvResult;
    QLabel *lbResetResult;
    QPushButton *btnSystemReset;
    QComboBox *cbFrameFreq;
    QPushButton *btnSaveConfig;
    QPushButton *btnFactoryReset;
    QLineEdit *etTimerInterval;
    QPushButton *btnStartTimer;
    QPushButton *btnStopTimer;
    QLineEdit *etIICNewAddress;
    QPushButton *btnAddress;
    QLineEdit *etIICAddress;
    QLabel *lbStartWiper_2;
    QPushButton *btnSwiperPeriod;
    QHBoxLayout *horizontalLayout;
    QLineEdit *etWiperPeriod;
    QComboBox *cbWiperPeriodUnit;
    QLineEdit *etCycleCnt;
    QPushButton *btnSetRoundtripCnt;
    QLabel *lbStartWiper;
    QPushButton *btnStartWiper;

    void setupUi(QWidget *WidgetSteeringIIC)
    {
        if (WidgetSteeringIIC->objectName().isEmpty())
            WidgetSteeringIIC->setObjectName(QString::fromUtf8("WidgetSteeringIIC"));
        WidgetSteeringIIC->resize(440, 433);
        gridLayout = new QGridLayout(WidgetSteeringIIC);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lbFactoryReset = new QLabel(WidgetSteeringIIC);
        lbFactoryReset->setObjectName(QString::fromUtf8("lbFactoryReset"));
        lbFactoryReset->setMinimumSize(QSize(0, 30));
        lbFactoryReset->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbFactoryReset, 5, 2, 1, 1);

        btnFrameFreq = new QPushButton(WidgetSteeringIIC);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFrameFreq, 2, 1, 1, 1);

        lbConfigSvResult = new QLabel(WidgetSteeringIIC);
        lbConfigSvResult->setObjectName(QString::fromUtf8("lbConfigSvResult"));
        lbConfigSvResult->setMinimumSize(QSize(0, 30));
        lbConfigSvResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbConfigSvResult, 1, 2, 1, 1);

        lbResetResult = new QLabel(WidgetSteeringIIC);
        lbResetResult->setObjectName(QString::fromUtf8("lbResetResult"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbResetResult->sizePolicy().hasHeightForWidth());
        lbResetResult->setSizePolicy(sizePolicy);
        lbResetResult->setMinimumSize(QSize(0, 30));
        lbResetResult->setMaximumSize(QSize(16777215, 30));
        lbResetResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbResetResult, 3, 2, 1, 1);

        btnSystemReset = new QPushButton(WidgetSteeringIIC);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSystemReset, 2, 2, 1, 1);

        cbFrameFreq = new QComboBox(WidgetSteeringIIC);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 2, 0, 1, 1);

        btnSaveConfig = new QPushButton(WidgetSteeringIIC);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 0, 2, 1, 1);

        btnFactoryReset = new QPushButton(WidgetSteeringIIC);
        btnFactoryReset->setObjectName(QString::fromUtf8("btnFactoryReset"));
        btnFactoryReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFactoryReset, 4, 2, 1, 1);

        etTimerInterval = new QLineEdit(WidgetSteeringIIC);
        etTimerInterval->setObjectName(QString::fromUtf8("etTimerInterval"));
        QSizePolicy sizePolicy1(QSizePolicy::Ignored, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(etTimerInterval->sizePolicy().hasHeightForWidth());
        etTimerInterval->setSizePolicy(sizePolicy1);
        etTimerInterval->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etTimerInterval, 7, 0, 1, 1);

        btnStartTimer = new QPushButton(WidgetSteeringIIC);
        btnStartTimer->setObjectName(QString::fromUtf8("btnStartTimer"));
        btnStartTimer->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStartTimer, 7, 1, 1, 1);

        btnStopTimer = new QPushButton(WidgetSteeringIIC);
        btnStopTimer->setObjectName(QString::fromUtf8("btnStopTimer"));
        btnStopTimer->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStopTimer, 7, 2, 1, 1);

        etIICNewAddress = new QLineEdit(WidgetSteeringIIC);
        etIICNewAddress->setObjectName(QString::fromUtf8("etIICNewAddress"));
        sizePolicy1.setHeightForWidth(etIICNewAddress->sizePolicy().hasHeightForWidth());
        etIICNewAddress->setSizePolicy(sizePolicy1);
        etIICNewAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etIICNewAddress, 1, 0, 1, 1);

        btnAddress = new QPushButton(WidgetSteeringIIC);
        btnAddress->setObjectName(QString::fromUtf8("btnAddress"));
        sizePolicy.setHeightForWidth(btnAddress->sizePolicy().hasHeightForWidth());
        btnAddress->setSizePolicy(sizePolicy);
        btnAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnAddress, 1, 1, 1, 1);

        etIICAddress = new QLineEdit(WidgetSteeringIIC);
        etIICAddress->setObjectName(QString::fromUtf8("etIICAddress"));
        sizePolicy1.setHeightForWidth(etIICAddress->sizePolicy().hasHeightForWidth());
        etIICAddress->setSizePolicy(sizePolicy1);
        etIICAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etIICAddress, 0, 0, 1, 1);

        lbStartWiper_2 = new QLabel(WidgetSteeringIIC);
        lbStartWiper_2->setObjectName(QString::fromUtf8("lbStartWiper_2"));
        lbStartWiper_2->setMinimumSize(QSize(0, 30));
        lbStartWiper_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbStartWiper_2, 0, 1, 1, 1);

        btnSwiperPeriod = new QPushButton(WidgetSteeringIIC);
        btnSwiperPeriod->setObjectName(QString::fromUtf8("btnSwiperPeriod"));
        btnSwiperPeriod->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSwiperPeriod, 3, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(5, 5, 5, 5);
        etWiperPeriod = new QLineEdit(WidgetSteeringIIC);
        etWiperPeriod->setObjectName(QString::fromUtf8("etWiperPeriod"));
        sizePolicy1.setHeightForWidth(etWiperPeriod->sizePolicy().hasHeightForWidth());
        etWiperPeriod->setSizePolicy(sizePolicy1);
        etWiperPeriod->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(etWiperPeriod);

        cbWiperPeriodUnit = new QComboBox(WidgetSteeringIIC);
        cbWiperPeriodUnit->addItem(QString());
        cbWiperPeriodUnit->addItem(QString());
        cbWiperPeriodUnit->setObjectName(QString::fromUtf8("cbWiperPeriodUnit"));
        cbWiperPeriodUnit->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(cbWiperPeriodUnit);


        gridLayout_2->addLayout(horizontalLayout, 3, 0, 1, 1);

        etCycleCnt = new QLineEdit(WidgetSteeringIIC);
        etCycleCnt->setObjectName(QString::fromUtf8("etCycleCnt"));
        sizePolicy1.setHeightForWidth(etCycleCnt->sizePolicy().hasHeightForWidth());
        etCycleCnt->setSizePolicy(sizePolicy1);
        etCycleCnt->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etCycleCnt, 4, 0, 1, 1);

        btnSetRoundtripCnt = new QPushButton(WidgetSteeringIIC);
        btnSetRoundtripCnt->setObjectName(QString::fromUtf8("btnSetRoundtripCnt"));
        btnSetRoundtripCnt->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSetRoundtripCnt, 4, 1, 1, 1);

        lbStartWiper = new QLabel(WidgetSteeringIIC);
        lbStartWiper->setObjectName(QString::fromUtf8("lbStartWiper"));
        lbStartWiper->setMinimumSize(QSize(0, 30));
        lbStartWiper->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbStartWiper, 5, 0, 1, 1);

        btnStartWiper = new QPushButton(WidgetSteeringIIC);
        btnStartWiper->setObjectName(QString::fromUtf8("btnStartWiper"));
        btnStartWiper->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStartWiper, 5, 1, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(WidgetSteeringIIC);

        QMetaObject::connectSlotsByName(WidgetSteeringIIC);
    } // setupUi

    void retranslateUi(QWidget *WidgetSteeringIIC)
    {
        WidgetSteeringIIC->setWindowTitle(QCoreApplication::translate("WidgetSteeringIIC", "Form", nullptr));
        lbFactoryReset->setText(QCoreApplication::translate("WidgetSteeringIIC", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        btnFrameFreq->setText(QCoreApplication::translate("WidgetSteeringIIC", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        lbConfigSvResult->setText(QCoreApplication::translate("WidgetSteeringIIC", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        lbResetResult->setText(QCoreApplication::translate("WidgetSteeringIIC", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("WidgetSteeringIIC", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetSteeringIIC", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetSteeringIIC", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetSteeringIIC", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetSteeringIIC", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetSteeringIIC", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetSteeringIIC", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetSteeringIIC", "1000", nullptr));

        btnSaveConfig->setText(QCoreApplication::translate("WidgetSteeringIIC", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        btnFactoryReset->setText(QCoreApplication::translate("WidgetSteeringIIC", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        etTimerInterval->setPlaceholderText(QCoreApplication::translate("WidgetSteeringIIC", "\350\275\256\350\257\242\346\227\266\351\227\264ms", nullptr));
        btnStartTimer->setText(QCoreApplication::translate("WidgetSteeringIIC", "\345\274\200\345\247\213\350\275\256\350\257\242", nullptr));
        btnStopTimer->setText(QCoreApplication::translate("WidgetSteeringIIC", "\345\201\234\346\255\242\350\275\256\350\257\242", nullptr));
        etIICNewAddress->setPlaceholderText(QCoreApplication::translate("WidgetSteeringIIC", "\350\257\267\350\276\223\345\205\245\346\226\260\347\232\204IIC\345\234\260\345\235\200", nullptr));
        btnAddress->setText(QCoreApplication::translate("WidgetSteeringIIC", "\350\256\276\347\275\256IIC\345\234\260\345\235\200", nullptr));
        etIICAddress->setPlaceholderText(QCoreApplication::translate("WidgetSteeringIIC", "\350\257\267\350\276\223\345\205\245\345\275\223\345\211\215IIC\345\234\260\345\235\200", nullptr));
        lbStartWiper_2->setText(QCoreApplication::translate("WidgetSteeringIIC", "IIC\345\234\260\345\235\200\357\274\214\347\225\231\347\251\272\344\270\2720x10", nullptr));
        btnSwiperPeriod->setText(QCoreApplication::translate("WidgetSteeringIIC", "\350\256\276\347\275\256\351\233\250\345\210\267\345\221\250\346\234\237", nullptr));
        cbWiperPeriodUnit->setItemText(0, QCoreApplication::translate("WidgetSteeringIIC", "\345\210\206\351\222\237", nullptr));
        cbWiperPeriodUnit->setItemText(1, QCoreApplication::translate("WidgetSteeringIIC", "\345\260\217\346\227\266", nullptr));

        etCycleCnt->setPlaceholderText(QCoreApplication::translate("WidgetSteeringIIC", "1~10\345\206\205\346\255\243\346\225\264\346\225\260", nullptr));
        btnSetRoundtripCnt->setText(QCoreApplication::translate("WidgetSteeringIIC", "\350\256\276\347\275\256\345\276\200\350\277\224\346\254\241\346\225\260", nullptr));
        lbStartWiper->setText(QCoreApplication::translate("WidgetSteeringIIC", "\346\230\276\347\244\272\346\230\257\345\220\246\345\220\257\345\212\250\351\233\250\345\210\267", nullptr));
        btnStartWiper->setText(QCoreApplication::translate("WidgetSteeringIIC", "\347\253\213\345\215\263\345\220\257\345\212\250\351\233\250\345\210\267", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetSteeringIIC: public Ui_WidgetSteeringIIC {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETSTEERINGIIC_H

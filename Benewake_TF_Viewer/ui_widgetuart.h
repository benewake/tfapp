/********************************************************************************
** Form generated from reading UI file 'widgetuart.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETUART_H
#define UI_WIDGETUART_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetUart
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QLabel *label_5;
    QLabel *label_4;
    QLineEdit *etDist;
    QComboBox *cbIOMode;
    QLabel *label_3;
    QLabel *label_2;
    QLineEdit *etZone;
    QLineEdit *etDelay1;
    QLineEdit *etDelay2;
    QPushButton *btnIOSetting;
    QGridLayout *layoutUart;
    QPushButton *btnSaveConfig;
    QComboBox *cbBaudrate;
    QPushButton *btnFrameFreq;
    QLabel *lbFactoryResetResult;
    QComboBox *cbLowConsumption;
    QPushButton *btnOutputFormat;
    QLabel *lbConfigResult;
    QLabel *lbResetResult;
    QComboBox *cbOutSwtich;
    QComboBox *cbOutputFormat;
    QPushButton *btnBaudrate;
    QLabel *lbVersion;
    QComboBox *cbFrameFreq;
    QPushButton *btnFallbackToFactory;
    QPushButton *btnOutSwitch;
    QPushButton *btnSystemReset;
    QPushButton *btnGetVersion;
    QPushButton *btnLowConsumption;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *WidgetUart)
    {
        if (WidgetUart->objectName().isEmpty())
            WidgetUart->setObjectName(QString::fromUtf8("WidgetUart"));
        WidgetUart->resize(486, 542);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        WidgetUart->setFont(font);
        gridLayout = new QGridLayout(WidgetUart);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(0);
        gridLayout->setVerticalSpacing(10);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(WidgetUart);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setFamily(QString::fromUtf8("Arial"));
        font1.setPointSize(9);
        groupBox->setFont(font1);
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(8);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(8, 8, 8, 8);
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setFont(font1);
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        sizePolicy1.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy1);
        label_5->setFont(font1);
        label_5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_5, 0, 2, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy1.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy1);
        label_4->setFont(font1);
        label_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_4, 2, 1, 1, 1);

        etDist = new QLineEdit(groupBox);
        etDist->setObjectName(QString::fromUtf8("etDist"));
        etDist->setMinimumSize(QSize(0, 30));
        etDist->setFont(font1);

        gridLayout_2->addWidget(etDist, 1, 1, 1, 1);

        cbIOMode = new QComboBox(groupBox);
        cbIOMode->addItem(QString());
        cbIOMode->addItem(QString());
        cbIOMode->addItem(QString());
        cbIOMode->setObjectName(QString::fromUtf8("cbIOMode"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(cbIOMode->sizePolicy().hasHeightForWidth());
        cbIOMode->setSizePolicy(sizePolicy2);
        cbIOMode->setMinimumSize(QSize(0, 30));
        cbIOMode->setFont(font1);

        gridLayout_2->addWidget(cbIOMode, 1, 0, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy1.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy1);
        label_3->setFont(font1);
        label_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);
        label_2->setFont(font1);
        label_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_2, 0, 1, 1, 1);

        etZone = new QLineEdit(groupBox);
        etZone->setObjectName(QString::fromUtf8("etZone"));
        etZone->setMinimumSize(QSize(0, 30));
        etZone->setFont(font1);

        gridLayout_2->addWidget(etZone, 1, 2, 1, 1);

        etDelay1 = new QLineEdit(groupBox);
        etDelay1->setObjectName(QString::fromUtf8("etDelay1"));
        sizePolicy2.setHeightForWidth(etDelay1->sizePolicy().hasHeightForWidth());
        etDelay1->setSizePolicy(sizePolicy2);
        etDelay1->setMinimumSize(QSize(0, 30));
        etDelay1->setFont(font1);

        gridLayout_2->addWidget(etDelay1, 3, 0, 1, 1);

        etDelay2 = new QLineEdit(groupBox);
        etDelay2->setObjectName(QString::fromUtf8("etDelay2"));
        etDelay2->setMinimumSize(QSize(0, 30));
        etDelay2->setFont(font1);

        gridLayout_2->addWidget(etDelay2, 3, 1, 1, 1);

        btnIOSetting = new QPushButton(groupBox);
        btnIOSetting->setObjectName(QString::fromUtf8("btnIOSetting"));
        btnIOSetting->setMinimumSize(QSize(0, 30));
        btnIOSetting->setFont(font1);

        gridLayout_2->addWidget(btnIOSetting, 3, 2, 1, 1);


        gridLayout->addWidget(groupBox, 1, 0, 1, 1);

        layoutUart = new QGridLayout();
        layoutUart->setObjectName(QString::fromUtf8("layoutUart"));
        layoutUart->setHorizontalSpacing(10);
        layoutUart->setVerticalSpacing(20);
        layoutUart->setContentsMargins(5, 5, 5, 5);
        btnSaveConfig = new QPushButton(WidgetUart);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnSaveConfig, 2, 2, 1, 1);

        cbBaudrate = new QComboBox(WidgetUart);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbBaudrate, 0, 0, 1, 1);

        btnFrameFreq = new QPushButton(WidgetUart);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnFrameFreq, 1, 1, 1, 1);

        lbFactoryResetResult = new QLabel(WidgetUart);
        lbFactoryResetResult->setObjectName(QString::fromUtf8("lbFactoryResetResult"));
        sizePolicy1.setHeightForWidth(lbFactoryResetResult->sizePolicy().hasHeightForWidth());
        lbFactoryResetResult->setSizePolicy(sizePolicy1);
        lbFactoryResetResult->setMinimumSize(QSize(0, 30));
        lbFactoryResetResult->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbFactoryResetResult, 5, 0, 1, 1);

        cbLowConsumption = new QComboBox(WidgetUart);
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->setObjectName(QString::fromUtf8("cbLowConsumption"));
        cbLowConsumption->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbLowConsumption, 4, 0, 1, 1);

        btnOutputFormat = new QPushButton(WidgetUart);
        btnOutputFormat->setObjectName(QString::fromUtf8("btnOutputFormat"));
        btnOutputFormat->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnOutputFormat, 3, 1, 1, 1);

        lbConfigResult = new QLabel(WidgetUart);
        lbConfigResult->setObjectName(QString::fromUtf8("lbConfigResult"));
        lbConfigResult->setMinimumSize(QSize(0, 30));
        lbConfigResult->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbConfigResult, 3, 2, 1, 1);

        lbResetResult = new QLabel(WidgetUart);
        lbResetResult->setObjectName(QString::fromUtf8("lbResetResult"));
        lbResetResult->setMinimumSize(QSize(0, 30));
        lbResetResult->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbResetResult, 5, 2, 1, 1);

        cbOutSwtich = new QComboBox(WidgetUart);
        cbOutSwtich->addItem(QString());
        cbOutSwtich->addItem(QString());
        cbOutSwtich->setObjectName(QString::fromUtf8("cbOutSwtich"));
        cbOutSwtich->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbOutSwtich, 2, 0, 1, 1);

        cbOutputFormat = new QComboBox(WidgetUart);
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->setObjectName(QString::fromUtf8("cbOutputFormat"));
        cbOutputFormat->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbOutputFormat, 3, 0, 1, 1);

        btnBaudrate = new QPushButton(WidgetUart);
        btnBaudrate->setObjectName(QString::fromUtf8("btnBaudrate"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnBaudrate->sizePolicy().hasHeightForWidth());
        btnBaudrate->setSizePolicy(sizePolicy3);
        btnBaudrate->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnBaudrate, 0, 1, 1, 1);

        lbVersion = new QLabel(WidgetUart);
        lbVersion->setObjectName(QString::fromUtf8("lbVersion"));
        lbVersion->setMinimumSize(QSize(0, 30));
        lbVersion->setAlignment(Qt::AlignCenter);

        layoutUart->addWidget(lbVersion, 1, 2, 1, 1);

        cbFrameFreq = new QComboBox(WidgetUart);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(cbFrameFreq, 1, 0, 1, 1);

        btnFallbackToFactory = new QPushButton(WidgetUart);
        btnFallbackToFactory->setObjectName(QString::fromUtf8("btnFallbackToFactory"));
        btnFallbackToFactory->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnFallbackToFactory, 5, 1, 1, 1);

        btnOutSwitch = new QPushButton(WidgetUart);
        btnOutSwitch->setObjectName(QString::fromUtf8("btnOutSwitch"));
        btnOutSwitch->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnOutSwitch, 2, 1, 1, 1);

        btnSystemReset = new QPushButton(WidgetUart);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnSystemReset, 4, 2, 1, 1);

        btnGetVersion = new QPushButton(WidgetUart);
        btnGetVersion->setObjectName(QString::fromUtf8("btnGetVersion"));
        btnGetVersion->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnGetVersion, 0, 2, 1, 1);

        btnLowConsumption = new QPushButton(WidgetUart);
        btnLowConsumption->setObjectName(QString::fromUtf8("btnLowConsumption"));
        btnLowConsumption->setMinimumSize(QSize(0, 30));

        layoutUart->addWidget(btnLowConsumption, 4, 1, 1, 1);


        gridLayout->addLayout(layoutUart, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 2, 0, 1, 1);


        retranslateUi(WidgetUart);

        QMetaObject::connectSlotsByName(WidgetUart);
    } // setupUi

    void retranslateUi(QWidget *WidgetUart)
    {
        WidgetUart->setWindowTitle(QCoreApplication::translate("WidgetUart", "Form", nullptr));
        groupBox->setTitle(QCoreApplication::translate("WidgetUart", "IO\345\217\202\346\225\260\350\256\276\347\275\256", nullptr));
        label->setText(QCoreApplication::translate("WidgetUart", "IO\346\250\241\345\274\217", nullptr));
        label_5->setText(QCoreApplication::translate("WidgetUart", "\346\273\236\345\233\236\345\214\272(cm)", nullptr));
        label_4->setText(QCoreApplication::translate("WidgetUart", "\345\273\266\350\277\237\346\227\266\351\227\264\344\272\214", nullptr));
        etDist->setPlaceholderText(QCoreApplication::translate("WidgetUart", "\350\257\267\350\276\223\345\205\245\344\270\264\347\225\214\345\200\274cm", nullptr));
        cbIOMode->setItemText(0, QCoreApplication::translate("WidgetUart", "\346\225\260\346\215\256\350\276\223\345\207\272\346\250\241\345\274\217", nullptr));
        cbIOMode->setItemText(1, QCoreApplication::translate("WidgetUart", "\345\274\200\345\205\263\351\207\217(\350\277\221\351\253\230\350\277\234\344\275\216)", nullptr));
        cbIOMode->setItemText(2, QCoreApplication::translate("WidgetUart", "\345\274\200\345\205\263\351\207\217(\350\277\221\344\275\216\350\277\234\351\253\230)", nullptr));

        label_3->setText(QCoreApplication::translate("WidgetUart", "\345\273\266\350\277\237\346\227\266\351\227\264\344\270\200", nullptr));
        label_2->setText(QCoreApplication::translate("WidgetUart", "\344\270\264\347\225\214\345\200\274(cm)", nullptr));
        etZone->setPlaceholderText(QCoreApplication::translate("WidgetUart", "\350\257\267\350\276\223\345\205\245\346\273\236\345\233\236\345\214\272cm", nullptr));
        etDelay1->setPlaceholderText(QCoreApplication::translate("WidgetUart", "\350\257\267\350\276\223\345\205\245\345\273\266\350\277\237\346\227\266\351\227\264/ms", nullptr));
        etDelay2->setPlaceholderText(QCoreApplication::translate("WidgetUart", "\350\257\267\350\276\223\345\205\245\345\273\266\350\277\237\346\227\266\351\227\264\344\272\214/ms", nullptr));
        btnIOSetting->setText(QCoreApplication::translate("WidgetUart", "\351\205\215\347\275\256IO\346\250\241\345\274\217", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("WidgetUart", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("WidgetUart", "9600", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("WidgetUart", "14400", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("WidgetUart", "19200", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("WidgetUart", "38400", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("WidgetUart", "56000", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("WidgetUart", "115200", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("WidgetUart", "128000", nullptr));
        cbBaudrate->setItemText(7, QCoreApplication::translate("WidgetUart", "230400", nullptr));
        cbBaudrate->setItemText(8, QCoreApplication::translate("WidgetUart", "256000", nullptr));
        cbBaudrate->setItemText(9, QCoreApplication::translate("WidgetUart", "460800", nullptr));
        cbBaudrate->setItemText(10, QCoreApplication::translate("WidgetUart", "512000", nullptr));
        cbBaudrate->setItemText(11, QCoreApplication::translate("WidgetUart", "750000", nullptr));
        cbBaudrate->setItemText(12, QCoreApplication::translate("WidgetUart", "921600", nullptr));

        btnFrameFreq->setText(QCoreApplication::translate("WidgetUart", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        lbFactoryResetResult->setText(QCoreApplication::translate("WidgetUart", "\346\230\276\347\244\272\346\230\257\345\220\246\346\201\242\345\244\215", nullptr));
        cbLowConsumption->setItemText(0, QCoreApplication::translate("WidgetUart", "1", nullptr));
        cbLowConsumption->setItemText(1, QCoreApplication::translate("WidgetUart", "2", nullptr));
        cbLowConsumption->setItemText(2, QCoreApplication::translate("WidgetUart", "3", nullptr));
        cbLowConsumption->setItemText(3, QCoreApplication::translate("WidgetUart", "4", nullptr));
        cbLowConsumption->setItemText(4, QCoreApplication::translate("WidgetUart", "5", nullptr));
        cbLowConsumption->setItemText(5, QCoreApplication::translate("WidgetUart", "6", nullptr));
        cbLowConsumption->setItemText(6, QCoreApplication::translate("WidgetUart", "7", nullptr));
        cbLowConsumption->setItemText(7, QCoreApplication::translate("WidgetUart", "8", nullptr));
        cbLowConsumption->setItemText(8, QCoreApplication::translate("WidgetUart", "9", nullptr));
        cbLowConsumption->setItemText(9, QCoreApplication::translate("WidgetUart", "10", nullptr));

        btnOutputFormat->setText(QCoreApplication::translate("WidgetUart", "\350\276\223\345\207\272\346\240\274\345\274\217", nullptr));
        lbConfigResult->setText(QCoreApplication::translate("WidgetUart", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        lbResetResult->setText(QCoreApplication::translate("WidgetUart", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        cbOutSwtich->setItemText(0, QCoreApplication::translate("WidgetUart", "\344\275\277\350\203\275", nullptr));
        cbOutSwtich->setItemText(1, QCoreApplication::translate("WidgetUart", "\345\205\263\351\227\255", nullptr));

        cbOutputFormat->setItemText(0, QCoreApplication::translate("WidgetUart", "9Byte(cm)", nullptr));
        cbOutputFormat->setItemText(1, QCoreApplication::translate("WidgetUart", "9Byte(mm)", nullptr));
        cbOutputFormat->setItemText(2, QCoreApplication::translate("WidgetUart", "\345\255\227\347\254\246\344\270\262\346\240\274\345\274\217(m)", nullptr));

        btnBaudrate->setText(QCoreApplication::translate("WidgetUart", "\350\256\276\347\275\256\346\263\242\347\211\271\347\216\207", nullptr));
        lbVersion->setText(QCoreApplication::translate("WidgetUart", "\347\211\210\346\234\254\345\217\267\357\274\232", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetUart", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetUart", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetUart", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetUart", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetUart", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetUart", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetUart", "1000", nullptr));

        btnFallbackToFactory->setText(QCoreApplication::translate("WidgetUart", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        btnOutSwitch->setText(QCoreApplication::translate("WidgetUart", "\350\276\223\345\207\272\345\274\200\345\205\263", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("WidgetUart", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        btnGetVersion->setText(QCoreApplication::translate("WidgetUart", "\350\216\267\345\217\226\347\211\210\346\234\254\345\217\267", nullptr));
        btnLowConsumption->setText(QCoreApplication::translate("WidgetUart", "\344\275\216\345\212\237\350\200\227\350\256\276\347\275\256/Hz", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetUart: public Ui_WidgetUart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETUART_H

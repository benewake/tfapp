/********************************************************************************
** Form generated from reading UI file 'maindialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINDIALOG_H
#define UI_MAINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainDialog
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widgetTitle;
    QHBoxLayout *horizontalLayout;
    QLabel *labIco;
    QLabel *labTitle;
    QSpacerItem *horizontalSpacer;
    QWidget *widgetMenu;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *btnMenu;
    QPushButton *btnMenu_Min;
    QPushButton *btnMenu_Max;
    QPushButton *btnMenu_Close;
    QWidget *widgetMain;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *layoutContent;

    void setupUi(QDialog *MainDialog)
    {
        if (MainDialog->objectName().isEmpty())
            MainDialog->setObjectName(QString::fromUtf8("MainDialog"));
        MainDialog->resize(1440, 900);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainDialog->sizePolicy().hasHeightForWidth());
        MainDialog->setSizePolicy(sizePolicy);
        MainDialog->setMinimumSize(QSize(0, 40));
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        MainDialog->setFont(font);
        verticalLayout = new QVBoxLayout(MainDialog);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        widgetTitle = new QWidget(MainDialog);
        widgetTitle->setObjectName(QString::fromUtf8("widgetTitle"));
        sizePolicy.setHeightForWidth(widgetTitle->sizePolicy().hasHeightForWidth());
        widgetTitle->setSizePolicy(sizePolicy);
        widgetTitle->setMinimumSize(QSize(0, 40));
        widgetTitle->setStyleSheet(QString::fromUtf8("QWidget#widgetTitle{\n"
"background-color: rgb(255,255,255);\n"
"}\n"
"QLabel{\n"
"color:0xFFFFFF;\n"
"}\n"
"QLabel#labTitle{\n"
"font:16px;\n"
"}\n"
"QLabel#labInfo{\n"
"font:12px;\n"
"}\n"
"QFrame#line{\n"
"color:#ABD6FF;\n"
"}\n"
"QToolButton::menu-indicator{\n"
"image:None;\n"
"}"));
        horizontalLayout = new QHBoxLayout(widgetTitle);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, 0, 0, 0);
        labIco = new QLabel(widgetTitle);
        labIco->setObjectName(QString::fromUtf8("labIco"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(labIco->sizePolicy().hasHeightForWidth());
        labIco->setSizePolicy(sizePolicy1);
        labIco->setMinimumSize(QSize(32, 32));
        labIco->setStyleSheet(QString::fromUtf8("image: url(:benewake-LOGO.ico);"));
        labIco->setAlignment(Qt::AlignCenter);
        labIco->setMargin(4);

        horizontalLayout->addWidget(labIco);

        labTitle = new QLabel(widgetTitle);
        labTitle->setObjectName(QString::fromUtf8("labTitle"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        labTitle->setFont(font1);
        labTitle->setLayoutDirection(Qt::LeftToRight);
        labTitle->setStyleSheet(QString::fromUtf8(""));
        labTitle->setMargin(10);

        horizontalLayout->addWidget(labTitle);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        widgetMenu = new QWidget(widgetTitle);
        widgetMenu->setObjectName(QString::fromUtf8("widgetMenu"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(widgetMenu->sizePolicy().hasHeightForWidth());
        widgetMenu->setSizePolicy(sizePolicy2);
        widgetMenu->setStyleSheet(QString::fromUtf8(""));
        horizontalLayout_2 = new QHBoxLayout(widgetMenu);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        btnMenu = new QToolButton(widgetMenu);
        btnMenu->setObjectName(QString::fromUtf8("btnMenu"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnMenu->sizePolicy().hasHeightForWidth());
        btnMenu->setSizePolicy(sizePolicy3);
        btnMenu->setMinimumSize(QSize(80, 0));
        btnMenu->setMaximumSize(QSize(35, 16777215));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font2.setPointSize(10);
        btnMenu->setFont(font2);
        btnMenu->setStyleSheet(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/imgs/titlemenu.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnMenu->setIcon(icon);
        btnMenu->setPopupMode(QToolButton::InstantPopup);
        btnMenu->setToolButtonStyle(Qt::ToolButtonTextOnly);

        horizontalLayout_2->addWidget(btnMenu);

        btnMenu_Min = new QPushButton(widgetMenu);
        btnMenu_Min->setObjectName(QString::fromUtf8("btnMenu_Min"));
        sizePolicy3.setHeightForWidth(btnMenu_Min->sizePolicy().hasHeightForWidth());
        btnMenu_Min->setSizePolicy(sizePolicy3);
        btnMenu_Min->setMinimumSize(QSize(40, 0));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/minimize.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnMenu_Min->setIcon(icon1);
        btnMenu_Min->setIconSize(QSize(24, 24));
        btnMenu_Min->setFlat(true);

        horizontalLayout_2->addWidget(btnMenu_Min);

        btnMenu_Max = new QPushButton(widgetMenu);
        btnMenu_Max->setObjectName(QString::fromUtf8("btnMenu_Max"));
        sizePolicy3.setHeightForWidth(btnMenu_Max->sizePolicy().hasHeightForWidth());
        btnMenu_Max->setSizePolicy(sizePolicy3);
        btnMenu_Max->setMinimumSize(QSize(40, 0));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/maxmize.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnMenu_Max->setIcon(icon2);
        btnMenu_Max->setIconSize(QSize(24, 24));
        btnMenu_Max->setFlat(true);

        horizontalLayout_2->addWidget(btnMenu_Max);

        btnMenu_Close = new QPushButton(widgetMenu);
        btnMenu_Close->setObjectName(QString::fromUtf8("btnMenu_Close"));
        sizePolicy3.setHeightForWidth(btnMenu_Close->sizePolicy().hasHeightForWidth());
        btnMenu_Close->setSizePolicy(sizePolicy3);
        btnMenu_Close->setMinimumSize(QSize(40, 0));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnMenu_Close->setIcon(icon3);
        btnMenu_Close->setIconSize(QSize(24, 24));
        btnMenu_Close->setFlat(true);

        horizontalLayout_2->addWidget(btnMenu_Close);


        horizontalLayout->addWidget(widgetMenu);


        verticalLayout->addWidget(widgetTitle);

        widgetMain = new QWidget(MainDialog);
        widgetMain->setObjectName(QString::fromUtf8("widgetMain"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(widgetMain->sizePolicy().hasHeightForWidth());
        widgetMain->setSizePolicy(sizePolicy4);
        verticalLayout_2 = new QVBoxLayout(widgetMain);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        layoutContent = new QVBoxLayout();
        layoutContent->setSpacing(0);
        layoutContent->setObjectName(QString::fromUtf8("layoutContent"));
        layoutContent->setContentsMargins(0, 0, 0, 0);

        verticalLayout_2->addLayout(layoutContent);


        verticalLayout->addWidget(widgetMain);


        retranslateUi(MainDialog);

        QMetaObject::connectSlotsByName(MainDialog);
    } // setupUi

    void retranslateUi(QDialog *MainDialog)
    {
        MainDialog->setWindowTitle(QCoreApplication::translate("MainDialog", "BenwakeTFViewer", nullptr));
        labIco->setText(QString());
        labTitle->setText(QCoreApplication::translate("MainDialog", "Benewake_TF_Viewer V0.1.0", nullptr));
        btnMenu->setText(QCoreApplication::translate("MainDialog", "\344\275\277\347\224\250\345\270\256\345\212\251", nullptr));
        btnMenu_Min->setText(QString());
        btnMenu_Max->setText(QString());
        btnMenu_Close->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainDialog: public Ui_MainDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINDIALOG_H

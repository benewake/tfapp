/********************************************************************************
** Form generated from reading UI file 'widgetmodbus.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETMODBUS_H
#define UI_WIDGETMODBUS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetModbus
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QLabel *lbCurDistance_2;
    QPushButton *btnMbContractSave;
    QComboBox *cbBaudrate;
    QPushButton *btnModbusContract;
    QLabel *lbCurDistance;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnStart;
    QPushButton *btnCurSignal;
    QLineEdit *etPeriod;
    QComboBox *cbFrameFreq;
    QPushButton *btnCurrentDistance;
    QPushButton *btnModbusAddress;
    QLabel *lbCurSignal;
    QPushButton *btnStop;
    QLabel *lbSaveConfigResult;
    QPushButton *btnSaveConfig;
    QLineEdit *etModbusAddress;
    QPushButton *btnBaudrate;
    QPushButton *btnFrameFreq;
    QLineEdit *etCurModbusAddress;
    QComboBox *cbModbusContract;
    QLabel *lbConfigResult;

    void setupUi(QWidget *WidgetModbus)
    {
        if (WidgetModbus->objectName().isEmpty())
            WidgetModbus->setObjectName(QString::fromUtf8("WidgetModbus"));
        WidgetModbus->resize(493, 428);
        gridLayout = new QGridLayout(WidgetModbus);
        gridLayout->setSpacing(5);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(5, 5, 5, 5);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setHorizontalSpacing(9);
        gridLayout_2->setContentsMargins(9, -1, 9, -1);
        lbCurDistance_2 = new QLabel(WidgetModbus);
        lbCurDistance_2->setObjectName(QString::fromUtf8("lbCurDistance_2"));

        gridLayout_2->addWidget(lbCurDistance_2, 1, 1, 1, 1);

        btnMbContractSave = new QPushButton(WidgetModbus);
        btnMbContractSave->setObjectName(QString::fromUtf8("btnMbContractSave"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnMbContractSave->sizePolicy().hasHeightForWidth());
        btnMbContractSave->setSizePolicy(sizePolicy);
        btnMbContractSave->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnMbContractSave, 0, 2, 1, 1);

        cbBaudrate = new QComboBox(WidgetModbus);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(cbBaudrate->sizePolicy().hasHeightForWidth());
        cbBaudrate->setSizePolicy(sizePolicy1);
        cbBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbBaudrate, 3, 0, 1, 1);

        btnModbusContract = new QPushButton(WidgetModbus);
        btnModbusContract->setObjectName(QString::fromUtf8("btnModbusContract"));
        sizePolicy.setHeightForWidth(btnModbusContract->sizePolicy().hasHeightForWidth());
        btnModbusContract->setSizePolicy(sizePolicy);
        btnModbusContract->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnModbusContract, 0, 1, 1, 1);

        lbCurDistance = new QLabel(WidgetModbus);
        lbCurDistance->setObjectName(QString::fromUtf8("lbCurDistance"));

        gridLayout_2->addWidget(lbCurDistance, 5, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(20);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnStart = new QPushButton(WidgetModbus);
        btnStart->setObjectName(QString::fromUtf8("btnStart"));
        sizePolicy1.setHeightForWidth(btnStart->sizePolicy().hasHeightForWidth());
        btnStart->setSizePolicy(sizePolicy1);
        btnStart->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(btnStart);


        gridLayout_2->addLayout(horizontalLayout, 9, 1, 1, 1);

        btnCurSignal = new QPushButton(WidgetModbus);
        btnCurSignal->setObjectName(QString::fromUtf8("btnCurSignal"));
        sizePolicy1.setHeightForWidth(btnCurSignal->sizePolicy().hasHeightForWidth());
        btnCurSignal->setSizePolicy(sizePolicy1);
        btnCurSignal->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnCurSignal, 6, 1, 1, 1);

        etPeriod = new QLineEdit(WidgetModbus);
        etPeriod->setObjectName(QString::fromUtf8("etPeriod"));
        sizePolicy1.setHeightForWidth(etPeriod->sizePolicy().hasHeightForWidth());
        etPeriod->setSizePolicy(sizePolicy1);
        etPeriod->setMinimumSize(QSize(0, 30));
        etPeriod->setMaximumSize(QSize(500, 30));

        gridLayout_2->addWidget(etPeriod, 9, 0, 1, 1);

        cbFrameFreq = new QComboBox(WidgetModbus);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        sizePolicy1.setHeightForWidth(cbFrameFreq->sizePolicy().hasHeightForWidth());
        cbFrameFreq->setSizePolicy(sizePolicy1);
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 4, 0, 1, 1);

        btnCurrentDistance = new QPushButton(WidgetModbus);
        btnCurrentDistance->setObjectName(QString::fromUtf8("btnCurrentDistance"));
        sizePolicy1.setHeightForWidth(btnCurrentDistance->sizePolicy().hasHeightForWidth());
        btnCurrentDistance->setSizePolicy(sizePolicy1);
        btnCurrentDistance->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnCurrentDistance, 5, 1, 1, 1);

        btnModbusAddress = new QPushButton(WidgetModbus);
        btnModbusAddress->setObjectName(QString::fromUtf8("btnModbusAddress"));
        sizePolicy.setHeightForWidth(btnModbusAddress->sizePolicy().hasHeightForWidth());
        btnModbusAddress->setSizePolicy(sizePolicy);
        btnModbusAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnModbusAddress, 2, 1, 1, 1);

        lbCurSignal = new QLabel(WidgetModbus);
        lbCurSignal->setObjectName(QString::fromUtf8("lbCurSignal"));

        gridLayout_2->addWidget(lbCurSignal, 6, 0, 1, 1);

        btnStop = new QPushButton(WidgetModbus);
        btnStop->setObjectName(QString::fromUtf8("btnStop"));
        sizePolicy1.setHeightForWidth(btnStop->sizePolicy().hasHeightForWidth());
        btnStop->setSizePolicy(sizePolicy1);
        btnStop->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStop, 9, 2, 1, 1);

        lbSaveConfigResult = new QLabel(WidgetModbus);
        lbSaveConfigResult->setObjectName(QString::fromUtf8("lbSaveConfigResult"));

        gridLayout_2->addWidget(lbSaveConfigResult, 8, 0, 1, 1);

        btnSaveConfig = new QPushButton(WidgetModbus);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        sizePolicy1.setHeightForWidth(btnSaveConfig->sizePolicy().hasHeightForWidth());
        btnSaveConfig->setSizePolicy(sizePolicy1);
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 8, 1, 1, 1);

        etModbusAddress = new QLineEdit(WidgetModbus);
        etModbusAddress->setObjectName(QString::fromUtf8("etModbusAddress"));
        sizePolicy1.setHeightForWidth(etModbusAddress->sizePolicy().hasHeightForWidth());
        etModbusAddress->setSizePolicy(sizePolicy1);
        etModbusAddress->setMinimumSize(QSize(30, 30));

        gridLayout_2->addWidget(etModbusAddress, 2, 0, 1, 1);

        btnBaudrate = new QPushButton(WidgetModbus);
        btnBaudrate->setObjectName(QString::fromUtf8("btnBaudrate"));
        sizePolicy.setHeightForWidth(btnBaudrate->sizePolicy().hasHeightForWidth());
        btnBaudrate->setSizePolicy(sizePolicy);
        btnBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnBaudrate, 3, 1, 1, 1);

        btnFrameFreq = new QPushButton(WidgetModbus);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        sizePolicy1.setHeightForWidth(btnFrameFreq->sizePolicy().hasHeightForWidth());
        btnFrameFreq->setSizePolicy(sizePolicy1);
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFrameFreq, 4, 1, 1, 1);

        etCurModbusAddress = new QLineEdit(WidgetModbus);
        etCurModbusAddress->setObjectName(QString::fromUtf8("etCurModbusAddress"));
        sizePolicy1.setHeightForWidth(etCurModbusAddress->sizePolicy().hasHeightForWidth());
        etCurModbusAddress->setSizePolicy(sizePolicy1);
        etCurModbusAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etCurModbusAddress, 1, 0, 1, 1);

        cbModbusContract = new QComboBox(WidgetModbus);
        cbModbusContract->addItem(QString());
        cbModbusContract->addItem(QString());
        cbModbusContract->setObjectName(QString::fromUtf8("cbModbusContract"));
        sizePolicy1.setHeightForWidth(cbModbusContract->sizePolicy().hasHeightForWidth());
        cbModbusContract->setSizePolicy(sizePolicy1);
        cbModbusContract->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbModbusContract, 0, 0, 1, 1);

        lbConfigResult = new QLabel(WidgetModbus);
        lbConfigResult->setObjectName(QString::fromUtf8("lbConfigResult"));

        gridLayout_2->addWidget(lbConfigResult, 1, 2, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 1, 1, 1);


        retranslateUi(WidgetModbus);

        QMetaObject::connectSlotsByName(WidgetModbus);
    } // setupUi

    void retranslateUi(QWidget *WidgetModbus)
    {
        WidgetModbus->setWindowTitle(QCoreApplication::translate("WidgetModbus", "Form", nullptr));
        lbCurDistance_2->setText(QCoreApplication::translate("WidgetModbus", "  \345\275\223\345\211\215Modbus\347\253\231\345\217\267", nullptr));
        btnMbContractSave->setText(QCoreApplication::translate("WidgetModbus", "\345\220\257\347\224\250/\345\205\263\351\227\255\344\277\235\345\255\230", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("WidgetModbus", "9600", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("WidgetModbus", "14400", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("WidgetModbus", "19200", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("WidgetModbus", "38400", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("WidgetModbus", "56000", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("WidgetModbus", "57600", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("WidgetModbus", "115200", nullptr));
        cbBaudrate->setItemText(7, QCoreApplication::translate("WidgetModbus", "12800", nullptr));
        cbBaudrate->setItemText(8, QCoreApplication::translate("WidgetModbus", "230400", nullptr));
        cbBaudrate->setItemText(9, QCoreApplication::translate("WidgetModbus", "256000", nullptr));
        cbBaudrate->setItemText(10, QCoreApplication::translate("WidgetModbus", "460800", nullptr));
        cbBaudrate->setItemText(11, QCoreApplication::translate("WidgetModbus", "512000", nullptr));
        cbBaudrate->setItemText(12, QCoreApplication::translate("WidgetModbus", "750000", nullptr));
        cbBaudrate->setItemText(13, QCoreApplication::translate("WidgetModbus", "921600", nullptr));

        btnModbusContract->setText(QCoreApplication::translate("WidgetModbus", "Modbus\345\215\217\350\256\256", nullptr));
        lbCurDistance->setText(QCoreApplication::translate("WidgetModbus", "\345\275\223\345\211\215\350\267\235\347\246\273\357\274\232", nullptr));
        btnStart->setText(QCoreApplication::translate("WidgetModbus", "\345\274\200\345\220\257\350\275\256\350\257\242", nullptr));
        btnCurSignal->setText(QCoreApplication::translate("WidgetModbus", "\350\216\267\345\217\226\345\275\223\345\211\215\344\277\241\345\217\267\345\274\272\345\272\246", nullptr));
        etPeriod->setPlaceholderText(QCoreApplication::translate("WidgetModbus", "\350\276\223\345\205\245\345\276\252\347\216\257\345\221\250\346\234\237ms", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetModbus", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetModbus", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetModbus", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetModbus", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetModbus", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetModbus", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetModbus", "1000", nullptr));

        btnCurrentDistance->setText(QCoreApplication::translate("WidgetModbus", "\350\216\267\345\217\226\345\275\223\345\211\215\350\267\235\347\246\273\345\200\274", nullptr));
        btnModbusAddress->setText(QCoreApplication::translate("WidgetModbus", "\350\256\276\347\275\256Modbus\345\234\260\345\235\200", nullptr));
        lbCurSignal->setText(QCoreApplication::translate("WidgetModbus", "\344\277\241\345\217\267\345\274\272\345\272\246\357\274\232", nullptr));
        btnStop->setText(QCoreApplication::translate("WidgetModbus", "\345\201\234\346\255\242\350\275\256\350\257\242", nullptr));
        lbSaveConfigResult->setText(QCoreApplication::translate("WidgetModbus", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230\357\274\201", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("WidgetModbus", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        etModbusAddress->setPlaceholderText(QCoreApplication::translate("WidgetModbus", "1~247\344\271\213\351\227\264\347\232\204\346\225\264\346\225\260", nullptr));
        btnBaudrate->setText(QCoreApplication::translate("WidgetModbus", "\350\256\276\347\275\256\346\263\242\347\211\271\347\216\207", nullptr));
        btnFrameFreq->setText(QCoreApplication::translate("WidgetModbus", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        etCurModbusAddress->setText(QCoreApplication::translate("WidgetModbus", "1", nullptr));
        etCurModbusAddress->setPlaceholderText(QCoreApplication::translate("WidgetModbus", "1~247\344\271\213\351\227\264\347\232\204\346\225\264\346\225\260", nullptr));
        cbModbusContract->setItemText(0, QCoreApplication::translate("WidgetModbus", "\345\220\257\347\224\250", nullptr));
        cbModbusContract->setItemText(1, QCoreApplication::translate("WidgetModbus", "\345\205\263\351\227\255", nullptr));

        lbConfigResult->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WidgetModbus: public Ui_WidgetModbus {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETMODBUS_H

#ifndef WIDGETUART_H
#define WIDGETUART_H

#include <QWidget>

#include "serialportio.h"


namespace Ui {
class WidgetUart;
}

class WidgetUart : public QWidget
{
    Q_OBJECT


protected:
    void changeEvent(QEvent *e);


private:
    void InitDatas();
public:
    explicit WidgetUart(QWidget *parent = nullptr);
    ~WidgetUart();
    serialportio *sensorRecver_;

    QString mProductModal;


//将控件暴露出来，供上层窗体使用
    Ui::WidgetUart *ui;
    bool CheckComState();
    void SetUartRecerver(serialportio* tReceiver,QString productModal);

private slots:
    void on_btnBaudrate_clicked();
    void on_btnGetVersion_clicked();
    void on_btnFrameFreq_clicked();
    void on_btnOutSwitch_clicked();
    void on_btnSaveConfig_clicked();
    void on_btnSystemReset_clicked();
    void on_btnLowConsumption_clicked();
    void on_btnFallbackToFactory_clicked();
    void on_btnOutputFormat_clicked();
    void on_btnIOSetting_clicked();
};

#endif // WIDGETUART_H

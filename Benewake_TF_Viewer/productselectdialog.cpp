#include "productselectdialog.h"
#include "ui_productselectdialog.h"

ProductSelectDialog::ProductSelectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProductSelectDialog)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);

    InitDatas();

}

void ProductSelectDialog:: InitDatas()
{

    QList<QString> qDataList1;
    qDataList1.append(tr("UART"));
    qDataList1.append(tr("IIC"));
    qDataList1.append(tr("IO"));
    mapDatas.insert(tr("TF-Luna"),qDataList1);

    QList<QString> qDataList2;
    qDataList2.append(tr("UART"));
    qDataList2.append(tr("IIC"));
    qDataList2.append(tr("IO"));
    mapDatas.insert(tr("TFmini-S"),qDataList2);


    QList<QString> qDataList3;
    qDataList3.append(tr("RS485（Modbus)"));
    qDataList3.append(tr("CAN"));
    mapDatas.insert(tr("TFmini-i"),qDataList3);

    QList<QString> qDataList4;
    qDataList4.append(tr("UART"));
    qDataList4.append(tr("IIC"));
    qDataList4.append(tr("IO"));
    mapDatas.insert(tr("TFmini Plus"),qDataList4);


    QList<QString> qDataList5;
    qDataList5.append(tr("UART"));
    qDataList5.append(tr("IIC"));
    qDataList5.append(tr("IO"));
    mapDatas.insert(tr("TF02-Pro"),qDataList5);


    QList<QString> qDataList6;
    qDataList6.append(tr("UART"));
    qDataList6.append(tr("IIC"));
    qDataList6.append(tr("RS485（Modbus)"));
    mapDatas.insert(tr("TF02-Pro-W&TF-LM40"),qDataList6);



    QList<QString> qDataList7;
    qDataList7.append(tr("RS485（Modbus)"));
    qDataList7.append(tr("CAN"));
    mapDatas.insert(tr("TF02-i"),qDataList7);


    QList<QString> qDataList8;
    qDataList8.append(tr("UART"));
    qDataList8.append(tr("RS485"));
    qDataList8.append(tr("RS232"));
    mapDatas.insert(tr("TF03"),qDataList8);

    foreach(QString s,mapDatas.keys())
    {
        ui->cbProductModal->addItem(s);
    }


}

ProductSelectDialog::~ProductSelectDialog()
{
    delete ui;
}

void ProductSelectDialog::on_btnClose_clicked()
{
    this->close();
}

void ProductSelectDialog::on_btnOK_clicked()
{
     this->close();
}

void ProductSelectDialog::on_btnCancel_clicked()
{
     this->close();
}



void ProductSelectDialog::on_cbProductModal_currentIndexChanged(const QString &s)
{

    ui->cbCommContract->clear();
    QString key = ui->cbProductModal->currentText();

    QList<QString> contractList = mapDatas[key];

    foreach(QString s,contractList)
    {
        ui->cbCommContract->addItem(s);
    }
}

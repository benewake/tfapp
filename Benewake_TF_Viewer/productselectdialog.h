#ifndef PRODUCTSELECTDIALOG_H
#define PRODUCTSELECTDIALOG_H

#include <QDialog>
#include <QMap>

namespace Ui {
class ProductSelectDialog;
}

class ProductSelectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProductSelectDialog(QWidget *parent = nullptr);
    ~ProductSelectDialog();

private:
    Ui::ProductSelectDialog *ui;
    QMap<QString,QList<QString>> mapDatas;
    void InitDatas();
private slots:

    void on_btnClose_clicked();
    void on_btnOK_clicked();
    void on_btnCancel_clicked();


    void on_cbProductModal_currentIndexChanged(const QString &s);
};

#endif // PRODUCTSELECTDIALOG_H

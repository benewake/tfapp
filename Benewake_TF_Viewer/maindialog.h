#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include <QWidget>
#include <QFont>
#include <QMenu>
#include <QSerialPortInfo>
#include "formcontent.h"
#include "serialportio.h"

#include <QtCharts/QtCharts>
#include <QMessageBox>
#include <QTextStream>
#include <QDateTime>
#include <QPointF>
#include <QList>
#include <QFile>
#include <QDir>

#include "SizeableWidget.h"

QT_CHARTS_USE_NAMESPACE

#define MAXDISPTIME         (10)
#define TIM_FREQ_TIMEOUT    (200)
#define FREQ_VEC_LENGTH     (1000 / TIM_FREQ_TIMEOUT * 2)   // 保存2s以内计算的帧率，肯定能得到非0帧率

namespace Ui {
class MainDialog;
}

class MainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MainDialog(QWidget *parent = nullptr);
    ~MainDialog();



protected:
    void keyPressEvent(QKeyEvent *event);


private:
    Ui::MainDialog *ui;
    bool max;
    QRect location;

	

    QAction* actGetCurrentConfig;
    QAction* actLogFile;
    QAction* actUserGuide;
    QAction* actProductBookDownload;
    QAction* actAbout;
    QAction* actDeviceConn;

    bool CheckDeviceConn();

    FormContent* formContent = nullptr;



    void initForm();

//    QPoint m_zPos;

//    //鼠标移动事件
//    void mouseMoveEvent(QMouseEvent *event);

//    //鼠标释放事件
//    void mouseReleaseEvent(QMouseEvent *event);

//    //鼠标按下事件
//    void mousePressEvent(QMouseEvent *event);


private slots:



    void on_btnMenu_Min_clicked();
    void on_btnMenu_Max_clicked();
    void on_btnMenu_Close_clicked();


    void onLogFile();
    void onUserGuide();
    void onProductBookDownload();
    void onAbout();




};

#endif // MAINDIALOG_H

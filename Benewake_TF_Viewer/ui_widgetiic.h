/********************************************************************************
** Form generated from reading UI file 'widgetiic.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETIIC_H
#define UI_WIDGETIIC_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetIIC
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QPushButton *btnStartQuery;
    QPushButton *btnStopQuery;
    QLabel *lbResetResult;
    QPushButton *btnFactoryReset;
    QLabel *lbConfigSvResult;
    QPushButton *btnSaveConfig;
    QLabel *lbFactoryReset;
    QPushButton *btnSystemReset;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *etPeriod;
    QPushButton *btnIICAddress;
    QLineEdit *etIICAddress;
    QLineEdit *etCurIICAddress;
    QLabel *label_2;
    QPushButton *btnFrameFreq;
    QComboBox *cbFrameFreq;
    QPushButton *btnOutputFormat;
    QComboBox *cbOutputFormat;
    QPushButton *btnLowConsumption;
    QComboBox *cbLowConsumption;
    QPushButton *btnGetMeasureResult;
    QLineEdit *etDistance;

    void setupUi(QWidget *WidgetIIC)
    {
        if (WidgetIIC->objectName().isEmpty())
            WidgetIIC->setObjectName(QString::fromUtf8("WidgetIIC"));
        WidgetIIC->resize(440, 433);
        gridLayout = new QGridLayout(WidgetIIC);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        btnStartQuery = new QPushButton(WidgetIIC);
        btnStartQuery->setObjectName(QString::fromUtf8("btnStartQuery"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnStartQuery->sizePolicy().hasHeightForWidth());
        btnStartQuery->setSizePolicy(sizePolicy);
        btnStartQuery->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(btnStartQuery, 7, 1, 1, 1);

        btnStopQuery = new QPushButton(WidgetIIC);
        btnStopQuery->setObjectName(QString::fromUtf8("btnStopQuery"));
        btnStopQuery->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStopQuery, 7, 2, 1, 1);

        lbResetResult = new QLabel(WidgetIIC);
        lbResetResult->setObjectName(QString::fromUtf8("lbResetResult"));
        lbResetResult->setMinimumSize(QSize(0, 30));
        lbResetResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbResetResult, 3, 2, 1, 1);

        btnFactoryReset = new QPushButton(WidgetIIC);
        btnFactoryReset->setObjectName(QString::fromUtf8("btnFactoryReset"));
        btnFactoryReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFactoryReset, 4, 2, 1, 1);

        lbConfigSvResult = new QLabel(WidgetIIC);
        lbConfigSvResult->setObjectName(QString::fromUtf8("lbConfigSvResult"));
        lbConfigSvResult->setMinimumSize(QSize(0, 30));
        lbConfigSvResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbConfigSvResult, 1, 2, 1, 1);

        btnSaveConfig = new QPushButton(WidgetIIC);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 0, 2, 1, 1);

        lbFactoryReset = new QLabel(WidgetIIC);
        lbFactoryReset->setObjectName(QString::fromUtf8("lbFactoryReset"));
        lbFactoryReset->setMinimumSize(QSize(0, 30));
        lbFactoryReset->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbFactoryReset, 5, 2, 1, 1);

        btnSystemReset = new QPushButton(WidgetIIC);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSystemReset, 2, 2, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(WidgetIIC);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        etPeriod = new QLineEdit(WidgetIIC);
        etPeriod->setObjectName(QString::fromUtf8("etPeriod"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(etPeriod->sizePolicy().hasHeightForWidth());
        etPeriod->setSizePolicy(sizePolicy1);
        etPeriod->setMinimumSize(QSize(30, 30));
        etPeriod->setMaximumSize(QSize(80, 16777215));

        horizontalLayout->addWidget(etPeriod);


        gridLayout_2->addLayout(horizontalLayout, 7, 0, 1, 1);

        btnIICAddress = new QPushButton(WidgetIIC);
        btnIICAddress->setObjectName(QString::fromUtf8("btnIICAddress"));
        sizePolicy.setHeightForWidth(btnIICAddress->sizePolicy().hasHeightForWidth());
        btnIICAddress->setSizePolicy(sizePolicy);
        btnIICAddress->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(btnIICAddress, 1, 1, 1, 1);

        etIICAddress = new QLineEdit(WidgetIIC);
        etIICAddress->setObjectName(QString::fromUtf8("etIICAddress"));
        sizePolicy.setHeightForWidth(etIICAddress->sizePolicy().hasHeightForWidth());
        etIICAddress->setSizePolicy(sizePolicy);
        etIICAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etIICAddress, 1, 0, 1, 1);

        etCurIICAddress = new QLineEdit(WidgetIIC);
        etCurIICAddress->setObjectName(QString::fromUtf8("etCurIICAddress"));
        sizePolicy.setHeightForWidth(etCurIICAddress->sizePolicy().hasHeightForWidth());
        etCurIICAddress->setSizePolicy(sizePolicy);
        etCurIICAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etCurIICAddress, 0, 0, 1, 1);

        label_2 = new QLabel(WidgetIIC);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 0, 1, 1, 1);

        btnFrameFreq = new QPushButton(WidgetIIC);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        sizePolicy.setHeightForWidth(btnFrameFreq->sizePolicy().hasHeightForWidth());
        btnFrameFreq->setSizePolicy(sizePolicy);
        btnFrameFreq->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(btnFrameFreq, 2, 1, 1, 1);

        cbFrameFreq = new QComboBox(WidgetIIC);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 2, 0, 1, 1);

        btnOutputFormat = new QPushButton(WidgetIIC);
        btnOutputFormat->setObjectName(QString::fromUtf8("btnOutputFormat"));
        sizePolicy.setHeightForWidth(btnOutputFormat->sizePolicy().hasHeightForWidth());
        btnOutputFormat->setSizePolicy(sizePolicy);
        btnOutputFormat->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(btnOutputFormat, 3, 1, 1, 1);

        cbOutputFormat = new QComboBox(WidgetIIC);
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->setObjectName(QString::fromUtf8("cbOutputFormat"));
        cbOutputFormat->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbOutputFormat, 3, 0, 1, 1);

        btnLowConsumption = new QPushButton(WidgetIIC);
        btnLowConsumption->setObjectName(QString::fromUtf8("btnLowConsumption"));
        sizePolicy.setHeightForWidth(btnLowConsumption->sizePolicy().hasHeightForWidth());
        btnLowConsumption->setSizePolicy(sizePolicy);
        btnLowConsumption->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(btnLowConsumption, 4, 1, 1, 1);

        cbLowConsumption = new QComboBox(WidgetIIC);
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->setObjectName(QString::fromUtf8("cbLowConsumption"));
        cbLowConsumption->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbLowConsumption, 4, 0, 1, 1);

        btnGetMeasureResult = new QPushButton(WidgetIIC);
        btnGetMeasureResult->setObjectName(QString::fromUtf8("btnGetMeasureResult"));
        sizePolicy.setHeightForWidth(btnGetMeasureResult->sizePolicy().hasHeightForWidth());
        btnGetMeasureResult->setSizePolicy(sizePolicy);
        btnGetMeasureResult->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(btnGetMeasureResult, 5, 1, 1, 1);

        etDistance = new QLineEdit(WidgetIIC);
        etDistance->setObjectName(QString::fromUtf8("etDistance"));
        sizePolicy.setHeightForWidth(etDistance->sizePolicy().hasHeightForWidth());
        etDistance->setSizePolicy(sizePolicy);
        etDistance->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etDistance, 5, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(WidgetIIC);

        QMetaObject::connectSlotsByName(WidgetIIC);
    } // setupUi

    void retranslateUi(QWidget *WidgetIIC)
    {
        WidgetIIC->setWindowTitle(QCoreApplication::translate("WidgetIIC", "Form", nullptr));
        btnStartQuery->setText(QCoreApplication::translate("WidgetIIC", "\345\274\200\345\247\213\350\275\256\350\257\242", nullptr));
        btnStopQuery->setText(QCoreApplication::translate("WidgetIIC", "\345\201\234\346\255\242\350\275\256\350\257\242", nullptr));
        lbResetResult->setText(QCoreApplication::translate("WidgetIIC", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        btnFactoryReset->setText(QCoreApplication::translate("WidgetIIC", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        lbConfigSvResult->setText(QCoreApplication::translate("WidgetIIC", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("WidgetIIC", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        lbFactoryReset->setText(QCoreApplication::translate("WidgetIIC", "\346\230\276\347\244\272\346\230\257\345\220\246\346\201\242\345\244\215", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("WidgetIIC", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        label->setText(QCoreApplication::translate("WidgetIIC", "\350\275\256\350\257\242\345\221\250\346\234\237ms", nullptr));
        etPeriod->setPlaceholderText(QCoreApplication::translate("WidgetIIC", "\345\221\250\346\234\237ms", nullptr));
        btnIICAddress->setText(QCoreApplication::translate("WidgetIIC", "\350\256\276\347\275\256IIC\345\234\260\345\235\200", nullptr));
        etIICAddress->setPlaceholderText(QCoreApplication::translate("WidgetIIC", "\350\256\276\347\275\256\346\226\260IIC\345\234\260\345\235\200 0x08~0x77", nullptr));
        etCurIICAddress->setPlaceholderText(QCoreApplication::translate("WidgetIIC", "IIC\345\234\260\345\235\200 0x08~0x77", nullptr));
        label_2->setText(QCoreApplication::translate("WidgetIIC", "IIC\345\234\260\345\235\200\357\274\214\351\273\230\350\256\2440x10", nullptr));
        btnFrameFreq->setText(QCoreApplication::translate("WidgetIIC", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetIIC", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetIIC", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetIIC", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetIIC", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetIIC", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetIIC", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetIIC", "1000", nullptr));

        btnOutputFormat->setText(QCoreApplication::translate("WidgetIIC", "\350\276\223\345\207\272\346\240\274\345\274\217", nullptr));
        cbOutputFormat->setItemText(0, QCoreApplication::translate("WidgetIIC", "9Byte(cm)", nullptr));
        cbOutputFormat->setItemText(1, QCoreApplication::translate("WidgetIIC", "9Byte(mm)", nullptr));
        cbOutputFormat->setItemText(2, QCoreApplication::translate("WidgetIIC", "\345\255\227\347\254\246\344\270\262\346\240\274\345\274\217", nullptr));

        btnLowConsumption->setText(QCoreApplication::translate("WidgetIIC", "\344\275\216\345\212\237\350\200\227\350\256\276\347\275\256\357\274\210Hz\357\274\211", nullptr));
        cbLowConsumption->setItemText(0, QCoreApplication::translate("WidgetIIC", "1", nullptr));
        cbLowConsumption->setItemText(1, QCoreApplication::translate("WidgetIIC", "2", nullptr));
        cbLowConsumption->setItemText(2, QCoreApplication::translate("WidgetIIC", "3", nullptr));
        cbLowConsumption->setItemText(3, QCoreApplication::translate("WidgetIIC", "4", nullptr));
        cbLowConsumption->setItemText(4, QCoreApplication::translate("WidgetIIC", "5", nullptr));
        cbLowConsumption->setItemText(5, QCoreApplication::translate("WidgetIIC", "6", nullptr));
        cbLowConsumption->setItemText(6, QCoreApplication::translate("WidgetIIC", "7", nullptr));
        cbLowConsumption->setItemText(7, QCoreApplication::translate("WidgetIIC", "8", nullptr));
        cbLowConsumption->setItemText(8, QCoreApplication::translate("WidgetIIC", "9", nullptr));
        cbLowConsumption->setItemText(9, QCoreApplication::translate("WidgetIIC", "10", nullptr));

        btnGetMeasureResult->setText(QCoreApplication::translate("WidgetIIC", "\350\216\267\345\217\226\346\265\213\350\267\235\347\273\223\346\236\234", nullptr));
        etDistance->setPlaceholderText(QCoreApplication::translate("WidgetIIC", "\346\230\276\347\244\272\346\265\213\350\267\235\347\273\223\346\236\234", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetIIC: public Ui_WidgetIIC {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETIIC_H

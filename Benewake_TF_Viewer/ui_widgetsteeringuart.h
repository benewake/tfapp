/********************************************************************************
** Form generated from reading UI file 'widgetsteeringuart.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETSTEERINGUART_H
#define UI_WIDGETSTEERINGUART_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetSteeringUart
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QLabel *lbSystemReset;
    QHBoxLayout *horizontalLayout;
    QLineEdit *etWiperPeriod;
    QComboBox *cbWiperPeriodUnit;
    QPushButton *btnSaveConfig;
    QLabel *lbVersion;
    QComboBox *cbBaudrate;
    QPushButton *btnStartWiper;
    QLabel *lbFactoryResult;
    QPushButton *btnFrameFreq;
    QPushButton *btnWiperPeriod;
    QComboBox *cbOutputFormat;
    QComboBox *cbOutSwitch;
    QComboBox *cbFrameFreq;
    QPushButton *btnOutSwitch;
    QPushButton *btnVersion;
    QPushButton *btnOutputFormat;
    QLabel *lbSaveConfig;
    QLabel *lbStartResult;
    QPushButton *btnBaudrate;
    QPushButton *btnSystemReset;
    QLineEdit *etCycleCnt;
    QPushButton *btnFactoryReset;
    QPushButton *btnCycleCnt;

    void setupUi(QWidget *WidgetSteeringUart)
    {
        if (WidgetSteeringUart->objectName().isEmpty())
            WidgetSteeringUart->setObjectName(QString::fromUtf8("WidgetSteeringUart"));
        WidgetSteeringUart->resize(440, 433);
        gridLayout = new QGridLayout(WidgetSteeringUart);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lbSystemReset = new QLabel(WidgetSteeringUart);
        lbSystemReset->setObjectName(QString::fromUtf8("lbSystemReset"));
        lbSystemReset->setMinimumSize(QSize(0, 30));
        lbSystemReset->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbSystemReset, 5, 2, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(5, 5, 5, 5);
        etWiperPeriod = new QLineEdit(WidgetSteeringUart);
        etWiperPeriod->setObjectName(QString::fromUtf8("etWiperPeriod"));
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(etWiperPeriod->sizePolicy().hasHeightForWidth());
        etWiperPeriod->setSizePolicy(sizePolicy);
        etWiperPeriod->setMinimumSize(QSize(50, 30));

        horizontalLayout->addWidget(etWiperPeriod);

        cbWiperPeriodUnit = new QComboBox(WidgetSteeringUart);
        cbWiperPeriodUnit->addItem(QString());
        cbWiperPeriodUnit->addItem(QString());
        cbWiperPeriodUnit->setObjectName(QString::fromUtf8("cbWiperPeriodUnit"));
        cbWiperPeriodUnit->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(cbWiperPeriodUnit);


        gridLayout_2->addLayout(horizontalLayout, 4, 0, 1, 1);

        btnSaveConfig = new QPushButton(WidgetSteeringUart);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 2, 2, 1, 1);

        lbVersion = new QLabel(WidgetSteeringUart);
        lbVersion->setObjectName(QString::fromUtf8("lbVersion"));
        lbVersion->setMinimumSize(QSize(0, 30));
        lbVersion->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbVersion, 1, 2, 1, 1);

        cbBaudrate = new QComboBox(WidgetSteeringUart);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbBaudrate, 0, 0, 1, 1);

        btnStartWiper = new QPushButton(WidgetSteeringUart);
        btnStartWiper->setObjectName(QString::fromUtf8("btnStartWiper"));
        btnStartWiper->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStartWiper, 6, 1, 1, 1);

        lbFactoryResult = new QLabel(WidgetSteeringUart);
        lbFactoryResult->setObjectName(QString::fromUtf8("lbFactoryResult"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lbFactoryResult->sizePolicy().hasHeightForWidth());
        lbFactoryResult->setSizePolicy(sizePolicy1);
        lbFactoryResult->setMinimumSize(QSize(0, 30));
        lbFactoryResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbFactoryResult, 7, 2, 1, 1);

        btnFrameFreq = new QPushButton(WidgetSteeringUart);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFrameFreq, 1, 1, 1, 1);

        btnWiperPeriod = new QPushButton(WidgetSteeringUart);
        btnWiperPeriod->setObjectName(QString::fromUtf8("btnWiperPeriod"));
        btnWiperPeriod->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnWiperPeriod, 4, 1, 1, 1);

        cbOutputFormat = new QComboBox(WidgetSteeringUart);
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->setObjectName(QString::fromUtf8("cbOutputFormat"));
        cbOutputFormat->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbOutputFormat, 3, 0, 1, 1);

        cbOutSwitch = new QComboBox(WidgetSteeringUart);
        cbOutSwitch->addItem(QString());
        cbOutSwitch->addItem(QString());
        cbOutSwitch->setObjectName(QString::fromUtf8("cbOutSwitch"));
        cbOutSwitch->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbOutSwitch, 2, 0, 1, 1);

        cbFrameFreq = new QComboBox(WidgetSteeringUart);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 1, 0, 1, 1);

        btnOutSwitch = new QPushButton(WidgetSteeringUart);
        btnOutSwitch->setObjectName(QString::fromUtf8("btnOutSwitch"));
        btnOutSwitch->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnOutSwitch, 2, 1, 1, 1);

        btnVersion = new QPushButton(WidgetSteeringUart);
        btnVersion->setObjectName(QString::fromUtf8("btnVersion"));
        btnVersion->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnVersion, 0, 2, 1, 1);

        btnOutputFormat = new QPushButton(WidgetSteeringUart);
        btnOutputFormat->setObjectName(QString::fromUtf8("btnOutputFormat"));
        btnOutputFormat->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnOutputFormat, 3, 1, 1, 1);

        lbSaveConfig = new QLabel(WidgetSteeringUart);
        lbSaveConfig->setObjectName(QString::fromUtf8("lbSaveConfig"));
        lbSaveConfig->setMinimumSize(QSize(0, 30));
        lbSaveConfig->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbSaveConfig, 3, 2, 1, 1);

        lbStartResult = new QLabel(WidgetSteeringUart);
        lbStartResult->setObjectName(QString::fromUtf8("lbStartResult"));
        lbStartResult->setMinimumSize(QSize(0, 30));
        lbStartResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbStartResult, 6, 0, 1, 1);

        btnBaudrate = new QPushButton(WidgetSteeringUart);
        btnBaudrate->setObjectName(QString::fromUtf8("btnBaudrate"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btnBaudrate->sizePolicy().hasHeightForWidth());
        btnBaudrate->setSizePolicy(sizePolicy2);
        btnBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnBaudrate, 0, 1, 1, 1);

        btnSystemReset = new QPushButton(WidgetSteeringUart);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSystemReset, 4, 2, 1, 1);

        etCycleCnt = new QLineEdit(WidgetSteeringUart);
        etCycleCnt->setObjectName(QString::fromUtf8("etCycleCnt"));
        sizePolicy.setHeightForWidth(etCycleCnt->sizePolicy().hasHeightForWidth());
        etCycleCnt->setSizePolicy(sizePolicy);
        etCycleCnt->setMinimumSize(QSize(50, 30));

        gridLayout_2->addWidget(etCycleCnt, 5, 0, 1, 1);

        btnFactoryReset = new QPushButton(WidgetSteeringUart);
        btnFactoryReset->setObjectName(QString::fromUtf8("btnFactoryReset"));
        btnFactoryReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFactoryReset, 6, 2, 1, 1);

        btnCycleCnt = new QPushButton(WidgetSteeringUart);
        btnCycleCnt->setObjectName(QString::fromUtf8("btnCycleCnt"));
        btnCycleCnt->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnCycleCnt, 5, 1, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(WidgetSteeringUart);

        QMetaObject::connectSlotsByName(WidgetSteeringUart);
    } // setupUi

    void retranslateUi(QWidget *WidgetSteeringUart)
    {
        WidgetSteeringUart->setWindowTitle(QCoreApplication::translate("WidgetSteeringUart", "Form", nullptr));
        lbSystemReset->setText(QCoreApplication::translate("WidgetSteeringUart", "\345\267\262\345\244\215\344\275\215", nullptr));
        cbWiperPeriodUnit->setItemText(0, QCoreApplication::translate("WidgetSteeringUart", "\345\210\206\351\222\237", nullptr));
        cbWiperPeriodUnit->setItemText(1, QCoreApplication::translate("WidgetSteeringUart", "\345\260\217\346\227\266", nullptr));

        btnSaveConfig->setText(QCoreApplication::translate("WidgetSteeringUart", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        lbVersion->setText(QCoreApplication::translate("WidgetSteeringUart", "\347\211\210\346\234\254\345\217\267\357\274\232", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("WidgetSteeringUart", "9600", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("WidgetSteeringUart", "14400", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("WidgetSteeringUart", "19200", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("WidgetSteeringUart", "38400", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("WidgetSteeringUart", "56000", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("WidgetSteeringUart", "57600", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("WidgetSteeringUart", "115200", nullptr));
        cbBaudrate->setItemText(7, QCoreApplication::translate("WidgetSteeringUart", "128000", nullptr));
        cbBaudrate->setItemText(8, QCoreApplication::translate("WidgetSteeringUart", "230400", nullptr));
        cbBaudrate->setItemText(9, QCoreApplication::translate("WidgetSteeringUart", "256000", nullptr));
        cbBaudrate->setItemText(10, QCoreApplication::translate("WidgetSteeringUart", "460800", nullptr));
        cbBaudrate->setItemText(11, QCoreApplication::translate("WidgetSteeringUart", "512000", nullptr));
        cbBaudrate->setItemText(12, QCoreApplication::translate("WidgetSteeringUart", "750000", nullptr));
        cbBaudrate->setItemText(13, QCoreApplication::translate("WidgetSteeringUart", "921600", nullptr));

        btnStartWiper->setText(QCoreApplication::translate("WidgetSteeringUart", "\347\253\213\345\215\263\345\220\257\345\212\250\351\233\250\345\210\267", nullptr));
        lbFactoryResult->setText(QCoreApplication::translate("WidgetSteeringUart", "\346\230\276\347\244\272\346\230\257\345\220\246\346\201\242\345\244\215", nullptr));
        btnFrameFreq->setText(QCoreApplication::translate("WidgetSteeringUart", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        btnWiperPeriod->setText(QCoreApplication::translate("WidgetSteeringUart", "\350\256\276\347\275\256\351\233\250\345\210\267\345\221\250\346\234\237", nullptr));
        cbOutputFormat->setItemText(0, QCoreApplication::translate("WidgetSteeringUart", "9Byte(mm)", nullptr));
        cbOutputFormat->setItemText(1, QCoreApplication::translate("WidgetSteeringUart", "9Byte(cm)", nullptr));
        cbOutputFormat->setItemText(2, QCoreApplication::translate("WidgetSteeringUart", "\345\255\227\347\254\246\344\270\262\346\240\274\345\274\217(m)", nullptr));

        cbOutSwitch->setItemText(0, QCoreApplication::translate("WidgetSteeringUart", "\344\275\277\350\203\275", nullptr));
        cbOutSwitch->setItemText(1, QCoreApplication::translate("WidgetSteeringUart", "\345\205\263\351\227\255", nullptr));

        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetSteeringUart", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetSteeringUart", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetSteeringUart", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetSteeringUart", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetSteeringUart", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetSteeringUart", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetSteeringUart", "1000", nullptr));

        btnOutSwitch->setText(QCoreApplication::translate("WidgetSteeringUart", "\350\276\223\345\207\272\345\274\200\345\205\263", nullptr));
        btnVersion->setText(QCoreApplication::translate("WidgetSteeringUart", "\350\216\267\345\217\226\347\211\210\346\234\254\345\217\267", nullptr));
        btnOutputFormat->setText(QCoreApplication::translate("WidgetSteeringUart", "\350\276\223\345\207\272\346\250\241\345\274\217", nullptr));
        lbSaveConfig->setText(QCoreApplication::translate("WidgetSteeringUart", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        lbStartResult->setText(QCoreApplication::translate("WidgetSteeringUart", "\346\230\276\347\244\272\346\230\257\345\220\246\345\220\257\345\212\250", nullptr));
        btnBaudrate->setText(QCoreApplication::translate("WidgetSteeringUart", "\350\256\276\347\275\256\346\263\242\347\211\271\347\216\207", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("WidgetSteeringUart", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        etCycleCnt->setPlaceholderText(QCoreApplication::translate("WidgetSteeringUart", "1~10\345\206\205\347\232\204\346\255\243\346\225\264\346\225\260", nullptr));
        btnFactoryReset->setText(QCoreApplication::translate("WidgetSteeringUart", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        btnCycleCnt->setText(QCoreApplication::translate("WidgetSteeringUart", "\351\233\250\345\210\267\345\276\200\350\277\224\346\254\241\346\225\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetSteeringUart: public Ui_WidgetSteeringUart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETSTEERINGUART_H

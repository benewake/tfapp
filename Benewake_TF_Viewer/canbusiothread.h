#ifndef CANBUSIOTHREAD_H
#define CANBUSIOTHREAD_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QVector>
#include "ControlCAN.h"

typedef QVector<QString> StringVector;

class CanBusIoThread :public QThread
{
    Q_OBJECT
public:
    CanBusIoThread();
    uint mDeviceIndex;
    uint mDeviceType;

    uint mBaudRate;
    uint mCanChanIndex;

    QTimer *timer_;

    bool canOpen;


    bool isOpen();

    //打开设备
    bool OpenCAN(uint deviceType, uint deviceIndex, uint deviceCom,uint baudrate);

    //关闭设置
    bool CloseCAN();

    //初始化CAN
    bool InitCAN();

    bool sendCanData(int ID , QByteArray cmdBytes);

    bool canSetFrameFreq(uint frameFreq);

    bool canOutputSwitch(bool isSwitchOn);

    bool canOuputFrameFormat(uint format);


    bool canSaveConfig();

    bool canSystemReset();

    bool canFactoryReset();

    bool canBaudrate(uint baudrate);

    bool canSetTxID(uint txID);

    bool canSetRxID(uint txID);

    //0x00 标准 0x01扩展
    bool canSetFrameType(uint frameType);

    bool canSetParam(uint frameType,uint baudrate,ulong rxID,ulong txID );

    QString canGetVersion(bool &result);

    bool stopped;

    bool paused;

    QVector<uint16_t> passDist_;
    QVector<uint16_t> passAmp_;
    QVector<QString> passRxFrame_;

    int numFrame_;
    int freq_;


    unsigned char calc_checksum(QByteArray &cmd);
   // bool read_ack(QByteArray ack);
    bool read_ack(QByteArray ack, QByteArray &targetFrame);

signals:
   void signalDistance(QVector<uint16_t> _dist, QVector<uint16_t> _amp, const StringVector& _hexStrArr,int _freq);
   void signalSerialCmdSend(QByteArray cmd);
   void signalDataRcv(QByteArray cmdRx);

private:

    void run() ;
    void sleep(unsigned int msec);

private slots:
       void slotCalcFreq();


};

#endif // CANBUSIOTHREAD_H

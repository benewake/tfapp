#ifndef WIDGETIIC_H
#define WIDGETIIC_H

#include "serialportio.h"

#include <QWidget>



namespace Ui {
class WidgetIIC;
}

class WidgetIIC : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetIIC(QWidget *parent = nullptr);
    ~WidgetIIC();

     void SetUartRecerver(serialportio *tReceiver, QString productModal);

     QTimer* timerIICQuery;

     bool CheckComStateNoMessage();
private slots:

    void on_btnIICAddress_clicked();



    void on_btnSaveConfig_clicked();

    void on_btnFrameFreq_clicked();



    void on_btnSystemReset_clicked();

    void on_btnFactoryReset_clicked();

    void on_btnLowConsumption_clicked();


    void on_btnGetMeasureResult_clicked();

    void on_btnOutputFormat_clicked();

    void on_btnStartQuery_clicked();

    void on_btnStopQuery_clicked();

    void slotiicQuery();

private:
    Ui::WidgetIIC *ui;

    QString mProductModal;

    serialportio *sensorRecver_;

    bool CheckComState();


};

#endif // WIDGETUART_H

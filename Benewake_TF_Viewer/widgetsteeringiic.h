#ifndef WIDGETSTEERINGIIC_H
#define WIDGETSTEERINGIIC_H

#include "serialportio.h"

#include <QWidget>

namespace Ui {
class WidgetSteeringIIC;
}

class WidgetSteeringIIC : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetSteeringIIC(QWidget *parent = nullptr);
    ~WidgetSteeringIIC();

     void SetUartRecerver(serialportio *tReceiver, QString productModal);

     bool CheckComState();
private slots:
     void on_btnAddress_clicked();

     void on_btnFrameFreq_clicked();

     void on_btnSwiperPeriod_clicked();

     void on_btnSetRoundtripCnt_clicked();

     void on_btnStartWiper_clicked();

     void on_btnSaveConfig_clicked();

     void on_btnSystemReset_clicked();

     void on_btnFactoryReset_clicked();

     void on_btnStartTimer_clicked();

private:
    Ui::WidgetSteeringIIC *ui;
    serialportio *sensorRecver_;
    QString mProductModal;
};

#endif // WIDGETUART_H

#include "widgetsteeringiic.h"
#include "ui_widgetsteeringiic.h"

#include <QMessageBox>

WidgetSteeringIIC::WidgetSteeringIIC(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetSteeringIIC)
{
    ui->setupUi(this);
}

WidgetSteeringIIC::~WidgetSteeringIIC()
{
    delete ui;
}

void WidgetSteeringIIC::SetUartRecerver(serialportio *tReceiver,QString productModal)
{
    this->sensorRecver_ = tReceiver;
    this->mProductModal = productModal;
}


bool WidgetSteeringIIC::CheckComState()
{
    if(sensorRecver_==nullptr)
        return false;

    if(!sensorRecver_->isOpen())
    {
        QMessageBox::warning(NULL, "Error", tr(u8"未连接Com口"));
        return false;
    }

    return true;
}


void WidgetSteeringIIC::on_btnAddress_clicked()
{
        if(!CheckComState()) return;

         unsigned char iicAddr = ui->etIICAddress->text().toUInt();
         uint iicNewAddr = ui->etIICNewAddress->text().toUInt();

         if(sensorRecver_->send_iic_setaddr_pro(iicAddr,iicNewAddr))
         {
             qDebug("set success");
             QMessageBox::information(NULL, tr(u8"提示"), tr(u8"IIC地址设定成功"));
         }
         else
         {
             qDebug("set fail");
             QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"IIC地址设定失败，请重试"));
         }


}



void WidgetSteeringIIC::on_btnFrameFreq_clicked()
{
    if(!CheckComState()) return;

    if(sensorRecver_->send_cmd_id_frame_rate_pro(ui->cbFrameFreq->currentText().toUShort()))
    {
       qDebug("帧率设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"帧率设定成功"));

    }else{
        qDebug("帧率设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"帧率设定失败"));
    }


}



void WidgetSteeringIIC::on_btnSwiperPeriod_clicked()
{
    if(!CheckComState()) return;

    uint wiperPeriod = ui->etWiperPeriod->text().toUInt();
    if(ui->cbWiperPeriodUnit->currentIndex()==1)
    {
          wiperPeriod = 60 * wiperPeriod;
    }


    if(sensorRecver_->send_cmd_id_set_wiper_peroid(wiperPeriod))
    {
       qDebug("雨刷周期设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷周期设定成功"));

    }else{
        qDebug("雨刷周期设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷周期设定失败"));
    }
}

void WidgetSteeringIIC::on_btnSetRoundtripCnt_clicked()
{
    if(!CheckComState()) return;

    uint cycleCnt = ui->etCycleCnt->text().toUInt();

    if(cycleCnt>10)
    {
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷往返次数不能超过10次！"));
       return;
    }

    if(sensorRecver_->send_cmd_set_wipercycle(cycleCnt))
    {
       qDebug("雨刷往返次数设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷往返次数设定成功！"));

    }else{
        qDebug("雨刷往返次数设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷往返次数设定失败！"));
    }
}

void WidgetSteeringIIC::on_btnStartWiper_clicked()
{
    if(!CheckComState()) return;

    if(sensorRecver_->send_cmd_startwiper())
    {
       qDebug("雨刷启动成功！");
       ui->lbStartWiper->setText(tr(u8"雨刷启动成功！"));
       //QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷启动成功！"));

    }else{
        qDebug("雨刷启动失败！");
         ui->lbStartWiper->setText(tr(u8"雨刷启动失败！"));
       // QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷启动失败！"));
    }

}

void WidgetSteeringIIC::on_btnSaveConfig_clicked()
{
    if(!CheckComState()) return;

    QString cont;
    cont.append(tr(u8"保存配置"));
    ui->lbConfigSvResult->setText("");

   if(sensorRecver_->send_cmd_id_save_settings_pro())
   {
       ui->lbConfigSvResult->setText(tr(u8"配置保存成功！"));
   }else{
      ui->lbConfigSvResult->setText(tr(u8"配置保存失败！"));
   }

}

void WidgetSteeringIIC::on_btnSystemReset_clicked()
{
    if(!CheckComState()) return;
    QString cont;
    cont.append(tr(u8"复位"));
    QMessageBox  msgBox;
   if(sensorRecver_->send_cmd_id_soft_reset())
   {
      qDebug("复位成功！");
      //QMessageBox::information(NULL, tr(u8"提示"), tr(u8"复位成功！"));
      ui->lbResetResult->setText(tr(u8"复位成功！"));


   }else{
      qDebug("复位失败");
      //QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"复位失败"));
       ui->lbResetResult->setText(tr(u8"复位失败！"));
   }
}

void WidgetSteeringIIC::on_btnFactoryReset_clicked()
{
    if(!CheckComState()) return;
    if(sensorRecver_->send_cmd_id_restore_default())
    {
        ui->lbFactoryReset->setText(tr(u8"恢复出厂成功"));
    }else{
        ui->lbFactoryReset->setText(tr(u8"恢复出厂失败"));
    }
}

void WidgetSteeringIIC::on_btnStartTimer_clicked()
{

}

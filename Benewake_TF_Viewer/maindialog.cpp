#include "maindialog.h"
#include "ui_maindialog.h"
#include "fontawesomeicons.h"
#include "quiwidget.h"
#include "failconndialog.h"
#include "productselectdialog.h"
#include "aboutdialog.h"
#include "formcontent.h"

#include<QScreen>
#include<QRect>


#include <QKeyEvent>
#include <QEvent>
#include <QToolButton>
#include <QStyle>

#include <QMouseEvent>




MainDialog::MainDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainDialog)
{
    ui->setupUi(this);

    QUIWidget::setFormInCenter(this);
    this->initForm();

    //CheckDeviceConn();
}


MainDialog::~MainDialog()
{
    delete ui;
}



void MainDialog::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Escape:
        break;
    default:
        QDialog::keyPressEvent(event);
    }
}



/**
 * @brief 检查是否是否连接
 * @return
 */
bool MainDialog::CheckDeviceConn()
{
    FailConnDialog* failConnDlg = new FailConnDialog;
    failConnDlg->setModal(true);
    failConnDlg->showNormal();

    return false;

}

void MainDialog::initForm()
{
    this->max = false;
    this->location = this->geometry();
    this->setProperty("form", true);
    this->setProperty("canMove", true);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);


    QStringList qss;
    //qss.append("QLabel#label{color:#F0F0F0;font:50pt;}");
    qss.append("QWidget#widgetMenu>QAbstractButton{border:0px solid #FF0000;border-radius:0px;padding:0px;margin:0px;}");
    qss.append("QWidget#widgetMenu>QAbstractButton:hover{background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #EEEEEE,stop:1 #E5E5E5);}");
    qss.append("QWidget#widgetMenu>QAbstractButton:pressed{background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #EEEEEE,stop:1 #E5E5E5);}");
    //qss.append("QToolButton,QPushButton{border:0px solid #FF0000;}QToolButton:hover,QPushButton:hover,QToolButton:pressed,QPushButton:pressed{background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #EEEEEE,stop:1 #E5E5E5);}");

    this->setStyleSheet(qss.join(""));


    actLogFile = new QAction(tr("记录Log文件"), this);
    actLogFile->setIcon(QIcon(":/imgs/folder-view.png"));
    //actLogFile->setShortcut(QKeySequence::Print);

    actUserGuide = new QAction(tr("用户操作手册"), this);
    actUserGuide->setIcon(QIcon(":/imgs/file-word-fill.png"));

    actProductBookDownload = new QAction(tr("产品用户手册下载"), this);
    actProductBookDownload->setIcon(QIcon(":/imgs/download.png"));

    actAbout = new QAction(tr("关于"), this);
    actAbout->setIcon(QIcon(":/imgs/question.png"));



    ui->btnMenu->addAction(actLogFile);
    ui->btnMenu->addAction(actUserGuide);
    ui->btnMenu->addAction(actProductBookDownload);
    ui->btnMenu->addAction(actAbout);

    //connect(actDeviceConn, &QAction::triggered, this, &MainDialog::onDeviceConn);

    connect(actLogFile, &QAction::triggered, this, &MainDialog::onLogFile);
    connect(actUserGuide, &QAction::triggered, this, &MainDialog::onUserGuide);
    connect(actProductBookDownload, &QAction::triggered, this, &MainDialog::onProductBookDownload);
    connect(actAbout, &QAction::triggered, this, &MainDialog::onAbout);

    if(formContent!=nullptr)
    {
        //ui->layoutContent->removeWidget(formContent);
        formContent->close();
        formContent = nullptr;

    }

    formContent = new  FormContent;
    ui->layoutContent->addWidget(formContent);

	ui->btnMenu_Max->setFocus();


}



void MainDialog::on_btnMenu_Min_clicked()
{
    showMinimized();
}

void MainDialog::on_btnMenu_Max_clicked()
{
    if (max) {
        this->setGeometry(location);
        this->setProperty("canMove", true);
    } else {
        location = this->geometry();

        QList<QScreen *> list_screen =  QGuiApplication::screens();  //多显示器
        QRect rect = list_screen.at(0)->geometry();

        rect.setY(-1);
        rect.setHeight(rect.height());

        this->setGeometry(rect);
        this->setProperty("canMove", false);
    }

    max = !max;
}

void MainDialog::on_btnMenu_Close_clicked()
{
    close();
}


void MainDialog::onLogFile()
{



}


void MainDialog::onUserGuide()
{



}


void MainDialog::onProductBookDownload()
{



}

void MainDialog::onAbout()
{
    AboutDialog* aboutDlg = new AboutDialog;
    aboutDlg->setModal(true);
    aboutDlg->showNormal();

}



#ifndef WIDGETSTEERINGMODBUS_H
#define WIDGETSTEERINGMODBUS_H

#include "serialportio.h"

#include <QWidget>



namespace Ui {
class WidgetSteeringModbus;
}

class WidgetSteeringModbus : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetSteeringModbus(QWidget *parent = nullptr);
    ~WidgetSteeringModbus();

     void SetUartRecerver(serialportio *tReceiver, QString productModal);

      bool CheckComState();

      serialportio *sensorRecver_;
      QString mProductModal;

private slots:
      void on_btnModbusEnable_clicked();

      void on_btnModbusAddress_clicked();

      void on_btnBaudrate_clicked();

      void on_btnOutFreq_clicked();

      void on_btnGetDistance_clicked();

      void on_btnSignalQuality_clicked();

      void on_btnSaveConfig_clicked();

      void on_btnWiperPeriod_clicked();

      void on_btnRoundtripCnt_clicked();

      void on_btnStartWiper_clicked();

private:
    Ui::WidgetSteeringModbus *ui;
};

#endif // WIDGETUART_H

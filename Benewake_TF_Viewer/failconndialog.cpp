#include "failconndialog.h"
#include "ui_failconndialog.h"
#include "quiwidget.h"

FailConnDialog::FailConnDialog(QWidget *parent):QDialog(parent),ui(new Ui::FailConnDialog)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint);
    QUIWidget::setFormInCenter(this);


    this->setProperty("form", true);
    this->setProperty("canMove", true);

}

FailConnDialog::~FailConnDialog()
{
    delete ui;
}

void FailConnDialog::on_btnClose_clicked()
{
    this->close();
}

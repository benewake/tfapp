#ifndef WIDGETCAN_H
#define WIDGETCAN_H

#include "canbusiothread.h"
#include "serialportio.h"

#include <QWidget>




namespace Ui {
class WidgetCAN;
}

class WidgetCAN : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetCAN(QWidget *parent = nullptr);
    ~WidgetCAN();

    void SetCanbusIo(CanBusIoThread *canbusioThread, QString productModal);

    QString mProductModal;

    CanBusIoThread* mCanbusThread;

	uint GetCanTxId();

private slots:



    void on_btnFrameFreq_clicked();

    void on_btnOutSwitch_clicked();

    void on_btnVersion_clicked();

    void on_btnSaveConfig_clicked();

    void on_btnSystemReset_clicked();

    void on_btnFactoryReset_clicked();



    void on_btnCANParamSet_clicked();

public:
    Ui::WidgetCAN *ui;

    bool CheckComState();
};

#endif // WIDGETUART_H

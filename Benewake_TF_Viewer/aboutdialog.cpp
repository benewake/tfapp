#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
     ui->setupUi(this);
     this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);

//     QStringList qss;
//     //qss.append("QLabel#label{color:#F0F0F0;font:50pt;}");
//     qss.append("QWidget#widgetTitle>QAbstractButton{border:0px solid #FF0000;border-radius:0px;padding:0px;margin:0px;}");
//     qss.append("QWidget#widgetTitle>QAbstractButton:hover{background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #EEEEEE,stop:1 #E5E5E5);}");
//     qss.append("QWidget#widgetTitle>QAbstractButton:pressed{background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #EEEEEE,stop:1 #E5E5E5);}");
//     //qss.append("QToolButton,QPushButton{border:0px solid #FF0000;}QToolButton:hover,QPushButton:hover,QToolButton:pressed,QPushButton:pressed{background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #EEEEEE,stop:1 #E5E5E5);}");

//     this->setStyleSheet(qss.join(""));

     this->setProperty("form", true);
     this->setProperty("canMove", true);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::on_btnClose_clicked()
{
    this->close();
}

#ifndef WIDGETSTEERINGUART_H
#define WIDGETSTEERINGUART_H



#include "serialportio.h"

#include <QWidget>

namespace Ui {
class WidgetSteeringUart;
}

class WidgetSteeringUart : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetSteeringUart(QWidget *parent = nullptr);
    ~WidgetSteeringUart();

    void SetUartRecerver(serialportio* tReceiver,QString productModal);

    bool CheckComState();

    serialportio *sensorRecver_;

    QString mProductModal;



private slots:
    void on_btnBaudrate_clicked();

    void on_btnVersion_clicked();

    void on_btnOutSwitch_clicked();

    void on_btnWiperPeriod_clicked();

    void on_btnCycleCnt_clicked();

    void on_btnStartWiper_clicked();

    void on_btnSaveConfig_clicked();

    void on_btnSystemReset_clicked();


    void on_btnFrameFreq_clicked();

    void on_btnOutputFormat_clicked();

    void on_btnFactoryReset_clicked();

private:
    Ui::WidgetSteeringUart *ui;
};

#endif // WIDGETUART_H

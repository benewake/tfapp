/********************************************************************************
** Form generated from reading UI file 'widgetcan.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETCAN_H
#define UI_WIDGETCAN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetCAN
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QLabel *lbResetResult;
    QPushButton *btnVersion;
    QLabel *lbConfigResult;
    QPushButton *btnSystemReset;
    QPushButton *btnFactoryReset;
    QLabel *lbVersion;
    QPushButton *btnSaveConfig;
    QLabel *lbFactoryResetResult;
    QComboBox *cbFrameFreq;
    QPushButton *btnFrameFreq;
    QComboBox *cbOutputSwitch;
    QPushButton *btnOutSwitch;
    QLabel *label_4;
    QComboBox *cbBaudrate;
    QLabel *label;
    QLineEdit *etCANRxId;
    QLabel *label_2;
    QLineEdit *etCANTxId;
    QLabel *label_3;
    QComboBox *cbFrameTyp;
    QPushButton *btnCANParamSet;

    void setupUi(QWidget *WidgetCAN)
    {
        if (WidgetCAN->objectName().isEmpty())
            WidgetCAN->setObjectName(QString::fromUtf8("WidgetCAN"));
        WidgetCAN->resize(440, 433);
        gridLayout = new QGridLayout(WidgetCAN);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lbResetResult = new QLabel(WidgetCAN);
        lbResetResult->setObjectName(QString::fromUtf8("lbResetResult"));
        lbResetResult->setMinimumSize(QSize(0, 30));
        lbResetResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbResetResult, 5, 2, 1, 1);

        btnVersion = new QPushButton(WidgetCAN);
        btnVersion->setObjectName(QString::fromUtf8("btnVersion"));
        btnVersion->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnVersion, 0, 2, 1, 1);

        lbConfigResult = new QLabel(WidgetCAN);
        lbConfigResult->setObjectName(QString::fromUtf8("lbConfigResult"));
        lbConfigResult->setMinimumSize(QSize(0, 30));
        lbConfigResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbConfigResult, 3, 2, 1, 1);

        btnSystemReset = new QPushButton(WidgetCAN);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSystemReset, 4, 2, 1, 1);

        btnFactoryReset = new QPushButton(WidgetCAN);
        btnFactoryReset->setObjectName(QString::fromUtf8("btnFactoryReset"));
        btnFactoryReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFactoryReset, 6, 2, 1, 1);

        lbVersion = new QLabel(WidgetCAN);
        lbVersion->setObjectName(QString::fromUtf8("lbVersion"));
        lbVersion->setMinimumSize(QSize(0, 30));
        lbVersion->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbVersion, 1, 2, 1, 1);

        btnSaveConfig = new QPushButton(WidgetCAN);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 2, 2, 1, 1);

        lbFactoryResetResult = new QLabel(WidgetCAN);
        lbFactoryResetResult->setObjectName(QString::fromUtf8("lbFactoryResetResult"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbFactoryResetResult->sizePolicy().hasHeightForWidth());
        lbFactoryResetResult->setSizePolicy(sizePolicy);
        lbFactoryResetResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbFactoryResetResult, 7, 2, 1, 1);

        cbFrameFreq = new QComboBox(WidgetCAN);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 0, 0, 1, 1);

        btnFrameFreq = new QPushButton(WidgetCAN);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFrameFreq, 0, 1, 1, 1);

        cbOutputSwitch = new QComboBox(WidgetCAN);
        cbOutputSwitch->addItem(QString());
        cbOutputSwitch->addItem(QString());
        cbOutputSwitch->setObjectName(QString::fromUtf8("cbOutputSwitch"));
        cbOutputSwitch->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbOutputSwitch, 1, 0, 1, 1);

        btnOutSwitch = new QPushButton(WidgetCAN);
        btnOutSwitch->setObjectName(QString::fromUtf8("btnOutSwitch"));
        btnOutSwitch->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnOutSwitch, 1, 1, 1, 1);

        label_4 = new QLabel(WidgetCAN);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        cbBaudrate = new QComboBox(WidgetCAN);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbBaudrate, 2, 1, 1, 1);

        label = new QLabel(WidgetCAN);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 3, 0, 1, 1);

        etCANRxId = new QLineEdit(WidgetCAN);
        etCANRxId->setObjectName(QString::fromUtf8("etCANRxId"));
        sizePolicy.setHeightForWidth(etCANRxId->sizePolicy().hasHeightForWidth());
        etCANRxId->setSizePolicy(sizePolicy);
        etCANRxId->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etCANRxId, 3, 1, 1, 1);

        label_2 = new QLabel(WidgetCAN);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 4, 0, 1, 1);

        etCANTxId = new QLineEdit(WidgetCAN);
        etCANTxId->setObjectName(QString::fromUtf8("etCANTxId"));
        sizePolicy.setHeightForWidth(etCANTxId->sizePolicy().hasHeightForWidth());
        etCANTxId->setSizePolicy(sizePolicy);
        etCANTxId->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etCANTxId, 4, 1, 1, 1);

        label_3 = new QLabel(WidgetCAN);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 5, 0, 1, 1);

        cbFrameTyp = new QComboBox(WidgetCAN);
        cbFrameTyp->addItem(QString());
        cbFrameTyp->addItem(QString());
        cbFrameTyp->setObjectName(QString::fromUtf8("cbFrameTyp"));
        cbFrameTyp->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameTyp, 5, 1, 1, 1);

        btnCANParamSet = new QPushButton(WidgetCAN);
        btnCANParamSet->setObjectName(QString::fromUtf8("btnCANParamSet"));
        btnCANParamSet->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnCANParamSet, 6, 0, 1, 2);


        gridLayout->addLayout(gridLayout_2, 1, 0, 1, 1);


        retranslateUi(WidgetCAN);

        QMetaObject::connectSlotsByName(WidgetCAN);
    } // setupUi

    void retranslateUi(QWidget *WidgetCAN)
    {
        WidgetCAN->setWindowTitle(QCoreApplication::translate("WidgetCAN", "Form", nullptr));
        lbResetResult->setText(QCoreApplication::translate("WidgetCAN", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        btnVersion->setText(QCoreApplication::translate("WidgetCAN", "\350\216\267\345\217\226\347\211\210\346\234\254\345\217\267", nullptr));
        lbConfigResult->setText(QCoreApplication::translate("WidgetCAN", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("WidgetCAN", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        btnFactoryReset->setText(QCoreApplication::translate("WidgetCAN", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        lbVersion->setText(QCoreApplication::translate("WidgetCAN", "\347\211\210\346\234\254\345\217\267\357\274\232", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("WidgetCAN", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        lbFactoryResetResult->setText(QCoreApplication::translate("WidgetCAN", "\345\207\272\345\216\202\346\250\241\345\274\217\347\273\223\346\236\234", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetCAN", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetCAN", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetCAN", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetCAN", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetCAN", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetCAN", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetCAN", "1000", nullptr));

        btnFrameFreq->setText(QCoreApplication::translate("WidgetCAN", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        cbOutputSwitch->setItemText(0, QCoreApplication::translate("WidgetCAN", "\344\275\277\350\203\275", nullptr));
        cbOutputSwitch->setItemText(1, QCoreApplication::translate("WidgetCAN", "\345\205\263\351\227\255", nullptr));

        btnOutSwitch->setText(QCoreApplication::translate("WidgetCAN", "\350\276\223\345\207\272\345\274\200\345\205\263", nullptr));
        label_4->setText(QCoreApplication::translate("WidgetCAN", "\350\256\276\347\275\256CAN\346\263\242\347\211\271\347\216\207(Kbps)", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("WidgetCAN", "1000", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("WidgetCAN", "900", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("WidgetCAN", "800", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("WidgetCAN", "666", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("WidgetCAN", "600", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("WidgetCAN", "500", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("WidgetCAN", "400", nullptr));
        cbBaudrate->setItemText(7, QCoreApplication::translate("WidgetCAN", "300", nullptr));
        cbBaudrate->setItemText(8, QCoreApplication::translate("WidgetCAN", "250", nullptr));
        cbBaudrate->setItemText(9, QCoreApplication::translate("WidgetCAN", "225", nullptr));
        cbBaudrate->setItemText(10, QCoreApplication::translate("WidgetCAN", "200", nullptr));
        cbBaudrate->setItemText(11, QCoreApplication::translate("WidgetCAN", "160", nullptr));
        cbBaudrate->setItemText(12, QCoreApplication::translate("WidgetCAN", "150", nullptr));
        cbBaudrate->setItemText(13, QCoreApplication::translate("WidgetCAN", "144", nullptr));
        cbBaudrate->setItemText(14, QCoreApplication::translate("WidgetCAN", "125", nullptr));
        cbBaudrate->setItemText(15, QCoreApplication::translate("WidgetCAN", "120", nullptr));
        cbBaudrate->setItemText(16, QCoreApplication::translate("WidgetCAN", "100", nullptr));

        label->setText(QCoreApplication::translate("WidgetCAN", "\350\256\276\347\275\256CAN\346\216\245\346\224\266ID", nullptr));
        etCANRxId->setPlaceholderText(QCoreApplication::translate("WidgetCAN", "0-10000\350\214\203\345\233\264\347\232\204\346\255\243\346\225\264\346\225\260", nullptr));
        label_2->setText(QCoreApplication::translate("WidgetCAN", "\350\256\276\347\275\256CAN\345\217\221\351\200\201ID", nullptr));
        etCANTxId->setPlaceholderText(QCoreApplication::translate("WidgetCAN", "0-10000\350\214\203\345\233\264\347\232\204\346\255\243\346\225\264\346\225\260", nullptr));
        label_3->setText(QCoreApplication::translate("WidgetCAN", "\350\256\276\347\275\256CAN\345\270\247\347\261\273\345\236\213", nullptr));
        cbFrameTyp->setItemText(0, QCoreApplication::translate("WidgetCAN", "\346\240\207\345\207\206\345\270\247", nullptr));
        cbFrameTyp->setItemText(1, QCoreApplication::translate("WidgetCAN", "\346\211\251\345\261\225\345\270\247", nullptr));

        btnCANParamSet->setText(QCoreApplication::translate("WidgetCAN", "\350\256\276\347\275\256CAN\345\217\202\346\225\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetCAN: public Ui_WidgetCAN {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETCAN_H

#include "widgetrs485.h"
#include "ui_widgetrs485.h"

#include <QMessageBox>

WidgetRs485::WidgetRs485(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetRs485)
{
    ui->setupUi(this);
    ui->cbBaudrate->setEditable(true);



}

WidgetRs485::~WidgetRs485()
{
    delete ui;
}

bool WidgetRs485::CheckComState()
{
    if(sensorRecver_==nullptr)
        return false;

    if(!sensorRecver_->isOpen())
    {
        QMessageBox::warning(NULL, "Error", tr(u8"未连接Com口"));
        return false;
    }

    return true;
}

void WidgetRs485::SetUartRecerver(serialportio *tReceiver,QString productModal)
{
    this->sensorRecver_ = tReceiver;
    this->mProductModal = productModal;

    if(this->mProductModal=="TF03")
    {
        ui->btnLowConsumption->setDisabled(true);
    }


}


void WidgetRs485::on_btnGetVersion_clicked()
{
    if(!CheckComState()) return;
     bool result;
     QString s_version;

     s_version = sensorRecver_->send_cmd_id_version(result);
     if(false == result)
     {
          ui->lbVersion->setText(tr(u8"版本号获取失败"));
     }else{
          ui->lbVersion->setText(s_version);
     }
}


void WidgetRs485::on_btnSetBaudrate_clicked()
{
    if(!CheckComState()) return;

     uint32_t baudrate;
     QString cont;

     baudrate = ui->cbBaudrate->currentText().toUInt();

     if(sensorRecver_->send_cmd_id_baud_rate(baudrate))
     {
         qDebug("波特率设定成功！");
         QMessageBox::information(NULL, tr(u8"提示"), tr(u8"波特率设定成功！"));
     }else{
         qDebug("波特率设定失败！");
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"波特率设定失败！"));
     }
}


void WidgetRs485::on_btnFrameFreq_clicked()
{
   if(!CheckComState()) return;

   if(sensorRecver_->send_cmd_id_frame_rate(ui->cbFrameFreq->currentText().toUShort()))
   {
      qDebug("帧率设定成功！");
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"帧率设定成功"));

   }else{
       qDebug("帧率设定失败！");
       QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"帧率设定失败"));
   }
}



void WidgetRs485::on_btnOutputSwitch_clicked()
{
    if(!CheckComState()) return;

   int index = ui->cbOutputSwitch->currentIndex();
    E_Output_Enable_Type type;
   if(index==0){
        type = E_Output_Enable_Type::OUTPUT_Enable;
   }else {
        type = E_Output_Enable_Type::OUTPUT_Disable;
    }


   if(sensorRecver_->send_cmd_id_output_en(type))
   {
      qDebug("设定成功！");
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));
   }
   else
   {
      qDebug("设定失败");
      QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
   }

}


void WidgetRs485::on_btnOutputFormat_clicked()
{
    if(!CheckComState()) return;

    E_OutputFormat_Type outputFormat;

    if(ui->cbOutputFormat->currentIndex()==0)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_9BYTECM;
    }
    else if(ui->cbOutputFormat->currentIndex()==1)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_9BYTEMM;
    }
    else if(ui->cbOutputFormat->currentIndex()==2)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_PIX;
    }


    if(sensorRecver_->send_cmd_id_output_format(outputFormat))
    {
       qDebug("设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));


    }else{
       qDebug("设定失败");
       QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
    }


}


void WidgetRs485::on_btnLowConsumption_clicked()
{

   if(!CheckComState()) return;

   uint sampleRate = ui->cbLowConsumption->currentText().toUInt();

   if(sensorRecver_->send_cmd_low_consumption(sampleRate))
   {
      qDebug("低功耗设定成功！");
      QMessageBox::information(NULL, "提示", tr(u8"低功耗设定成功"));

   }else{
       qDebug("低功耗设定失败！");
      QMessageBox::warning(NULL, "提示", tr(u8"低功耗设定失败"));
   }

}


void WidgetRs485::on_btnFactoryReset_clicked()
{
    if(!CheckComState()) return;
     if(sensorRecver_->send_cmd_id_restore_default())
     {
         ui->lbFactoryResetResult->setText(tr(u8"恢复出厂成功"));
     }else{
         ui->lbFactoryResetResult->setText(tr(u8"恢复出厂失败"));
     }

}


void WidgetRs485::on_btnSaveConfig_clicked()
{

    if(!CheckComState()) return;

    QString cont;
    cont.append(tr(u8"保存配置"));
    ui->lbConfigResult->setText("");
    if(sensorRecver_->send_cmd_id_save_settings())
    {
        ui->lbConfigResult->setText(tr(u8"配置保存成功！"));
    }else{
       ui->lbConfigResult->setText(tr(u8"配置保存失败！"));
    }

}



void WidgetRs485::on_btnSystemReset_clicked()
{
    if(!CheckComState()) return;
    QString cont;
    cont.append(tr(u8"复位"));
    QMessageBox  msgBox;
   if(sensorRecver_->send_cmd_id_soft_reset())
   {
      qDebug("复位成功！");
      //QMessageBox::information(NULL, tr(u8"提示"), tr(u8"复位成功！"));
      ui->lbResetResult->setText(tr(u8"复位成功！"));


   }else{
      qDebug("复位失败");
      //QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"复位失败"));
       ui->lbResetResult->setText(tr(u8"复位失败！"));
   }


}



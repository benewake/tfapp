/********************************************************************************
** Form generated from reading UI file 'widgetsteeringmodbus.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETSTEERINGMODBUS_H
#define UI_WIDGETSTEERINGMODBUS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetSteeringModbus
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QPushButton *btnModbusAddress;
    QPushButton *btnSaveConfig;
    QLineEdit *etCycleCnt;
    QPushButton *btnBaudrate;
    QComboBox *cbFrameFreq;
    QComboBox *cbModbusContract;
    QLabel *lbCurDistance;
    QLabel *lbSaveConfig;
    QPushButton *btnModbusEnable;
    QPushButton *btnGetDistance;
    QPushButton *btnOutFreq;
    QLineEdit *etAddress;
    QPushButton *btnSignalQuality;
    QComboBox *cbBaudrate;
    QHBoxLayout *horizontalLayout;
    QLineEdit *etWiperPeriod;
    QComboBox *cbWiperPeriodUnit;
    QPushButton *btnWiperPeriod;
    QLabel *lbCurSignal;
    QPushButton *btnRoundtripCnt;
    QLabel *lbStartWiper;
    QPushButton *btnStartWiper;

    void setupUi(QWidget *WidgetSteeringModbus)
    {
        if (WidgetSteeringModbus->objectName().isEmpty())
            WidgetSteeringModbus->setObjectName(QString::fromUtf8("WidgetSteeringModbus"));
        WidgetSteeringModbus->resize(440, 433);
        gridLayout = new QGridLayout(WidgetSteeringModbus);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        btnModbusAddress = new QPushButton(WidgetSteeringModbus);
        btnModbusAddress->setObjectName(QString::fromUtf8("btnModbusAddress"));
        btnModbusAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnModbusAddress, 1, 1, 1, 1);

        btnSaveConfig = new QPushButton(WidgetSteeringModbus);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 4, 2, 1, 1);

        etCycleCnt = new QLineEdit(WidgetSteeringModbus);
        etCycleCnt->setObjectName(QString::fromUtf8("etCycleCnt"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(etCycleCnt->sizePolicy().hasHeightForWidth());
        etCycleCnt->setSizePolicy(sizePolicy);
        etCycleCnt->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etCycleCnt, 5, 0, 1, 1);

        btnBaudrate = new QPushButton(WidgetSteeringModbus);
        btnBaudrate->setObjectName(QString::fromUtf8("btnBaudrate"));
        btnBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnBaudrate, 2, 1, 1, 1);

        cbFrameFreq = new QComboBox(WidgetSteeringModbus);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 3, 0, 1, 1);

        cbModbusContract = new QComboBox(WidgetSteeringModbus);
        cbModbusContract->addItem(QString());
        cbModbusContract->addItem(QString());
        cbModbusContract->setObjectName(QString::fromUtf8("cbModbusContract"));
        cbModbusContract->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbModbusContract, 0, 0, 1, 1);

        lbCurDistance = new QLabel(WidgetSteeringModbus);
        lbCurDistance->setObjectName(QString::fromUtf8("lbCurDistance"));
        lbCurDistance->setMinimumSize(QSize(0, 30));
        lbCurDistance->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbCurDistance, 1, 2, 1, 1);

        lbSaveConfig = new QLabel(WidgetSteeringModbus);
        lbSaveConfig->setObjectName(QString::fromUtf8("lbSaveConfig"));
        lbSaveConfig->setMinimumSize(QSize(0, 30));
        lbSaveConfig->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbSaveConfig, 5, 2, 1, 1);

        btnModbusEnable = new QPushButton(WidgetSteeringModbus);
        btnModbusEnable->setObjectName(QString::fromUtf8("btnModbusEnable"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnModbusEnable->sizePolicy().hasHeightForWidth());
        btnModbusEnable->setSizePolicy(sizePolicy1);
        btnModbusEnable->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnModbusEnable, 0, 1, 1, 1);

        btnGetDistance = new QPushButton(WidgetSteeringModbus);
        btnGetDistance->setObjectName(QString::fromUtf8("btnGetDistance"));
        btnGetDistance->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnGetDistance, 0, 2, 1, 1);

        btnOutFreq = new QPushButton(WidgetSteeringModbus);
        btnOutFreq->setObjectName(QString::fromUtf8("btnOutFreq"));
        btnOutFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnOutFreq, 3, 1, 1, 1);

        etAddress = new QLineEdit(WidgetSteeringModbus);
        etAddress->setObjectName(QString::fromUtf8("etAddress"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(etAddress->sizePolicy().hasHeightForWidth());
        etAddress->setSizePolicy(sizePolicy2);
        etAddress->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(etAddress, 1, 0, 1, 1);

        btnSignalQuality = new QPushButton(WidgetSteeringModbus);
        btnSignalQuality->setObjectName(QString::fromUtf8("btnSignalQuality"));
        btnSignalQuality->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSignalQuality, 2, 2, 1, 1);

        cbBaudrate = new QComboBox(WidgetSteeringModbus);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbBaudrate, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(5, 5, 5, 5);
        etWiperPeriod = new QLineEdit(WidgetSteeringModbus);
        etWiperPeriod->setObjectName(QString::fromUtf8("etWiperPeriod"));
        QSizePolicy sizePolicy3(QSizePolicy::Ignored, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(etWiperPeriod->sizePolicy().hasHeightForWidth());
        etWiperPeriod->setSizePolicy(sizePolicy3);
        etWiperPeriod->setMinimumSize(QSize(0, 30));
        etWiperPeriod->setFrame(true);

        horizontalLayout->addWidget(etWiperPeriod);

        cbWiperPeriodUnit = new QComboBox(WidgetSteeringModbus);
        cbWiperPeriodUnit->addItem(QString());
        cbWiperPeriodUnit->addItem(QString());
        cbWiperPeriodUnit->setObjectName(QString::fromUtf8("cbWiperPeriodUnit"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(cbWiperPeriodUnit->sizePolicy().hasHeightForWidth());
        cbWiperPeriodUnit->setSizePolicy(sizePolicy4);
        cbWiperPeriodUnit->setMinimumSize(QSize(0, 0));

        horizontalLayout->addWidget(cbWiperPeriodUnit);


        gridLayout_2->addLayout(horizontalLayout, 4, 0, 1, 1);

        btnWiperPeriod = new QPushButton(WidgetSteeringModbus);
        btnWiperPeriod->setObjectName(QString::fromUtf8("btnWiperPeriod"));
        btnWiperPeriod->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnWiperPeriod, 4, 1, 1, 1);

        lbCurSignal = new QLabel(WidgetSteeringModbus);
        lbCurSignal->setObjectName(QString::fromUtf8("lbCurSignal"));
        lbCurSignal->setMinimumSize(QSize(0, 30));
        lbCurSignal->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbCurSignal, 3, 2, 1, 1);

        btnRoundtripCnt = new QPushButton(WidgetSteeringModbus);
        btnRoundtripCnt->setObjectName(QString::fromUtf8("btnRoundtripCnt"));
        btnRoundtripCnt->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnRoundtripCnt, 5, 1, 1, 1);

        lbStartWiper = new QLabel(WidgetSteeringModbus);
        lbStartWiper->setObjectName(QString::fromUtf8("lbStartWiper"));
        lbStartWiper->setMinimumSize(QSize(0, 30));
        lbStartWiper->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbStartWiper, 6, 0, 1, 1);

        btnStartWiper = new QPushButton(WidgetSteeringModbus);
        btnStartWiper->setObjectName(QString::fromUtf8("btnStartWiper"));
        btnStartWiper->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnStartWiper, 6, 1, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(WidgetSteeringModbus);

        QMetaObject::connectSlotsByName(WidgetSteeringModbus);
    } // setupUi

    void retranslateUi(QWidget *WidgetSteeringModbus)
    {
        WidgetSteeringModbus->setWindowTitle(QCoreApplication::translate("WidgetSteeringModbus", "Form", nullptr));
        btnModbusAddress->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\256\276\347\275\256\345\234\260\345\235\200", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("WidgetSteeringModbus", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        etCycleCnt->setPlaceholderText(QCoreApplication::translate("WidgetSteeringModbus", "1~10\345\206\205\346\255\243\346\225\264\346\225\260", nullptr));
        btnBaudrate->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\256\276\347\275\256\346\263\242\347\211\271\347\216\207", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetSteeringModbus", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetSteeringModbus", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetSteeringModbus", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetSteeringModbus", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetSteeringModbus", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetSteeringModbus", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetSteeringModbus", "1000", nullptr));

        cbModbusContract->setItemText(0, QCoreApplication::translate("WidgetSteeringModbus", "\344\275\277\350\203\275", nullptr));
        cbModbusContract->setItemText(1, QCoreApplication::translate("WidgetSteeringModbus", "\345\205\263\351\227\255", nullptr));

        lbCurDistance->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\267\235\347\246\273\357\274\232", nullptr));
        lbSaveConfig->setText(QCoreApplication::translate("WidgetSteeringModbus", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        btnModbusEnable->setText(QCoreApplication::translate("WidgetSteeringModbus", "Modbus\345\215\217\350\256\256", nullptr));
        btnGetDistance->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\216\267\345\217\226\350\267\235\347\246\273\345\200\274", nullptr));
        btnOutFreq->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        etAddress->setPlaceholderText(QCoreApplication::translate("WidgetSteeringModbus", "\350\257\267\350\276\223\345\205\245Modbus\345\234\260\345\235\200", nullptr));
        btnSignalQuality->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\216\267\345\217\226\344\277\241\345\217\267\345\274\272\345\272\246", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("WidgetSteeringModbus", "9600", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("WidgetSteeringModbus", "14400", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("WidgetSteeringModbus", "19200", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("WidgetSteeringModbus", "56000", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("WidgetSteeringModbus", "115200", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("WidgetSteeringModbus", "460800", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("WidgetSteeringModbus", "921600", nullptr));

        cbWiperPeriodUnit->setItemText(0, QCoreApplication::translate("WidgetSteeringModbus", "\345\210\206\351\222\237", nullptr));
        cbWiperPeriodUnit->setItemText(1, QCoreApplication::translate("WidgetSteeringModbus", "\345\260\217\346\227\266", nullptr));

        btnWiperPeriod->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\256\276\347\275\256\351\233\250\345\210\267\345\221\250\346\234\237", nullptr));
        lbCurSignal->setText(QCoreApplication::translate("WidgetSteeringModbus", "\344\277\241\345\217\267\345\274\272\345\272\246\357\274\232", nullptr));
        btnRoundtripCnt->setText(QCoreApplication::translate("WidgetSteeringModbus", "\350\256\276\347\275\256\345\276\200\350\277\224\346\254\241\346\225\260", nullptr));
        lbStartWiper->setText(QCoreApplication::translate("WidgetSteeringModbus", "\346\230\276\347\244\272\346\230\257\345\220\246\345\220\257\345\212\250", nullptr));
        btnStartWiper->setText(QCoreApplication::translate("WidgetSteeringModbus", "\347\253\213\345\215\263\345\220\257\345\212\250\351\233\250\345\210\267", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WidgetSteeringModbus: public Ui_WidgetSteeringModbus {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETSTEERINGMODBUS_H

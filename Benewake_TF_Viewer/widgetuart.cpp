#include "widgetuart.h"
#include "ui_widgetuart.h"
#include "serialportio.h"
#include <QMessageBox>

WidgetUart::WidgetUart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetUart)
{
    ui->setupUi(this);



    InitDatas();
}

WidgetUart::~WidgetUart()
{
    delete ui;

}

void WidgetUart::SetUartRecerver(serialportio *tReceiver,QString productModal)
{
    this->sensorRecver_ = tReceiver;
    this->mProductModal = productModal;

//    if(this->mProductModal=="TF03")
//    {
//        ui->btnLowConsumption->setDisabled(true);
//    }
}

void WidgetUart::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
       switch (e->type()) {
       case QEvent::LanguageChange:
           ui->retranslateUi(this);
           break;
       default:
           break;
       }
}

void WidgetUart::InitDatas()
{

    ui->cbBaudrate->setEditable(true);

}

bool WidgetUart::CheckComState()
{
    try{

        if(sensorRecver_==nullptr)
            return false;

        if(!sensorRecver_->isOpen())
        {
            QMessageBox::warning(NULL, "Error", tr(u8"未连接Com口"));
            qWarning("串口没有打开");
            return false;
        }

    }

    catch(QString ex){
        qErrnoWarning(1001,ex.toStdString().data());
        return false;
    }

    return true;
}

void WidgetUart::on_btnBaudrate_clicked()
{
   if(!CheckComState()) return;

    uint32_t baudrate;
    QString cont;
    baudrate = ui->cbBaudrate->currentText().toUInt();

    qDebug("波特率设定");
    if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
    {

        if(sensorRecver_->send_cmd_id_baud_rate(baudrate))
        {
           qDebug("波特率设定成功！");


        }else{
           qDebug("波特率设定失败！");
           QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"波特率设定失败！"));
        }
    }

    else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
    {
        if(sensorRecver_->send_cmd_id_baud_rate_pro(baudrate))
        {
           qDebug("波特率设定成功！");
           QMessageBox::information(NULL, tr(u8"错误"), tr(u8"波特率设定成功！"));

        }else{
            qDebug("波特率设定失败！");
            QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"波特率设定失败！"));
        }
    }

}

void WidgetUart::on_btnGetVersion_clicked()
{
   qDebug("获取版本号");
  if(!CheckComState()) return;
   bool result;
   QString s_version;

   if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
    {
        s_version = sensorRecver_->send_cmd_id_version(result);
        if(false == result)
        {
            ui->lbVersion->setText(tr(u8"版本号获取失败"));
            qDebug("版本号获取失败");
        }else{
            ui->lbVersion->setText(s_version);

        }
    }
    else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
    {
        s_version = sensorRecver_->send_cmd_id_version_pro(result);
        if(false == result)
        {
            ui->lbVersion->setText(tr(u8"版本号获取失败"));
            qDebug("版本号获取失败");
        }else{
             ui->lbVersion->setText(s_version);
        }

    }

}

void WidgetUart::on_btnFrameFreq_clicked()
{
     if(!CheckComState()) return;
     if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
     {
         if(sensorRecver_->send_cmd_id_frame_rate(ui->cbFrameFreq->currentText().toUShort()))
         {
            qDebug("帧率设定成功！");
            QMessageBox::information(NULL, tr(u8"提示"), tr(u8"帧率设定成功"));

         }else{
             qDebug("帧率设定失败！");
             QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"帧率设定失败"));
         }
     }
     else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
     {
         if(sensorRecver_->send_cmd_id_frame_rate_pro(ui->cbFrameFreq->currentText().toUShort()))
         {
            qDebug("帧率设定成功！");
            QMessageBox::information(NULL, tr(u8"提示"), tr(u8"帧率设定成功"));

         }else{
             qDebug("帧率设定失败！");
             QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"帧率设定失败"));
         }
     }



}

void WidgetUart::on_btnOutSwitch_clicked()
{

     if(!CheckComState()) return;

    int index = ui->cbOutSwtich->currentIndex();
     E_Output_Enable_Type type;
     if(index==0)
     {
         type = E_Output_Enable_Type::OUTPUT_Enable;
     }
     else
     {
         type = E_Output_Enable_Type::OUTPUT_Disable;
     }

    if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
     {
         if(sensorRecver_->send_cmd_id_output_en(type))
         {
            qDebug("设定成功！");
            QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));
         }
         else
         {
            qDebug("设定失败");
            QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
         }
     }
     else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
     {
         if(sensorRecver_->send_cmd_id_output_en_pro(type))
         {
            qDebug("设定成功！");
            QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));
         }
         else
         {
            qDebug("设定失败");
            QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
         }
     }


}


void WidgetUart::on_btnOutputFormat_clicked()
{
    if(!CheckComState()) return;

    E_OutputFormat_Type outputFormat;



    if(ui->cbOutputFormat->currentIndex()==0)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_9BYTECM;
    }
    else if(ui->cbOutputFormat->currentIndex()==1)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_9BYTEMM;
    }
    else if(ui->cbOutputFormat->currentIndex()==2)
    {
        outputFormat = E_OutputFormat_Type::FORMAT_PIX;
    }
    

    if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
    {
    
        if(sensorRecver_->send_cmd_id_output_format(outputFormat))
        {
           qDebug("设定成功！");
           QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));


        }else{
           qDebug("设定失败");
           QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
        }
    }
    else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
    {
        if(sensorRecver_->send_cmd_id_output_format_pro(outputFormat))
        {
           qDebug("设定成功！");
           QMessageBox::information(NULL, tr(u8"提示"), tr(u8"设定成功！"));


        }else{
           qDebug("设定失败");
           QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"设定失败"));
        }

    }

}

/**
 * @brief WidgetUart::on_btnLowConsumption_clicked
 * 低功耗设置
 */

void WidgetUart::on_btnLowConsumption_clicked()
{
     if(!CheckComState()) return;
    QString cont = tr(u8"获取dist");
    uint sampleRate = ui->cbLowConsumption->currentText().toUInt();
    if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
    {
        if(sensorRecver_->send_cmd_low_consumption(sampleRate))
        {
           qDebug("低功耗设定成功！");
           QMessageBox::information(NULL, "提示", tr(u8"低功耗设定成功"));

        }else{
            qDebug("低功耗设定失败！");
           QMessageBox::warning(NULL, "提示", tr(u8"低功耗设定失败"));
        }
    }
    else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
    {
        //无协议

    }
}



void WidgetUart::on_btnSaveConfig_clicked()
{
     if(!CheckComState()) return;

     QString cont;

     ui->lbConfigResult->setText("");

     if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
     {

         if(sensorRecver_->send_cmd_id_save_settings())
         {
             ui->lbConfigResult->setText(tr(u8"配置保存成功！"));
         }else
         {
            ui->lbConfigResult->setText(tr(u8"配置保存失败！"));
         }

     }
     else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
     {
         //待确定
//         if(sensorRecver_->send_cmd_id_save_settings_pro())
//         {
//             ui->lbConfigResult->setText(tr(u8"配置保存成功！"));
//         }else
//         {
//            ui->lbConfigResult->setText(tr(u8"配置保存失败！"));
//         }

     }



}




void WidgetUart::on_btnSystemReset_clicked()
{
     if(!CheckComState()) return;


      if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
      {
          if(sensorRecver_->send_cmd_id_soft_reset())
          {
             qDebug("系统复位成功");
              ui->lbResetResult->setText(tr(u8"系统复位成功"));


          }else{
             qDebug("系统复位失败");
              ui->lbResetResult->setText(tr(u8"系统复位失败"));
          }
      }
      else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
      {
          if(sensorRecver_->send_cmd_id_soft_reset_pro())
          {
             qDebug("系统复位成功");
              ui->lbResetResult->setText(tr(u8"系统复位成功"));


          }else{
             qDebug("系统复位失败");
              ui->lbResetResult->setText(tr(u8"系统复位失败"));
          }
      }

}




void WidgetUart::on_btnFallbackToFactory_clicked()
{
   if(!CheckComState()) return;
    if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
    {
        if(sensorRecver_->send_cmd_id_restore_default())
        {
            ui->lbFactoryResetResult->setText(tr(u8"恢复出厂成功"));
        }else{
            ui->lbFactoryResetResult->setText(tr(u8"恢复出厂失败"));
        }
    }
    else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
    {
        if(sensorRecver_->send_cmd_id_restore_default_pro())
        {
            ui->lbFactoryResetResult->setText(tr(u8"恢复出厂成功"));
        }else{
            ui->lbFactoryResetResult->setText(tr(u8"恢复出厂失败"));
        }
    }

}





void WidgetUart::on_btnIOSetting_clicked()
{

    if(ui->etDist->text()=="")
    {
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"请输入临界值"));
         return;
    }

    if(ui->etZone->text()=="")
    {
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"请输入滞回区值"));
         return;
    }

    if(ui->etDelay1->text()=="")
    {
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"请输入延迟时间一"));
         return;
    }

    if(ui->etDelay2->text()=="")
    {
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"请输入延迟时间二"));
         return;
    }

    if(!CheckComState()) return;

     uint iMode = ui->cbIOMode->currentIndex();
     uint distVal = ui->etDist->text().toUInt();
     uint zoneVal = ui->etZone->text().toUInt();
     uint delayVal1 = ui->etDelay1->text().toUInt();
     uint delayVal2 = ui->etDelay2->text().toUInt();

     if(distVal<0||distVal>2000)
     {
          QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"临界值必须在0~2000之间"));
          return;
     }


     if(zoneVal<0||zoneVal>2000)
     {
         QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"滞回区间必须在0~2000之间"));
         return;
     }


      if(this->mProductModal=="TF-Luna"||this->mProductModal=="TFmini-S"||this->mProductModal=="TFmini-Plus"||this->mProductModal=="TF03")
     {
         if(sensorRecver_->send_cmd_id_uart_setio(iMode,distVal,zoneVal,delayVal1,delayVal2))
         {
             QMessageBox::information(NULL, tr(u8"提示"), tr(u8"IO设置成功"));
         }else{
              QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"IO设置失败"));
         }
     }
     else if(this->mProductModal=="TF02-Pro"||this->mProductModal=="TF02-Pro-W")
     {
         //无协议



     }




}

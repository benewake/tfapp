#ifndef WIDGETRS485_H
#define WIDGETRS485_H



#include <QWidget>
#include "serialportio.h"



namespace Ui {
class WidgetRs485;
}

class WidgetRs485 : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetRs485(QWidget *parent = nullptr);
    ~WidgetRs485();

    bool CheckComState();
    void SetUartRecerver(serialportio *tReceiver, QString productModal);
private slots:
    void on_btnOutputFormat_clicked();

    void on_btnGetVersion_clicked();

    void on_btnSetBaudrate_clicked();

    void on_btnFrameFreq_clicked();

    void on_btnOutputSwitch_clicked();




    void on_btnLowConsumption_clicked();

    void on_btnFactoryReset_clicked();



    void on_btnSaveConfig_clicked();

    void on_btnSystemReset_clicked();


private:
    Ui::WidgetRs485 *ui;

    serialportio *sensorRecver_;

    QString mProductModal;
};

#endif // WIDGETUART_H

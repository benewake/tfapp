<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="176"/>
        <source>版本号：Benewake_TF_Viewer V0.1.0</source>
        <translation>Version：Benewake_TF_Viewer V0.1.0</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="195"/>
        <source>官网地址：Http://www.benewake.com</source>
        <translation>Website：Http://www.benewake.com</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="214"/>
        <source>版本号：400-880-9610</source>
        <translation>Phone Number：400-880-9610</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="252"/>
        <source>北醒（北京）光子科技有限公司</source>
        <translation type="unfinished">Beixing (Beijing) Photonics Technology Co., LTD</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="271"/>
        <source>©2022 Benewake Inc. 400-880-9610 | All rights reserved</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FailConnDialog</name>
    <message>
        <location filename="failconndialog.ui" line="20"/>
        <source>Connection Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="failconndialog.ui" line="96"/>
        <source>未检测到雷达，请连接至设备</source>
        <translation type="unfinished">No radar detected, please connect your device to the computer</translation>
    </message>
    <message>
        <location filename="failconndialog.ui" line="115"/>
        <source>欢迎使用北醒上位机</source>
        <translation type="unfinished"> Welcome to use Benewake GUI</translation>
    </message>
</context>
<context>
    <name>FormContent</name>
    <message>
        <location filename="formcontent.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="105"/>
        <source>通信端口</source>
        <translation>Comm Port</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="118"/>
        <source>波特率</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="141"/>
        <location filename="formcontent.cpp" line="516"/>
        <location filename="formcontent.cpp" line="535"/>
        <location filename="formcontent.cpp" line="587"/>
        <location filename="formcontent.cpp" line="603"/>
        <location filename="formcontent.cpp" line="813"/>
        <source>连接</source>
        <oldsource>连接雷达</oldsource>
        <translation type="unfinished">Connect Radar</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="154"/>
        <source>接口协议</source>
        <translation>IO Contract</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="177"/>
        <source>产品型号</source>
        <translation>Product Modal</translation>
    </message>
    <message>
        <source>断开连接</source>
        <translation type="vanished">Disconnect</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="200"/>
        <source>CAN通道</source>
        <translation type="unfinished">CAN</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="214"/>
        <source>CAN1</source>
        <translation type="unfinished">CAN1</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="219"/>
        <source>CAN2</source>
        <translation type="unfinished">CAN2</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="240"/>
        <source>参数设置区</source>
        <translation>Parameter Setting</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="270"/>
        <source>数据采录区</source>
        <translation>Data Recording</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="282"/>
        <source>文件名称：</source>
        <translation>File Name</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="295"/>
        <source>记录</source>
        <translation>Record</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="308"/>
        <source>选择文件</source>
        <translation>Choose File</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="321"/>
        <source>Hex</source>
        <translation>Hex</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="337"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="350"/>
        <source>时间戳</source>
        <translation>Timestamp</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="398"/>
        <source>固件升级区</source>
        <translation>Firmware Update</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="410"/>
        <source>文件路径：</source>
        <translation>File Path:</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="423"/>
        <source>已完成0%</source>
        <translation>Progress:0%</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="436"/>
        <source>打开最新版固件</source>
        <translation>Open Latest firmware</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="449"/>
        <source>升级</source>
        <translation>Upgrade</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="462"/>
        <source>Bridge</source>
        <translation>Bridge</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="481"/>
        <source>注意：请将固件文件保存至全英文路径下</source>
        <translation>Tip:Please save firmware file to English Path</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="538"/>
        <source>测量距离 ：</source>
        <translation type="unfinished">Distance：</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="551"/>
        <source>信号强度 ：</source>
        <translation type="unfinished">Signal Strength:</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="564"/>
        <source>频率 ：</source>
        <translation>Frequency:</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="347"/>
        <source>测量距离：</source>
        <translation>Distance:</translation>
    </message>
    <message>
        <source>信号强度：</source>
        <translation type="vanished">Signal Strength:</translation>
    </message>
    <message>
        <source>频率：</source>
        <translation type="vanished">Frequency:</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="586"/>
        <source>中文</source>
        <translation>Chinese</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="611"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="684"/>
        <source>通信记录区</source>
        <translation>Communication Record Zone</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="740"/>
        <source>请输入雷达指令</source>
        <translation>Input the Radar command:</translation>
    </message>
    <message>
        <location filename="formcontent.ui" line="753"/>
        <source>发送</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="40"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="43"/>
        <source>Distance</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="348"/>
        <source>信号强度:</source>
        <translation>Signal Strength:</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="349"/>
        <source>采样频率:</source>
        <translation>Frequency:</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="384"/>
        <location filename="formcontent.cpp" line="390"/>
        <location filename="formcontent.cpp" line="402"/>
        <location filename="formcontent.cpp" line="409"/>
        <location filename="formcontent.cpp" line="416"/>
        <location filename="formcontent.cpp" line="424"/>
        <location filename="formcontent.cpp" line="435"/>
        <source>UART</source>
        <translation>UART</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="385"/>
        <location filename="formcontent.cpp" line="391"/>
        <location filename="formcontent.cpp" line="403"/>
        <location filename="formcontent.cpp" line="410"/>
        <location filename="formcontent.cpp" line="417"/>
        <source>IIC</source>
        <translation>IIC</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="386"/>
        <location filename="formcontent.cpp" line="392"/>
        <location filename="formcontent.cpp" line="404"/>
        <location filename="formcontent.cpp" line="411"/>
        <source>IO</source>
        <translation>IO</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="387"/>
        <source>TF-Luna</source>
        <translation>TF-Luna</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="393"/>
        <source>TFmini-S</source>
        <translation>TFmini-S</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="397"/>
        <location filename="formcontent.cpp" line="418"/>
        <location filename="formcontent.cpp" line="429"/>
        <location filename="formcontent.cpp" line="438"/>
        <source>RS485(Modbus)</source>
        <translation>RS485(Modbus)</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="398"/>
        <location filename="formcontent.cpp" line="430"/>
        <location filename="formcontent.cpp" line="440"/>
        <source>CAN</source>
        <translation>CAN</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="399"/>
        <source>TFmini-i</source>
        <translation>TFmini-i</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="405"/>
        <source>TFmini-Plus</source>
        <translation>TFmini-Plus</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="412"/>
        <source>TF02-Pro</source>
        <translation>TF02-Pro</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="419"/>
        <source>TF02-Pro-W</source>
        <translation>TF02-Pro-W</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="425"/>
        <source>TF-LM40</source>
        <translation>TF-LM40</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="431"/>
        <source>TF02-i</source>
        <translation>TF02-i</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="437"/>
        <source>RS485</source>
        <translation>RS485</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="483"/>
        <location filename="formcontent.cpp" line="502"/>
        <location filename="formcontent.cpp" line="559"/>
        <location filename="formcontent.cpp" line="575"/>
        <source>断开</source>
        <translation>Disconnect</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="818"/>
        <source>提示</source>
        <translation>Notice</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="436"/>
        <source>RS232</source>
        <translation>RS232</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="439"/>
        <source>4-20mA</source>
        <translation>4-20mA</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="442"/>
        <source>TF03</source>
        <translation>TF03</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">Notice</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="972"/>
        <source>error</source>
        <translation>error</translation>
    </message>
    <message>
        <location filename="formcontent.cpp" line="993"/>
        <source>打开录制文件</source>
        <translation>Open Record File</translation>
    </message>
</context>
<context>
    <name>Log4Qt::AppenderSkeleton</name>
    <message>
        <location filename="log4qt/appenderskeleton.cpp" line="137"/>
        <source>Activation of appender &apos;%1&apos; that requires layout and has no layout set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/appenderskeleton.cpp" line="228"/>
        <source>Use of non activated appender &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/appenderskeleton.cpp" line="236"/>
        <source>Use of closed appender &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/appenderskeleton.cpp" line="244"/>
        <source>Use of appender &apos;%1&apos; that requires layout and has no layout set</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::DailyRollingFileAppender</name>
    <message>
        <location filename="log4qt/dailyrollingfileappender.cpp" line="148"/>
        <source>Use of appender &apos;%1&apos; without having a valid date pattern set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/dailyrollingfileappender.cpp" line="215"/>
        <source>The pattern &apos;%1&apos; does not specify a frequency for appender &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::Factory</name>
    <message>
        <location filename="log4qt/helpers/factory.cpp" line="264"/>
        <source>Cannot convert to type &apos;%1&apos; for property &apos;%2&apos; on object of class &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/factory.cpp" line="377"/>
        <source>Unable to set property value on object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/factory.cpp" line="383"/>
        <source>Invalid null object pointer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/factory.cpp" line="392"/>
        <source>Invalid empty property name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/factory.cpp" line="410"/>
        <source>Property &apos;%1&apos; does not exist in class &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/factory.cpp" line="423"/>
        <source>Property &apos;%1&apos; is not writable in class &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::FileAppender</name>
    <message>
        <location filename="log4qt/fileappender.cpp" line="131"/>
        <source>Activation of Appender &apos;%1&apos; that requires file and has no file set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/fileappender.cpp" line="161"/>
        <source>Use of appender &apos;%1&apos; without open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/fileappender.cpp" line="224"/>
        <source>Unable to write to file &apos;%1&apos; for appender &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/fileappender.cpp" line="258"/>
        <source>Unable to open file &apos;%1&apos; for appender &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/fileappender.cpp" line="276"/>
        <source>Unable to remove file &apos;%1&apos; for appender &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/fileappender.cpp" line="292"/>
        <source>Unable to rename file &apos;%1&apos; to &apos;%2&apos; for appender &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::OptionConverter</name>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="103"/>
        <source>Missing closing bracket for opening bracket at %1. Invalid subsitution in value %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="151"/>
        <source>Invalid option string &apos;%1&apos; for a boolean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="207"/>
        <source>Invalid option string &apos;%1&apos; for a file size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="227"/>
        <source>Invalid option string &apos;%1&apos; for an integer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="242"/>
        <source>Invalid option string &apos;%1&apos; for an qint64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="260"/>
        <source>Invalid option string &apos;%1&apos; for a level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/optionconverter.cpp" line="299"/>
        <source>Invalid option string &apos;%1&apos; for a target</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::PatternFormatter</name>
    <message>
        <location filename="log4qt/helpers/patternformatter.cpp" line="535"/>
        <source>Found character &apos;%1&apos; where digit was expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/patternformatter.cpp" line="620"/>
        <source>Option &apos;%1&apos; cannot be converted into an integer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/helpers/patternformatter.cpp" line="628"/>
        <source>Option %1 isn&apos;t a positive integer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::PropertyConfigurator</name>
    <message>
        <location filename="log4qt/propertyconfigurator.cpp" line="146"/>
        <source>Unable to open property file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/propertyconfigurator.cpp" line="158"/>
        <source>Unable to read property file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/propertyconfigurator.cpp" line="370"/>
        <source>Missing appender definition for appender named &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/propertyconfigurator.cpp" line="380"/>
        <source>Unable to create appender of class &apos;%1&apos; namd &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/propertyconfigurator.cpp" line="428"/>
        <source>Missing layout definition for appender &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/propertyconfigurator.cpp" line="438"/>
        <source>Unable to create layoput of class &apos;%1&apos; requested by appender &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log4Qt::WriterAppender</name>
    <message>
        <location filename="log4qt/writerappender.cpp" line="137"/>
        <source>Activation of Appender &apos;%1&apos; that requires writer and has no writer set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="log4qt/writerappender.cpp" line="192"/>
        <source>Use of appender &apos;%1&apos; without a writer set</source>
        <translation type="unfinished">Use of appender &apos;%1&apos; without a writer set</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="maindialog.ui" line="31"/>
        <source>BenwakeTFViewer</source>
        <translation>BenwakeTFViewer</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="145"/>
        <source>Benewake_TF_Viewer V0.1.0</source>
        <translation>Benewake_TF_Viewer V0.1.0</translation>
    </message>
    <message>
        <location filename="maindialog.ui" line="222"/>
        <source>使用帮助</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="87"/>
        <source>获取当前配置文件</source>
        <oldsource>获取当前配置</oldsource>
        <translation>Get Current Config</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="91"/>
        <source>记录Log文件</source>
        <oldsource>记录log文件</oldsource>
        <translation>Record log file</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="95"/>
        <source>用户操作手册</source>
        <translation>Software user guide</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="98"/>
        <source>产品用户手册下载</source>
        <translation>Product user guide download</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="101"/>
        <source>关于</source>
        <translation>About</translation>
    </message>
</context>
<context>
    <name>ProductSelectDialog</name>
    <message>
        <location filename="productselectdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="productselectdialog.ui" line="50"/>
        <source>产品型号</source>
        <translation>Product Modal</translation>
    </message>
    <message>
        <location filename="productselectdialog.ui" line="73"/>
        <source>接口协议</source>
        <translation>Interface Contract</translation>
    </message>
    <message>
        <location filename="productselectdialog.ui" line="96"/>
        <source>取消</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="productselectdialog.ui" line="109"/>
        <source>确认</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="20"/>
        <location filename="productselectdialog.cpp" line="26"/>
        <location filename="productselectdialog.cpp" line="38"/>
        <location filename="productselectdialog.cpp" line="45"/>
        <location filename="productselectdialog.cpp" line="52"/>
        <location filename="productselectdialog.cpp" line="66"/>
        <source>UART</source>
        <translation>UART</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="21"/>
        <location filename="productselectdialog.cpp" line="27"/>
        <location filename="productselectdialog.cpp" line="39"/>
        <location filename="productselectdialog.cpp" line="46"/>
        <location filename="productselectdialog.cpp" line="53"/>
        <source>IIC</source>
        <translation>IIC</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="22"/>
        <location filename="productselectdialog.cpp" line="28"/>
        <location filename="productselectdialog.cpp" line="40"/>
        <location filename="productselectdialog.cpp" line="47"/>
        <source>IO</source>
        <translation>IO</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="23"/>
        <source>TF-Luna</source>
        <translation>TF-Luna</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="29"/>
        <source>TFmini-S</source>
        <translation>TFmini-S</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="33"/>
        <location filename="productselectdialog.cpp" line="54"/>
        <location filename="productselectdialog.cpp" line="60"/>
        <source>RS485（Modbus)</source>
        <translation>RS485(Modbus) {485（?}</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="34"/>
        <location filename="productselectdialog.cpp" line="61"/>
        <source>CAN</source>
        <translation>CAN</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="35"/>
        <source>TFmini-i</source>
        <translation>TFmini-i</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="41"/>
        <source>TFmini Plus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="48"/>
        <source>TF02-Pro</source>
        <translation>TF02-Pro</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="55"/>
        <source>TF02-Pro-W&amp;TF-LM40</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="62"/>
        <source>TF02-i</source>
        <translation>TF02-i</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="67"/>
        <source>RS485</source>
        <translation>RS485</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="68"/>
        <source>RS232</source>
        <translation>RS232</translation>
    </message>
    <message>
        <location filename="productselectdialog.cpp" line="69"/>
        <source>TF03</source>
        <translation>TF03</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="serialportio.h" line="222"/>
        <source>连续模式</source>
        <translation>Continuous Mode</translation>
    </message>
    <message>
        <location filename="serialportio.h" line="223"/>
        <source>触发模式</source>
        <translation>Trigger Mode</translation>
    </message>
    <message>
        <location filename="serialportio.h" line="225"/>
        <source>断开</source>
        <translation>Disconnect</translation>
    </message>
</context>
<context>
    <name>Widget420</name>
    <message>
        <location filename="widget420.ui" line="19"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="widget420.ui" line="55"/>
        <source>模拟量参数设置</source>
        <translation type="unfinished">Analog parameter setting</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="88"/>
        <source>设定上限距离值/cm</source>
        <translation type="unfinished">Set the upper limit distance value/cm</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="113"/>
        <source>请输入上限距离值cm</source>
        <translation type="unfinished">Please enter the upper limit distance value/cm</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="135"/>
        <source>下限值</source>
        <translation type="unfinished">Lower limit value</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="157"/>
        <source>上限值</source>
        <translation type="unfinished">Upper limit value</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="191"/>
        <source>请输入下限距离值cm</source>
        <translation type="unfinished">Please enter the lower  limit distance value/cm</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="204"/>
        <source>设定下限距离值/cm</source>
        <translation type="unfinished">Set the lower limit distance value/cm</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="217"/>
        <source>切换模式</source>
        <translation type="unfinished">Switch mode</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="243"/>
        <source>正向</source>
        <translation type="unfinished">positive</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="248"/>
        <source>反向</source>
        <translation type="unfinished">reverse</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="268"/>
        <source>模式切换</source>
        <translation type="unfinished">The mode switch</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="307"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="321"/>
        <source>9600</source>
        <translation type="unfinished">9600</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="326"/>
        <source>14400</source>
        <translation type="unfinished">14400</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="331"/>
        <source>19200</source>
        <translation type="unfinished">19200</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="336"/>
        <source>38400</source>
        <translation type="unfinished">38400</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="341"/>
        <source>56000</source>
        <translation type="unfinished">56000</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="346"/>
        <source>115200</source>
        <translation type="unfinished">115200</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="351"/>
        <source>128000</source>
        <translation type="unfinished">128000</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="356"/>
        <source>230400</source>
        <translation type="unfinished">230400</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="361"/>
        <source>256000</source>
        <translation type="unfinished">256000</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="366"/>
        <source>460800</source>
        <translation type="unfinished">460800</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="371"/>
        <source>512000</source>
        <translation type="unfinished">512000</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="376"/>
        <source>750000</source>
        <translation type="unfinished">750000</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="381"/>
        <source>921600</source>
        <translation type="unfinished">921600</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="395"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="414"/>
        <source>显示是否恢复</source>
        <translation type="unfinished">Show whether to restore</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="431"/>
        <location filename="widget420.ui" line="616"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="436"/>
        <source>2</source>
        <translation type="unfinished">2</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="441"/>
        <source>3</source>
        <translation type="unfinished">3</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="446"/>
        <source>4</source>
        <translation type="unfinished">4</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="451"/>
        <location filename="widget420.ui" line="621"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="456"/>
        <source>6</source>
        <translation type="unfinished">6</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="461"/>
        <source>7</source>
        <translation type="unfinished">7</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="466"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="471"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="476"/>
        <location filename="widget420.ui" line="626"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="490"/>
        <source>输出格式</source>
        <translation type="unfinished">Output Format</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="503"/>
        <source>配置已保存</source>
        <translation>Config Saved</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="519"/>
        <source>显示是否复位</source>
        <translation type="unfinished">Show if reset successful</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="536"/>
        <source>使能</source>
        <translation type="unfinished">Enable</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="541"/>
        <source>关闭</source>
        <translation type="unfinished">Disable</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="556"/>
        <source>9Byte(cm)</source>
        <translation type="unfinished">9Byte(cm)</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="561"/>
        <source>9Byte(mm)</source>
        <translation type="unfinished">9Byte(mm)</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="566"/>
        <source>字符串格式(m)</source>
        <translation type="unfinished">String Farmat</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="586"/>
        <source>设置波特率</source>
        <translation type="unfinished">Baudrate Set</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="599"/>
        <source>版本号：</source>
        <translation type="unfinished">Version:</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="631"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="636"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="641"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="646"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="660"/>
        <source>恢复出厂设置</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="673"/>
        <source>输出开关</source>
        <translation type="unfinished">Output Swtich</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="686"/>
        <source>系统复位</source>
        <translation type="unfinished">System Reset</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="699"/>
        <source>获取版本号</source>
        <translation type="unfinished">Get Version</translation>
    </message>
    <message>
        <location filename="widget420.ui" line="712"/>
        <source>低功耗设置</source>
        <translation>Consumption</translation>
    </message>
</context>
<context>
    <name>WidgetCAN</name>
    <message>
        <location filename="widgetcan.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="28"/>
        <source>显示是否复位</source>
        <translation>Show if reset successful</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="44"/>
        <source>获取版本号</source>
        <translation>Get Version</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="57"/>
        <source>配置已保存</source>
        <translation>Config Saved</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="73"/>
        <source>系统复位</source>
        <translation>System Reset</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="86"/>
        <source>恢复出厂设置</source>
        <translation>Factory Reset</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="99"/>
        <source>版本号：</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="115"/>
        <source>保存配置</source>
        <translation>Save Config</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="128"/>
        <source>出厂模式结果</source>
        <translation>Factory Reset Result</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="145"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="150"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="155"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="160"/>
        <location filename="widgetcan.ui" line="446"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="165"/>
        <location filename="widgetcan.ui" line="416"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="170"/>
        <location filename="widgetcan.ui" line="391"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="175"/>
        <location filename="widgetcan.ui" line="366"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="189"/>
        <source>设置帧率</source>
        <translation>FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="203"/>
        <source>使能</source>
        <translation>Enable</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="208"/>
        <source>关闭</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="222"/>
        <source>输出开关</source>
        <translation type="unfinished">Output Set</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="236"/>
        <source>9Byte(cm)</source>
        <translation type="unfinished">9Byte(cm)</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="241"/>
        <source>9Byte(mm)</source>
        <translation type="unfinished">9Byte(mm)</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="246"/>
        <source>字符串格式(m)</source>
        <translation>String Farmat</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="260"/>
        <source>输出模式</source>
        <translation>Output Format</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="279"/>
        <location filename="widgetcan.ui" line="312"/>
        <source>0-10000范围的正整数</source>
        <translation>0-10000 Interger</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="286"/>
        <source>设置CAN接收ID</source>
        <translation>Set CAN RxID</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="293"/>
        <source>设置CAN发送ID</source>
        <translation>Set CAN TxID</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="325"/>
        <source>设置CAN参数</source>
        <translation>Set CAN Param</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="339"/>
        <source>标准帧</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="344"/>
        <source>扩展帧</source>
        <translation>Extend</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="352"/>
        <source>设置CAN帧类型</source>
        <translation>Set CAN FrameType</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="371"/>
        <source>900</source>
        <translation type="unfinished">900</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="376"/>
        <source>800</source>
        <translation type="unfinished">800</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="381"/>
        <source>666</source>
        <translation type="unfinished">666</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="386"/>
        <source>600</source>
        <translation type="unfinished">600</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="396"/>
        <source>400</source>
        <translation type="unfinished">400</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="401"/>
        <source>300</source>
        <translation type="unfinished">300</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="406"/>
        <source>250</source>
        <translation type="unfinished">250</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="411"/>
        <source>225</source>
        <translation type="unfinished">225</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="421"/>
        <source>160</source>
        <translation type="unfinished">160</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="426"/>
        <source>150</source>
        <translation type="unfinished">150</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="431"/>
        <source>144</source>
        <translation type="unfinished">144</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="436"/>
        <source>125</source>
        <translation type="unfinished">125</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="441"/>
        <source>120</source>
        <translation type="unfinished">120</translation>
    </message>
    <message>
        <location filename="widgetcan.ui" line="454"/>
        <source>设置CAN波特率(Kbps)</source>
        <translation>Set CAN Baudrate</translation>
    </message>
</context>
<context>
    <name>WidgetIIC</name>
    <message>
        <location filename="widgetiic.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="37"/>
        <source>开始轮询</source>
        <translation type="unfinished">Start polling</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="273"/>
        <location filename="widgetiic.ui" line="381"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="278"/>
        <location filename="widgetiic.ui" line="401"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="283"/>
        <location filename="widgetiic.ui" line="426"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="288"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="293"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="298"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="303"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="195"/>
        <source>设置IIC地址</source>
        <translation type="unfinished">IIC Address Set</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="337"/>
        <source>9Byte(cm)</source>
        <translation type="unfinished">9Byte(cm)</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="342"/>
        <source>9Byte(mm)</source>
        <translation type="unfinished">9Byte(mm)</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="347"/>
        <source>字符串格式</source>
        <translation type="unfinished">String format</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="50"/>
        <source>停止轮询</source>
        <translation type="unfinished">Stop polling</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="63"/>
        <source>显示是否复位</source>
        <translation type="unfinished">Show if reset successful</translation>
    </message>
    <message>
        <source>使能</source>
        <translation type="obsolete">Enable</translation>
    </message>
    <message>
        <source>关闭</source>
        <translation type="obsolete">Disable</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="259"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="79"/>
        <source>恢复出厂设置</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="92"/>
        <source>配置已保存</source>
        <translation type="unfinished">Config Saved</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="214"/>
        <source>设置新IIC地址 0x08~0x77</source>
        <translation type="unfinished">Set a new IIC address 0x08~0x77</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="233"/>
        <source>IIC地址 0x08~0x77</source>
        <translation type="unfinished">IIC address 0x08~0x77</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="240"/>
        <source>IIC地址，默认0x10</source>
        <translation type="unfinished">IIC address, default 0x10</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="386"/>
        <source>2</source>
        <translation type="unfinished">2</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="391"/>
        <source>3</source>
        <translation type="unfinished">3</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="396"/>
        <source>4</source>
        <translation type="unfinished">4</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="406"/>
        <source>6</source>
        <translation type="unfinished">6</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="411"/>
        <source>7</source>
        <translation type="unfinished">7</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="416"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="421"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="367"/>
        <source>低功耗设置（Hz）</source>
        <translation type="unfinished">Consumption(Hz )</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="323"/>
        <source>输出格式</source>
        <translation type="unfinished">Output format</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="108"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="121"/>
        <source>显示是否恢复</source>
        <translation type="unfinished">Show whether to restore</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="137"/>
        <source>系统复位</source>
        <translation type="unfinished">System Reset</translation>
    </message>
    <message>
        <source>输出开关</source>
        <translation type="obsolete">Output Swtich</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="465"/>
        <source>显示测距结果</source>
        <translation type="unfinished">Display range results</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="446"/>
        <source>获取测距结果</source>
        <translation>Get Distance</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="149"/>
        <source>轮询周期ms</source>
        <translation type="unfinished">The polling cycle/ms</translation>
    </message>
    <message>
        <location filename="widgetiic.ui" line="174"/>
        <source>周期ms</source>
        <translation type="unfinished">cycle/ms</translation>
    </message>
</context>
<context>
    <name>WidgetModbus</name>
    <message>
        <location filename="widgetmodbus.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="177"/>
        <source>当前距离：</source>
        <translation type="unfinished">Current distance:</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="267"/>
        <location filename="widgetmodbus.ui" line="464"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="272"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="277"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="282"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="287"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="292"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="297"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="407"/>
        <location filename="widgetmodbus.ui" line="467"/>
        <source>1~247之间的整数</source>
        <translation type="unfinished">The value is an integer between 1 and 247</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="487"/>
        <source>启用</source>
        <translation type="unfinished">Enable</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="492"/>
        <source>关闭</source>
        <translation type="unfinished">Disable</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="388"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="170"/>
        <source>Modbus协议</source>
        <translation type="unfinished">Modbus protocol</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="426"/>
        <source>设置波特率</source>
        <translation type="unfinished">set baud rate</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="222"/>
        <source>获取当前信号强度</source>
        <translation type="unfinished">Get current signal strength</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="201"/>
        <source>开启轮询</source>
        <translation type="unfinished">Open the polling</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="362"/>
        <source>停止轮询</source>
        <translation type="unfinished">Stop polling</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="343"/>
        <source>信号强度：</source>
        <translation type="unfinished">Signal Strength:</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="369"/>
        <source>配置已保存！</source>
        <translation type="unfinished">Configuration saved!</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="317"/>
        <source>获取当前距离值</source>
        <translation type="unfinished">Get the current distance value</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="336"/>
        <source>设置Modbus地址</source>
        <translation type="unfinished">Set Modbus address</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="85"/>
        <source>9600</source>
        <translation type="unfinished">9600</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="46"/>
        <source>  当前Modbus站号</source>
        <translation type="unfinished">Current Modbus station number</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="65"/>
        <source>启用/关闭保存</source>
        <translation type="unfinished">Enable/disable saving</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="90"/>
        <source>14400</source>
        <translation type="unfinished">14400</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="95"/>
        <source>19200</source>
        <translation type="unfinished">19200</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="100"/>
        <source>38400</source>
        <translation type="unfinished">38400</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="105"/>
        <source>56000</source>
        <translation type="unfinished">56000</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="110"/>
        <source>57600</source>
        <translation type="unfinished">57600</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="115"/>
        <source>115200</source>
        <translation type="unfinished">115200</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="120"/>
        <source>12800</source>
        <translation type="unfinished">12800</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="125"/>
        <source>230400</source>
        <translation type="unfinished">230400</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="130"/>
        <source>256000</source>
        <translation type="unfinished">256000</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="135"/>
        <source>460800</source>
        <translation type="unfinished">460800</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="140"/>
        <source>512000</source>
        <translation type="unfinished">512000</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="145"/>
        <source>750000</source>
        <translation type="unfinished">750000</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="150"/>
        <source>921600</source>
        <translation type="unfinished">921600</translation>
    </message>
    <message>
        <source>距离：</source>
        <translation type="obsolete">Distance：</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="445"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetmodbus.ui" line="247"/>
        <source>输入循环周期ms</source>
        <translation type="unfinished">Enter the cycle ms</translation>
    </message>
</context>
<context>
    <name>WidgetRs485</name>
    <message>
        <location filename="widgetrs485.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="121"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="247"/>
        <location filename="widgetrs485.ui" line="305"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="252"/>
        <location filename="widgetrs485.ui" line="325"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="257"/>
        <location filename="widgetrs485.ui" line="350"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="262"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="267"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="272"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="277"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="42"/>
        <source>9600</source>
        <translation type="unfinished">9600</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="47"/>
        <source>14400</source>
        <translation type="unfinished">14400</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="52"/>
        <source>19200</source>
        <translation type="unfinished">19200</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="57"/>
        <source>38400</source>
        <translation type="unfinished">38400</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="62"/>
        <source>56000</source>
        <translation type="unfinished">56000</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="67"/>
        <source>57600</source>
        <translation type="unfinished">57600</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="72"/>
        <source>115200</source>
        <translation type="unfinished">115200</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="77"/>
        <source>128000</source>
        <translation type="unfinished">128000</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="82"/>
        <source>230400</source>
        <translation type="unfinished">230400</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="87"/>
        <source>256000</source>
        <translation type="unfinished">256000</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="92"/>
        <source>460800</source>
        <translation type="unfinished">460800</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="97"/>
        <source>512000</source>
        <translation type="unfinished">512000</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="102"/>
        <source>750000</source>
        <translation type="unfinished">750000</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="107"/>
        <source>921600</source>
        <translation type="unfinished">921600</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="291"/>
        <source>获取版本号</source>
        <translation type="unfinished">Get Version</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="364"/>
        <source>低功耗设置（Hz）</source>
        <translation type="unfinished">Low power mode/Hz</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="134"/>
        <source>配置已保存</source>
        <translation type="unfinished">Config Saved</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="163"/>
        <source>输出模式</source>
        <translation type="unfinished">Output Format</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="206"/>
        <source>9Byte(cm)</source>
        <translation type="unfinished">9Byte(cm)</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="211"/>
        <source>9Byte(mm)</source>
        <translation type="unfinished">9Byte(mm)</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="216"/>
        <source>字符串格式(m)</source>
        <translation type="unfinished">String Farmat</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="230"/>
        <source>显示是否复位</source>
        <translation type="unfinished">Show if reset successful</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="150"/>
        <source>设置波特率</source>
        <translation type="unfinished">Set baud rate </translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="383"/>
        <source>显示是否恢复</source>
        <translation type="unfinished">Show whether to restore</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="412"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="176"/>
        <source>版本号：</source>
        <translation type="unfinished">Version:</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="310"/>
        <source>2</source>
        <translation type="unfinished">2</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="315"/>
        <source>3</source>
        <translation type="unfinished">3</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="320"/>
        <source>4</source>
        <translation type="unfinished">4</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="330"/>
        <source>6</source>
        <translation type="unfinished">6</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="335"/>
        <source>7</source>
        <translation type="unfinished">7</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="340"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="345"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="426"/>
        <source>使能</source>
        <translation type="unfinished">Enable</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="431"/>
        <source>关闭</source>
        <translation type="unfinished">Disable</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="192"/>
        <source>系统复位</source>
        <translation type="unfinished">System Reset</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="399"/>
        <source>输出开关</source>
        <translation type="unfinished">Output Swtich</translation>
    </message>
    <message>
        <location filename="widgetrs485.ui" line="28"/>
        <source>恢复出厂设置</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
</context>
<context>
    <name>WidgetSteeringIIC</name>
    <message>
        <location filename="widgetsteeringiic.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="28"/>
        <location filename="widgetsteeringiic.ui" line="85"/>
        <source>显示是否复位</source>
        <translation type="unfinished">Show if reset successful</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="44"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="236"/>
        <source>请输入新的IIC地址</source>
        <translation type="unfinished">IIC address</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="274"/>
        <source>请输入当前IIC地址</source>
        <translation type="unfinished">Please enter the current IIC address</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="287"/>
        <source>IIC地址，留空为0x10</source>
        <translation type="unfinished">IIC address, leaving 0x10 blank</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="303"/>
        <source>设置雨刷周期</source>
        <translation type="unfinished">WiperCycle Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="57"/>
        <source>配置已保存</source>
        <translation type="unfinished">Config Saved</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="347"/>
        <source>分钟</source>
        <translation type="unfinished">minute</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="352"/>
        <source>小时</source>
        <translation type="unfinished">hour</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="416"/>
        <source>立即启动雨刷</source>
        <translation type="unfinished">Test Wipers</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="387"/>
        <source>设置往返次数</source>
        <translation type="unfinished">Round-trips Set</translation>
    </message>
    <message>
        <source>使能</source>
        <translation type="obsolete">Enable</translation>
    </message>
    <message>
        <source>关闭</source>
        <translation type="obsolete">Disable</translation>
    </message>
    <message>
        <source>输出开关</source>
        <translation type="obsolete">Output Swtich</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="400"/>
        <source>显示是否启动雨刷</source>
        <translation type="unfinished">Displays whether the wiper is activated</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="101"/>
        <source>系统复位</source>
        <translation type="unfinished">System Reset</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="255"/>
        <source>设置IIC地址</source>
        <translation type="unfinished">IIC Address Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="374"/>
        <source>1~10内正整数</source>
        <translation type="unfinished">The value is an integer ranging from 1 to 10</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="115"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="120"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="125"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="130"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="135"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="140"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="145"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="159"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="172"/>
        <source>恢复出厂设置</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="191"/>
        <source>轮询时间ms</source>
        <translation type="unfinished">Polling time ms</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="204"/>
        <source>开始轮询</source>
        <translation type="unfinished">Start polling</translation>
    </message>
    <message>
        <location filename="widgetsteeringiic.ui" line="217"/>
        <source>停止轮询</source>
        <translation type="unfinished">Stop polling</translation>
    </message>
</context>
<context>
    <name>WidgetSteeringModbus</name>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="28"/>
        <source>设置地址</source>
        <translation type="unfinished">Address Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="41"/>
        <source>保存配置</source>
        <translation>Save Config</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="60"/>
        <source>1~10内正整数</source>
        <translation type="unfinished">The value is an integer ranging from 1 to 10</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="73"/>
        <source>设置波特率</source>
        <translation>Baudrate Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="87"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="92"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="97"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="102"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="107"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="112"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="117"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="132"/>
        <source>使能</source>
        <translation type="unfinished">Enable</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="137"/>
        <source>关闭</source>
        <translation type="unfinished">Disable</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="151"/>
        <source>距离：</source>
        <translation>Distance：</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="167"/>
        <source>配置已保存</source>
        <translation type="unfinished">Config Saved</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="189"/>
        <source>Modbus协议</source>
        <translation type="unfinished">Modbus protocol</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="202"/>
        <source>获取距离值</source>
        <translation type="unfinished">Get Distance</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="215"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="234"/>
        <source>请输入Modbus地址</source>
        <translation type="unfinished">Please enter Modbus address</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="247"/>
        <source>获取信号强度</source>
        <translation type="unfinished">Get Strength:</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="313"/>
        <source>分钟</source>
        <translation type="unfinished">minute</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="318"/>
        <source>小时</source>
        <translation type="unfinished">hour</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="334"/>
        <source>设置雨刷周期</source>
        <translation type="unfinished">Wiper cycle Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="347"/>
        <source>信号强度：</source>
        <translation type="unfinished">Signal Strength:</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="363"/>
        <source>设置往返次数</source>
        <translation type="unfinished">Round-trips cnt</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="376"/>
        <source>显示是否启动</source>
        <translation type="unfinished">Show whether to start</translation>
    </message>
    <message>
        <location filename="widgetsteeringmodbus.ui" line="392"/>
        <source>立即启动雨刷</source>
        <translation type="unfinished">Wiper Test</translation>
    </message>
</context>
<context>
    <name>WidgetSteeringUart</name>
    <message>
        <location filename="widgetsteeringuart.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="28"/>
        <source>已复位</source>
        <translation type="unfinished">Has been reset</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="78"/>
        <source>分钟</source>
        <translation type="unfinished">minute</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="83"/>
        <source>小时</source>
        <translation type="unfinished">hour</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="99"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="112"/>
        <source>版本号：</source>
        <translation type="unfinished">Version:</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="129"/>
        <source>9600</source>
        <translation type="unfinished">9600</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="134"/>
        <source>14400</source>
        <translation type="unfinished">14400</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="139"/>
        <source>19200</source>
        <translation type="unfinished">19200</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="144"/>
        <source>38400</source>
        <translation type="unfinished">38400</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="149"/>
        <source>56000</source>
        <translation type="unfinished">56000</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="154"/>
        <source>57600</source>
        <translation type="unfinished">57600</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="159"/>
        <source>115200</source>
        <translation type="unfinished">115200</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="164"/>
        <source>128000</source>
        <translation type="unfinished">128000</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="169"/>
        <source>230400</source>
        <translation type="unfinished">230400</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="174"/>
        <source>256000</source>
        <translation type="unfinished">256000</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="179"/>
        <source>460800</source>
        <translation type="unfinished">460800</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="184"/>
        <source>512000</source>
        <translation type="unfinished">512000</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="189"/>
        <source>750000</source>
        <translation type="unfinished">750000</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="194"/>
        <source>921600</source>
        <translation type="unfinished">921600</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="208"/>
        <source>立即启动雨刷</source>
        <translation type="unfinished">Test Wiper</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="227"/>
        <source>显示是否恢复</source>
        <translation type="unfinished">Show whether to restore</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="243"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="256"/>
        <source>设置雨刷周期</source>
        <translation type="unfinished">Wiper Cycle</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="270"/>
        <source>9Byte(mm)</source>
        <translation type="unfinished">9Byte(mm)</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="275"/>
        <source>9Byte(cm)</source>
        <translation type="unfinished">9Byte(cm)</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="280"/>
        <source>字符串格式(m)</source>
        <translation type="unfinished">String Farmat</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="295"/>
        <source>使能</source>
        <translation type="unfinished">Enable</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="300"/>
        <source>关闭</source>
        <translation type="unfinished">Disable</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="315"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="320"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="325"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="330"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="335"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="340"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="345"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="359"/>
        <source>输出开关</source>
        <translation type="unfinished">Output Swtich</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="372"/>
        <source>获取版本号</source>
        <translation type="unfinished">Get Version</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="385"/>
        <source>输出模式</source>
        <translation type="unfinished">Output Format</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="398"/>
        <source>配置已保存</source>
        <translation type="unfinished">Config Saved</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="414"/>
        <source>显示是否启动</source>
        <translation type="unfinished">Show whether to start</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="436"/>
        <source>设置波特率</source>
        <translation type="unfinished">Set baud rate</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="449"/>
        <source>系统复位</source>
        <translation type="unfinished">System Reset</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="468"/>
        <source>1~10内的正整数</source>
        <translation type="unfinished">The value is a positive integer between 1 and 10</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="481"/>
        <source>恢复出厂设置</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="widgetsteeringuart.ui" line="494"/>
        <source>雨刷往返次数</source>
        <translation type="unfinished">Round trips</translation>
    </message>
</context>
<context>
    <name>WidgetUart</name>
    <message>
        <location filename="widgetuart.ui" line="19"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="55"/>
        <source>IO参数设置</source>
        <translation type="unfinished">IO Parameter Settings</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="88"/>
        <source>IO模式</source>
        <translation type="unfinished">IO mode</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="232"/>
        <source>滞回区</source>
        <translation type="unfinished">Hysteresis area</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="132"/>
        <source>延迟时间二</source>
        <translation type="unfinished">Delay2</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="110"/>
        <source>滞回区(cm)</source>
        <translation type="unfinished">Hysteresis area (cm)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="154"/>
        <source>请输入临界值cm</source>
        <translation type="unfinished">Please enter the critical value cm</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="180"/>
        <source>数据输出模式</source>
        <translation type="unfinished">Data</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="185"/>
        <source>开关量(近高远低)</source>
        <translation type="unfinished">Switching quantity (near high and far low)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="190"/>
        <source>开关量(近低远高)</source>
        <translation type="unfinished">Switching quantity (near high and far low)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="210"/>
        <source>延迟时间一</source>
        <translation type="unfinished">Delay1</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="254"/>
        <source>临界值(cm)</source>
        <oldsource>临界值</oldsource>
        <translation type="unfinished">The critical value (cm)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="276"/>
        <source>请输入滞回区cm</source>
        <translation type="unfinished">Zone Value(cm)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="301"/>
        <source>请输入延迟时间/ms</source>
        <translation type="unfinished">Please enter delay time /ms</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="320"/>
        <source>请输入延迟时间二/ms</source>
        <translation type="unfinished">Please enter delay time two /ms</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="339"/>
        <source>配置IO模式</source>
        <translation>Config Apply</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="375"/>
        <source>保存配置</source>
        <translation type="unfinished">Save Config</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="389"/>
        <source>9600</source>
        <translation type="unfinished">9600</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="394"/>
        <source>14400</source>
        <translation type="unfinished">14400</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="399"/>
        <source>19200</source>
        <translation type="unfinished">19200</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="404"/>
        <source>38400</source>
        <translation type="unfinished">38400</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="409"/>
        <source>56000</source>
        <translation type="unfinished">56000</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="414"/>
        <source>115200</source>
        <translation type="unfinished">115200</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="419"/>
        <source>128000</source>
        <translation type="unfinished">128000</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="424"/>
        <source>230400</source>
        <translation type="unfinished">230400</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="429"/>
        <source>256000</source>
        <translation type="unfinished">256000</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="434"/>
        <source>460800</source>
        <translation type="unfinished">460800</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="439"/>
        <source>512000</source>
        <translation type="unfinished">512000</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="444"/>
        <source>750000</source>
        <translation type="unfinished">750000</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="449"/>
        <source>921600</source>
        <translation type="unfinished">921600</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="463"/>
        <source>设置帧率</source>
        <translation type="unfinished">FrameFreq Set</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="482"/>
        <source>显示是否恢复</source>
        <translation type="unfinished">Show whether to restore</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="499"/>
        <location filename="widgetuart.ui" line="684"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="504"/>
        <source>2</source>
        <translation type="unfinished">2</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="509"/>
        <source>3</source>
        <translation type="unfinished">3</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="514"/>
        <source>4</source>
        <translation type="unfinished">4</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="519"/>
        <location filename="widgetuart.ui" line="689"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="524"/>
        <source>6</source>
        <translation type="unfinished">6</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="529"/>
        <source>7</source>
        <translation type="unfinished">7</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="534"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="539"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="544"/>
        <location filename="widgetuart.ui" line="694"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="558"/>
        <source>输出格式</source>
        <translation type="unfinished">Output format</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="571"/>
        <source>配置已保存</source>
        <translation type="unfinished">Config Saved</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="587"/>
        <source>显示是否复位</source>
        <translation type="unfinished">Show if reset successful</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="604"/>
        <source>使能</source>
        <translation type="unfinished">Enable</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="609"/>
        <source>关闭</source>
        <translation type="unfinished">Disable</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="624"/>
        <source>9Byte(cm)</source>
        <translation type="unfinished">9Byte(cm)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="629"/>
        <source>9Byte(mm)</source>
        <translation type="unfinished">9Byte(mm)</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="634"/>
        <source>字符串格式(m)</source>
        <translation type="unfinished">String Farmat</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="654"/>
        <source>设置波特率</source>
        <translation type="unfinished">Set baud rate</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="667"/>
        <source>版本号：</source>
        <translation type="unfinished">Version:</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="699"/>
        <source>100</source>
        <translation type="unfinished">100</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="704"/>
        <source>200</source>
        <translation type="unfinished">200</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="709"/>
        <source>500</source>
        <translation type="unfinished">500</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="714"/>
        <source>1000</source>
        <translation type="unfinished">1000</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="728"/>
        <source>恢复出厂设置</source>
        <translation type="unfinished">Factory Reset</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="741"/>
        <source>输出开关</source>
        <translation type="unfinished">Output Swtich</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="754"/>
        <source>系统复位</source>
        <translation type="unfinished">System Reset</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="767"/>
        <source>获取版本号</source>
        <translation type="unfinished">Get Version</translation>
    </message>
    <message>
        <location filename="widgetuart.ui" line="780"/>
        <source>低功耗设置/Hz</source>
        <translation type="unfinished">Low power mode/HZ</translation>
    </message>
</context>
</TS>

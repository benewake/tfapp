/********************************************************************************
** Form generated from reading UI file 'widgetrs485.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGETRS485_H
#define UI_WIDGETRS485_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidgetRs485
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QPushButton *btnFactoryReset;
    QComboBox *cbBaudrate;
    QPushButton *btnFrameFreq;
    QLabel *lbConfigResult;
    QPushButton *btnSetBaudrate;
    QPushButton *btnOutputFormat;
    QLabel *lbVersion;
    QPushButton *btnSystemReset;
    QComboBox *cbOutputFormat;
    QLabel *lbResetResult;
    QComboBox *cbFrameFreq;
    QPushButton *btnGetVersion;
    QComboBox *cbLowConsumption;
    QPushButton *btnLowConsumption;
    QLabel *lbFactoryResetResult;
    QPushButton *btnOutputSwitch;
    QPushButton *btnSaveConfig;
    QComboBox *cbOutputSwitch;

    void setupUi(QWidget *WidgetRs485)
    {
        if (WidgetRs485->objectName().isEmpty())
            WidgetRs485->setObjectName(QString::fromUtf8("WidgetRs485"));
        WidgetRs485->resize(440, 399);
        gridLayout = new QGridLayout(WidgetRs485);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        btnFactoryReset = new QPushButton(WidgetRs485);
        btnFactoryReset->setObjectName(QString::fromUtf8("btnFactoryReset"));
        btnFactoryReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFactoryReset, 5, 0, 1, 1);

        cbBaudrate = new QComboBox(WidgetRs485);
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->addItem(QString());
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbBaudrate, 0, 0, 1, 1);

        btnFrameFreq = new QPushButton(WidgetRs485);
        btnFrameFreq->setObjectName(QString::fromUtf8("btnFrameFreq"));
        btnFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnFrameFreq, 1, 1, 1, 1);

        lbConfigResult = new QLabel(WidgetRs485);
        lbConfigResult->setObjectName(QString::fromUtf8("lbConfigResult"));
        lbConfigResult->setMinimumSize(QSize(0, 30));
        lbConfigResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbConfigResult, 3, 2, 1, 1);

        btnSetBaudrate = new QPushButton(WidgetRs485);
        btnSetBaudrate->setObjectName(QString::fromUtf8("btnSetBaudrate"));
        btnSetBaudrate->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSetBaudrate, 0, 1, 1, 1);

        btnOutputFormat = new QPushButton(WidgetRs485);
        btnOutputFormat->setObjectName(QString::fromUtf8("btnOutputFormat"));
        btnOutputFormat->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnOutputFormat, 3, 1, 1, 1);

        lbVersion = new QLabel(WidgetRs485);
        lbVersion->setObjectName(QString::fromUtf8("lbVersion"));
        lbVersion->setMinimumSize(QSize(0, 30));
        lbVersion->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbVersion, 1, 2, 1, 1);

        btnSystemReset = new QPushButton(WidgetRs485);
        btnSystemReset->setObjectName(QString::fromUtf8("btnSystemReset"));
        btnSystemReset->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSystemReset, 4, 2, 1, 1);

        cbOutputFormat = new QComboBox(WidgetRs485);
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->addItem(QString());
        cbOutputFormat->setObjectName(QString::fromUtf8("cbOutputFormat"));
        cbOutputFormat->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbOutputFormat, 3, 0, 1, 1);

        lbResetResult = new QLabel(WidgetRs485);
        lbResetResult->setObjectName(QString::fromUtf8("lbResetResult"));
        lbResetResult->setMinimumSize(QSize(0, 30));
        lbResetResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbResetResult, 5, 2, 1, 1);

        cbFrameFreq = new QComboBox(WidgetRs485);
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->addItem(QString());
        cbFrameFreq->setObjectName(QString::fromUtf8("cbFrameFreq"));
        cbFrameFreq->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbFrameFreq, 1, 0, 1, 1);

        btnGetVersion = new QPushButton(WidgetRs485);
        btnGetVersion->setObjectName(QString::fromUtf8("btnGetVersion"));
        btnGetVersion->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnGetVersion, 0, 2, 1, 1);

        cbLowConsumption = new QComboBox(WidgetRs485);
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->addItem(QString());
        cbLowConsumption->setObjectName(QString::fromUtf8("cbLowConsumption"));
        cbLowConsumption->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbLowConsumption, 4, 0, 1, 1);

        btnLowConsumption = new QPushButton(WidgetRs485);
        btnLowConsumption->setObjectName(QString::fromUtf8("btnLowConsumption"));
        btnLowConsumption->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnLowConsumption, 4, 1, 1, 1);

        lbFactoryResetResult = new QLabel(WidgetRs485);
        lbFactoryResetResult->setObjectName(QString::fromUtf8("lbFactoryResetResult"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbFactoryResetResult->sizePolicy().hasHeightForWidth());
        lbFactoryResetResult->setSizePolicy(sizePolicy);
        lbFactoryResetResult->setMinimumSize(QSize(0, 30));
        lbFactoryResetResult->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(lbFactoryResetResult, 5, 1, 1, 1);

        btnOutputSwitch = new QPushButton(WidgetRs485);
        btnOutputSwitch->setObjectName(QString::fromUtf8("btnOutputSwitch"));
        btnOutputSwitch->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnOutputSwitch, 2, 1, 1, 1);

        btnSaveConfig = new QPushButton(WidgetRs485);
        btnSaveConfig->setObjectName(QString::fromUtf8("btnSaveConfig"));
        btnSaveConfig->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(btnSaveConfig, 2, 2, 1, 1);

        cbOutputSwitch = new QComboBox(WidgetRs485);
        cbOutputSwitch->addItem(QString());
        cbOutputSwitch->addItem(QString());
        cbOutputSwitch->setObjectName(QString::fromUtf8("cbOutputSwitch"));
        cbOutputSwitch->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(cbOutputSwitch, 2, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(WidgetRs485);

        QMetaObject::connectSlotsByName(WidgetRs485);
    } // setupUi

    void retranslateUi(QWidget *WidgetRs485)
    {
        WidgetRs485->setWindowTitle(QCoreApplication::translate("WidgetRs485", "Form", nullptr));
        btnFactoryReset->setText(QCoreApplication::translate("WidgetRs485", "\346\201\242\345\244\215\345\207\272\345\216\202\350\256\276\347\275\256", nullptr));
        cbBaudrate->setItemText(0, QCoreApplication::translate("WidgetRs485", "9600", nullptr));
        cbBaudrate->setItemText(1, QCoreApplication::translate("WidgetRs485", "14400", nullptr));
        cbBaudrate->setItemText(2, QCoreApplication::translate("WidgetRs485", "19200", nullptr));
        cbBaudrate->setItemText(3, QCoreApplication::translate("WidgetRs485", "38400", nullptr));
        cbBaudrate->setItemText(4, QCoreApplication::translate("WidgetRs485", "56000", nullptr));
        cbBaudrate->setItemText(5, QCoreApplication::translate("WidgetRs485", "57600", nullptr));
        cbBaudrate->setItemText(6, QCoreApplication::translate("WidgetRs485", "115200", nullptr));
        cbBaudrate->setItemText(7, QCoreApplication::translate("WidgetRs485", "128000", nullptr));
        cbBaudrate->setItemText(8, QCoreApplication::translate("WidgetRs485", "230400", nullptr));
        cbBaudrate->setItemText(9, QCoreApplication::translate("WidgetRs485", "256000", nullptr));
        cbBaudrate->setItemText(10, QCoreApplication::translate("WidgetRs485", "460800", nullptr));
        cbBaudrate->setItemText(11, QCoreApplication::translate("WidgetRs485", "512000", nullptr));
        cbBaudrate->setItemText(12, QCoreApplication::translate("WidgetRs485", "750000", nullptr));
        cbBaudrate->setItemText(13, QCoreApplication::translate("WidgetRs485", "921600", nullptr));

        btnFrameFreq->setText(QCoreApplication::translate("WidgetRs485", "\350\256\276\347\275\256\345\270\247\347\216\207", nullptr));
        lbConfigResult->setText(QCoreApplication::translate("WidgetRs485", "\351\205\215\347\275\256\345\267\262\344\277\235\345\255\230", nullptr));
        btnSetBaudrate->setText(QCoreApplication::translate("WidgetRs485", "\350\256\276\347\275\256\346\263\242\347\211\271\347\216\207", nullptr));
        btnOutputFormat->setText(QCoreApplication::translate("WidgetRs485", "\350\276\223\345\207\272\346\250\241\345\274\217", nullptr));
        lbVersion->setText(QCoreApplication::translate("WidgetRs485", "\347\211\210\346\234\254\345\217\267\357\274\232", nullptr));
        btnSystemReset->setText(QCoreApplication::translate("WidgetRs485", "\347\263\273\347\273\237\345\244\215\344\275\215", nullptr));
        cbOutputFormat->setItemText(0, QCoreApplication::translate("WidgetRs485", "9Byte(cm)", nullptr));
        cbOutputFormat->setItemText(1, QCoreApplication::translate("WidgetRs485", "9Byte(mm)", nullptr));
        cbOutputFormat->setItemText(2, QCoreApplication::translate("WidgetRs485", "\345\255\227\347\254\246\344\270\262\346\240\274\345\274\217(m)", nullptr));

        lbResetResult->setText(QCoreApplication::translate("WidgetRs485", "\346\230\276\347\244\272\346\230\257\345\220\246\345\244\215\344\275\215", nullptr));
        cbFrameFreq->setItemText(0, QCoreApplication::translate("WidgetRs485", "1", nullptr));
        cbFrameFreq->setItemText(1, QCoreApplication::translate("WidgetRs485", "5", nullptr));
        cbFrameFreq->setItemText(2, QCoreApplication::translate("WidgetRs485", "10", nullptr));
        cbFrameFreq->setItemText(3, QCoreApplication::translate("WidgetRs485", "100", nullptr));
        cbFrameFreq->setItemText(4, QCoreApplication::translate("WidgetRs485", "200", nullptr));
        cbFrameFreq->setItemText(5, QCoreApplication::translate("WidgetRs485", "500", nullptr));
        cbFrameFreq->setItemText(6, QCoreApplication::translate("WidgetRs485", "1000", nullptr));

        btnGetVersion->setText(QCoreApplication::translate("WidgetRs485", "\350\216\267\345\217\226\347\211\210\346\234\254\345\217\267", nullptr));
        cbLowConsumption->setItemText(0, QCoreApplication::translate("WidgetRs485", "1", nullptr));
        cbLowConsumption->setItemText(1, QCoreApplication::translate("WidgetRs485", "2", nullptr));
        cbLowConsumption->setItemText(2, QCoreApplication::translate("WidgetRs485", "3", nullptr));
        cbLowConsumption->setItemText(3, QCoreApplication::translate("WidgetRs485", "4", nullptr));
        cbLowConsumption->setItemText(4, QCoreApplication::translate("WidgetRs485", "5", nullptr));
        cbLowConsumption->setItemText(5, QCoreApplication::translate("WidgetRs485", "6", nullptr));
        cbLowConsumption->setItemText(6, QCoreApplication::translate("WidgetRs485", "7", nullptr));
        cbLowConsumption->setItemText(7, QCoreApplication::translate("WidgetRs485", "8", nullptr));
        cbLowConsumption->setItemText(8, QCoreApplication::translate("WidgetRs485", "9", nullptr));
        cbLowConsumption->setItemText(9, QCoreApplication::translate("WidgetRs485", "10", nullptr));

        btnLowConsumption->setText(QCoreApplication::translate("WidgetRs485", "\344\275\216\345\212\237\350\200\227\350\256\276\347\275\256\357\274\210Hz\357\274\211", nullptr));
        lbFactoryResetResult->setText(QCoreApplication::translate("WidgetRs485", "\346\230\276\347\244\272\346\230\257\345\220\246\346\201\242\345\244\215", nullptr));
        btnOutputSwitch->setText(QCoreApplication::translate("WidgetRs485", "\350\276\223\345\207\272\345\274\200\345\205\263", nullptr));
        btnSaveConfig->setText(QCoreApplication::translate("WidgetRs485", "\344\277\235\345\255\230\351\205\215\347\275\256", nullptr));
        cbOutputSwitch->setItemText(0, QCoreApplication::translate("WidgetRs485", "\344\275\277\350\203\275", nullptr));
        cbOutputSwitch->setItemText(1, QCoreApplication::translate("WidgetRs485", "\345\205\263\351\227\255", nullptr));

    } // retranslateUi

};

namespace Ui {
    class WidgetRs485: public Ui_WidgetRs485 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGETRS485_H

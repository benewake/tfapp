#ifndef WIDGETMODBUS_H
#define WIDGETMODBUS_H

#include "serialportio.h"

#include <QWidget>



namespace Ui {
class WidgetModbus;
}

class WidgetModbus : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetModbus(QWidget *parent = nullptr);
    ~WidgetModbus();


    void SetUartRecerver(serialportio *tReceiver, QString productModal);
private slots:
    void on_btnModbusContract_clicked();

    void on_btnModbusAddress_clicked();

    void on_btnBaudrate_clicked();



    void on_btnCurrentDistance_clicked();

    void on_btnCurSignal_clicked();

    void on_btnSaveConfig_clicked();

    void on_btnFrameFreq_clicked();

    void on_btnStart_clicked();

    void on_btnStop_clicked();

    void slotModbusQuery();

    void on_btnMbContractSave_clicked();

private:
    Ui::WidgetModbus *ui;

    serialportio *sensorRecver_;

    QTimer* timerModbusQuery;

    QString mProductModal;

    bool CheckComState();
    bool CheckComStateNoMessage();
};

#endif // WIDGETUART_H

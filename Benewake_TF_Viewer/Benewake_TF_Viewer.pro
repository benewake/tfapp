#-------------------------------------------------
#
# Project created by QtCreator 2022-02-17T10:37:50
#
#-------------------------------------------------

QT       += core gui
QT       += serialport
QT       += network

QT       += charts

RC_ICONS = benewake-LOGO.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Benewake_TF_Viewer
TEMPLATE = app


msvc {
    QMAKE_CFLAGS += /utf-8
    QMAKE_CXXFLAGS += /utf-8
}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        aboutdialog.cpp \
        appinit.cpp \
        canbusiothread.cpp \
        failconndialog.cpp \
        formcontent.cpp \
        main.cpp \
        maindialog.cpp \
        productselectdialog.cpp \
        quiwidget.cpp \
        serialportio.cpp \
        widget420.cpp \
        widgetcan.cpp \
        widgetiic.cpp \
        widgetmodbus.cpp \
        widgetrs485.cpp \
        widgetsteeringiic.cpp \
        widgetsteeringmodbus.cpp \
        widgetsteeringuart.cpp \
        widgetuart.cpp

HEADERS += \
        aboutdialog.h \
        appinit.h \
        canbusiothread.h \
        failconndialog.h \
        formcontent.h \
        head.h \
        maindialog.h \
        productselectdialog.h \
        quiwidget.h \
        serialportio.h \
        widget420.h \
        widgetcan.h \
        widgetiic.h \
        widgetmodbus.h \
        widgetrs485.h \
        widgetsteeringiic.h \
        widgetsteeringmodbus.h \
        widgetsteeringuart.h \
        widgetuart.h\
        ControlCAN.h

FORMS += \
        aboutdialog.ui \
        failconndialog.ui \
        formcontent.ui \
        maindialog.ui \
        productselectdialog.ui \
        widget420.ui \
        widgetcan.ui \
        widgetiic.ui \
        widgetmodbus.ui \
        widgetrs485.ui \
        widgetsteeringiic.ui \
        widgetsteeringmodbus.ui \
        widgetsteeringuart.ui \
        widgetuart.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

include(log4qt/log4qt.pri)
INCLUDEPATH += log4qt

TRANSLATIONS = tfviewer_cn.ts\
               tfviewer_en.ts

win32: LIBS += -L$$PWD/./ -lControlCAN

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

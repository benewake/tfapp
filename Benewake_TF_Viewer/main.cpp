#include "maindialog.h"
#include <QApplication>
#include <QFile>
#include <QStringList>
#include <QFontDatabase>
#include <QFont>

#include "appinit.h"
#include "logmanager.h"
#include "propertyconfigurator.h"

#include <QSettings>



int main(int argc, char *argv[])
{
	

#if (QT_VERSION >= QT_VERSION_CHECK(5,6,0))
		QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
		QApplication a(argc, argv);

		Log4Qt::PropertyConfigurator::configure("QtLog4Qt.conf");
		Log4Qt::LogManager::setHandleQtMessages(true);


		AppInit::Instance()->start();

		QFile qss(":Aqua.qss");
		if (qss.open(QFile::ReadOnly))
		{
			qDebug("Qss Open success");
			QString style = QLatin1String(qss.readAll());
			a.setStyleSheet(style);
			qss.close();

		}
		else
		{
			qDebug("Open failed");
		}


		//读配置文件，当前界面是中/英

		QSettings *settings = new QSettings("config.ini", QSettings::IniFormat);

		QString LANG = settings->value("UI/LANG", "").toString();

		QString LangSet = "Cur UI LANG Is =>" + LANG;

		qDebug() << LangSet;

		QTranslator translator;
		bool b = false;
		if (LANG == "CN" || LANG == "")
		{
			b = translator.load("tfviewer_cn.qm");
			settings->setValue("UI/LANG", "CN");
			settings->setValue("UI/AAA", "11");

		}
		else  if (LANG == "EN") {
			b = translator.load("tfviewer_en.qm");

			settings->setValue("UI/LANG", "EN");
		}

		delete settings;

		qApp->installTranslator(&translator);



		QFont font("Arial", 9);
		qApp->setFont(font);

		//    int fontId = QFontDatabase::addApplicationFont(":/font/SourceHanSans.ttc");
		//    QStringList fontIDs = QFontDatabase::applicationFontFamilies(fontId);
		//    if (! fontIDs.isEmpty()) {
		//        QFont font(fontIDs.first());
		//        QApplication::setFont(font);
		//    }
		//    else {
		//        qDebug()<<"Failed to load font.";
		//    }


		MainDialog dialog;
		dialog.show();

		return a.exec();
	

}

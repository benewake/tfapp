#ifndef SERIALPORTIO_H
#define SERIALPORTIO_H

#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QtSerialPort>
#include <QMetaType>
#include <QByteArray>
#include <QVector>
#include <QDebug>
#include <QTimer>
#include <QTime>


#define MAXDISTNUM  100

// modbus异常响应的最长长度
#define LENGTH_MAX_REP_MB               (8)
#define LENGTH_REQ                      (8)
#define LENGTH_CRC                      (2)
#define CRC16_TABLE_LENGTH              (256)
/*
 * CAN帧类型
 */
typedef enum{
    CAN_FRAME_STD           = 0x0,
    CAN_FRAME_EXD           = 0x1,
}E_CAN_FRAME_Type;

/*
 * 使能类型
 */
typedef enum{
    TYPE_ENABLE             = 0x00,		// 使能
    TYPE_DISABLE            = 0x01,		// 不使能
}E_ENABLE_Type;

/*
 * 雷达多回波/ 单回波
 */
typedef enum{
    MULTI_SIGNAL            = 0x00,		// 多回波
    SINGLE_SIGNAL           = 0x01,		// 单回波
}E_SIGNAL_Type;


/*
 * 增益自动调节使能控制
 */
typedef enum{
    GAIN_ADJUST_ENABLE      = 0x00,		// 使能
    GAIN_ADJUST_DISABLE     = 0x01,		// 不使能
}E_GAIN_AJUST_Type;

/*
 * flash保护是否使能
 */
typedef enum{
    FLASH_PROTECT_DISABLE	= 0x00,		// 不使能
    FLASH_PROTECT_ENABLE 	= 0x01,		// 使能
}E_FLASH_PROTECT_Type;

/*
 * 工作模式定义
 */
typedef enum{
    MODE_Continuous         = 0x01,		// 连续工作模式
    MODE_Trigger            = 0x02,		// 指令单次触发模式
}E_WorkMode_Type;

/*
 * 协议类型定义
 */
typedef enum{
    PROTOCOL_Product        = 0x01,		// 量产协议- 9byte
    PROTOCOL_Develop        = 0x02,		// 研发版本协议 - 22 byte
}E_Protocol_Type;

/*
 * 传输类型定义
 */
typedef enum{
    TRANS_UART              = 0x01,		// 串口通信
    TRANS_CAN               = 0x02,		// CAN口通信
}E_Trans_Type;

/*
 * 雷达输出是否使能
 */
typedef enum{
    OUTPUT_Disable          = 0x00,		// 雷达不输出数据
    OUTPUT_Enable           = 0x01,		// 指令输出数据
}E_Output_Enable_Type;

/*
 * 是否检验校验和
 */
typedef enum{
    CHECKSUM_Invalid        = 0x00,		// 不检测校验和
    CHECKSUM_Valid          = 0x01,		// 检测校验和
}E_CheckSum_Type;

/*
 * 客户
 */
typedef enum{
    Custom_NULL             = 0x00,		// 通用版本
    Custom_BL               = 0x01,		// BL客户定制版本
    Custom_I13              = 0x02,		// I13客户定制版本
    Custom_QL               = 0x03,		// QL客户定制版本
    Custom_ZC               = 0x04,		// 中产客户定制版本
    Custom_Yifei            = 0x05,		// 一飞客户定制版本
    Custom_DaXie            = 0x06,		// 大榭客户定制版本
    Custom_ZhenRui          = 0x07,		// 臻叡客户定制版本
    Custom_HANGTIAN         = 0x08,		// 航天所客户定制版本
    Custom_A_TECH           = 0x09,		// A tech客户定制版本
    Custom_DPL              = 0x0a,		// 多普勒客户定制版本
}E_Custom_Type;

/*
 * 数据输出格式
 */
typedef enum{
    FORMAT_ASCII            = 0x00,		// ASCII码输出
    FORMAT_9BYTECM          = 0x01, 	// 9字节二进制cm输出
    FORMAT_PIX              = 0x02, 	// PIX输出M
    FORMAT_DEBUG3           = 0x03, 	// Debug 格式 3
    FORMAT_DEBUG            = 0x04, 	// Debug 格式
    FORMAT_DEBUG2           = 0x05, 	// Debug 格式 2
    FORMAT_9BYTEMM          = 0x06, 	// 9 字节二进制 mm 输出
    FORMAT_32BITTIMESTAMP   = 0x07, 	// 带 32bit 时间戳
    FORMAT_ID0              = 0x08,     // ID0输出
    FORMAT_8BYTECM          = 0x09,     // 8 字节二进制 cm 输出
    FORMAT_DEBUG4           = 0x0A      // Debug 格式 4
}E_OutputFormat_Type;

/*
 * 检测方式
 */
typedef enum{
    TYPE_PULSEWIDTH         = 0x00,		// 脉宽
    TYPE_LEADINGEDGE        = 0x01,		// 前沿检测
}E_DETECT_Type;

/*
 * 量程范围
 */
typedef enum{
    TYPE_RANGE_100M         = 0x00,     // 100m量程
    TYPE_RANGE_180M         = 0x01,     // 180m量程
}E_RANGE_Type;

/*
 * dist和DAC code的映射关系
 */
typedef enum{
    TYPE_FORWARD            =  0x00,         // 同向映射，dist越大，DAC code越大
    TYPE_BACKWARD           =  0x01,         // 逆向映射，dist越大，DAC code越小
}E_MAP_Type;

/*
 * 读写操作
 */
enum
{
    op_read                 = 0x00,
    op_write                = 0x01,
};

/**
  * @brief  GPIO Bit SET and Bit RESET enumeration
  */
typedef enum
{
  GPIO_PIN_RESET = 0,
  GPIO_PIN_SET
}GPIO_PinState;

#define Str_FPS_1                   ("1")
#define Str_FPS_5                   ("5")
#define Str_FPS_10                  ("10")
#define Str_FPS_100                 ("100")
#define Str_FPS_200                 ("200")
#define Str_FPS_500                 ("500")
#define Str_FPS_1000                ("1000")
#define Str_FPS_5000                ("5000")
#define Str_FPS_10000               ("10000")

#define DATA_FPS_1                  (1)
#define DATA_FPS_5                  (5)
#define DATA_FPS_10                 (10)
#define DATA_FPS_100                (100)
#define DATA_FPS_200                (200)
#define DATA_FPS_500                (500)
#define DATA_FPS_1000               (1000)
#define DATA_FPS_5000               (5000)
#define DATA_FPS_10000              (10000)

#define Str_BAUD_9600               ("9600")
#define Str_BAUD_115200             ("115200")
#define Str_BAUD_256000             ("256000")
#define Str_BAUD_460800             ("460800")
#define Str_BAUD_921600             ("921600")
#define Str_BAUD_1000000            ("1000000")
#define Str_BAUD_2000000            ("2000000")
#define Str_BAUD_500K               ("500k")
#define Str_BAUD_250K               ("250k")
#define Str_BAUD_125K               ("125k")

#define Data_BAUD_9600              (9600)
#define Data_BAUD_115200            (115200)
#define Data_BAUD_256000            (256000)
#define Data_BAUD_460800            (460800)
#define Data_BAUD_921600            (921600)
#define Data_BAUD_1000000           (1000000)
#define Data_BAUD_2000000           (2000000)
#define Data_BAUD_500K              (500000)
#define Data_BAUD_250K              (250000)
#define Data_BAUD_125K              (125000)

/***
 * 中英文UI
 */
#define Str_CH_MODE_Continuous      (QObject::tr("连续模式"))
#define Str_CH_MODE_Trigger         (QObject::tr("触发模式"))

#define Str_CH_DISCONNECT           (QObject::tr("断开"))

#define ID_GET_DIST                0x00	// 获取测距结果
#define ID_GET_VERSION             0x01	// 获取版本号
#define ID_SOFT_RESET              0x02	// 系统软件复位
#define ID_SAMPLE_FREQ             0x03	// 设置工作频率
#define ID_SAMPLE_TRIG             0x04	// 单次触发指令
#define ID_OUTPUT_FORMAT           0x05	// 设置输出格式
#define ID_BAUD_RATE               0x06	// 设置串口波特率
#define ID_OUTPUT_EN               0x07	// 输出开关
#define ID_FRAME_CHECKSUM_EN       0x08	// 校验和开关
#define ID_RESTORE_DEFAULT         0x10	// 恢复出厂设置
#define ID_SAVE_SETTINGS           0x11	// 将当前设置写入FLASH
#define ID_GET_VERSION_FULL        0x14	// 读完整版本号
#define ID_LOW_CONSUMPTION         0x35 // 低功耗设置
#define ID_VOL_APD_CONFIG          0x40	// 配置apd偏压
#define ID_VDBS_BIAS_CONFIG        0x41	// 配置Vdbs_bias
#define ID_TABLE_CORR_A_CONFIG     0x42	// 配置修正列表系数table_corr_a
#define ID_TABLE_CORR_B_CONFIG     0x43	// 配置修正列表系数table_corr_b
#define ID_PROTOCOL_CONFIG         0x44	// 配置通信协议类型，22byte或者9byte
#define ID_TRANS_TYPE_CONFIG       0x45	// 配置传输方式，串口或者can口
#define ID_ARGU_BREAK_CONFIG       0x46	// 配置校准参数breaks
#define ID_ARGU_COEFS_CONFIG       0x47	// 配置校准参数coefs
#define ID_FLASH_R_PROTECT         0x48	// 配置flash读保护
#define ID_UPGRADE                 0x49	// 版本升级
#define ID_UPGRADE_FINAL           0x4A	// 预留
#define ID_VDBS_ADJUST             0x4B	// VDBS自动调节
#define ID_VOL_APD_ADAPT_ENABLE    0x4C	// APD调节使能
#define ID_APD_CLOSELOOP_CTRL      0x4D // APD闭环控制
#define ID_GAIN_ADJUST_ENABLE      0x4E	// 增益自动调节控制开关
#define ID_TDC_OUTRANGE_VALUE      0x4F	// 超量程TDC测试值
#define ID_CAN_SND_ID              0x50	// CAN发送ID
#define ID_CAN_RCV_ID              0x51	// CAN发送ID
#define ID_CAN_BAUDRATE            0x52	// CAN波特率
#define ID_SAVE_FACTORY            0x53	// 保存为出厂设置
#define ID_GET_KEY_STRING          0x54	// 获取字符串
#define ID_GET_KEY_CHAR            0x55	// 获取字符
#define ID_SEQUENCE_NUM            0x56	// 设置序列号
#define ID_SIGNAL_MODE_CONFIG      0x57	// 多回波使能控制
#define ID_B_L1_WR                 0x58	// B客户初始值L1读写
#define ID_B_L_WR                  0x59	// B客户变化量阈值L读写
#define ID_CUSTOM_CHOSE            0x5A	// 选择客户
#define ID_HORI_ANGLE_CONFIG       0x5B // 水平安装角度
#define ID_VDBS_ADJUST_ENABLE      0x5C // vdbs自动调节使能
#define ID_CAN_FRAME_TYPE          0x5D // CAN帧类型
#define ID_APD_TEMP_THRESHOLD      0x60 // APD温度调节阈值
#define ID_IO_LEVEL_NEAR           0x61	// IO近距离电平值
#define ID_IO_OUTPUT_DELAY         0x62	// IO输出延时
#define ID_IO_DIST_THRESHOLD       0x63	// IO变化的dist阈值及缓冲距离
#define ID_FOG_FEATURE_ENABLE      0x64	// 雨雾算法使能
#define ID_APD_READ_ENABLE         0x65	// apd电压读取
#define ID_DETECT_TYPE             0x68	// 检测方式
#define ID_OFFSET_CALI_CONF        0x69	// 偏移校准
#define ID_DEBUG_MODE_ENABLE       0x6a	// 测试模式使能
#define ID_RD_ERR_CONFIG           0x6b	// rawdist误差配置
#define ID_RANGE_TYPE_CONFIG       0x6E // 量程类型
#define ID_MODBUS_CTRL             0x6F // modbus协议使能控制
#define ID_MODBUS_ADDR_CONF        0x70 // modbus地址配置
#define ID_DAC_MAP_TYPE            0x71 // dac映射方式
#define ID_DAC_MIN_CONFIG          0x72 // dac min配置
#define ID_DAC_MAX_CONFIG          0x73 // dac max配置
#define ID_ELEC_OUTPUT_DAC         0x74 // dac 输出测试

#define LENGTH_SEQUENCE_NUM        (14) // 序列号长度
#define RES_LEN_UNIVERSAL          (5)
#define RES_LEN_SEQ_NUM_R          (RES_LEN_UNIVERSAL + LENGTH_SEQUENCE_NUM)
#define RES_LEN_B_L_L1_R           (7)
#define RES_LEN_ID_FULL_V          (30)

#define UPGRADE_FRAME_LENGTH_SERIAL     (255)
#define UPGRADE_FRAME_LENGTH_CAN        (8)
#define CTRL_DATA_LENGTH                (6)	// 一个升级帧数据里控制数据的长度。包括head(1byte) + length(1byte) + id(1byte) + frame_index(2byte) + check_sum(1byte)
//#define EFFIECT_DATA_LENGTH           (UPGRADE_FRAME_LENGTH - CTRL_DATA_LENGTH)   // 升级帧有效数据长度

#define LENGTH_OF_TABLE_ARG		(16)
#define LENGTH_OF_BREAKS		(8)
#define ROW_NUM_OF_COEFS		(4)
#define COL_NUM_OF_COEFS		(7)


typedef QVector<QString> StringVector;


class serialportio : public QObject
{
    Q_OBJECT
public:
    explicit serialportio(QObject *parent = nullptr);

    ~serialportio();


    QSerialPort *serialPort_;

	//0： 非字符串格式接收 1：字符串格式接收
	int RxFrameType;

    QTimer *timer_;

    int iicFreq_;

    bool open(const QString _serialSelected, int _baudRate);

    void run();

    bool close();

    bool isOpen();

    // 雷达指令接口
    bool send_cmd_id_soft_reset();

    bool send_cmd_id_sample_trig(unsigned short &dist);

    bool send_cmd_id_output_format(E_OutputFormat_Type format);

    bool send_cmd_id_baud_rate(unsigned int baudrate);

    bool send_cmd_id_baud_rate_pro(unsigned int baudrate);

    bool send_cmd_id_output_en(E_Output_Enable_Type is_enabled);

    bool send_cmd_id_frame_checksum_en(E_CheckSum_Type is_enabled);

    bool send_cmd_id_restore_default();

    bool send_cmd_id_save_settings();

    bool send_cmd_id_trans_type_config(E_Trans_Type trans);

    bool send_cmd_id_upgrade(unsigned int upgrade_frame_length, uint16_t index, QByteArray data);

    bool send_cmd_id_gain_adjust_enable(E_GAIN_AJUST_Type enable);

    bool send_cmd_id_can_snd_id(uint32_t value);

    bool send_cmd_id_can_rcv_id(uint32_t value);

    bool send_cmd_id_can_baudrate(uint32_t value);

    QString send_cmd_id_version(bool &result);

    QString send_cmd_id_full_version(bool &result);

    bool send_cmd_id_frame_rate(uint16_t fps);



	bool send_modbus_startwiper(uint addr);

	bool send_cmd_id_Custom_choise(E_Custom_Type custom);

    bool send_cmd_id_can_frame_type_config(E_CAN_FRAME_Type type);

    bool send_cmd_id_dist_io_threshold(uint16_t value1, uint16_t value2);

    bool send_cmd_id_near_io_value(GPIO_PinState value);

    bool send_cmd_id_io_delay(uint16_t value1, uint16_t value2);

    bool send_cmd_id_fog_feature_enable(E_ENABLE_Type enable);

    bool send_cmd_id_dac_map_type(E_MAP_Type type);

    bool send_cmd_id_offset_config(uint16_t value);

    bool send_cmd_id_modbus_enable();

    bool send_cmd_id_modbus_dev_addr_set(unsigned char modbus_dev_addr);

    bool send_cmd_id_mb_baudrate_config(unsigned char dev_addr, unsigned int baudrate);

    bool send_cmd_id_mb_dev_addr_config(unsigned char dev_addr, unsigned int mb_dev_addr);

    bool send_cmd_id_mb_get_dist(unsigned char dev_addr, unsigned short &dist);

    bool send_cmd_id_mb_modbus_disable(unsigned char dev_addr);

    bool send_cmd_id_mb_save_config(unsigned char dev_addr);

    bool send_cmd_id_modbus_set_fps(unsigned char dev_addr, int fps);

    bool send_cmd_id_modbus_get_version(unsigned char dev_addr, QString &ver);

    bool send_cmd_id_modbus_shutdown(unsigned char dev_addr);

    bool send_cmd_id_modbus_reboot(unsigned char dev_addr);

    void upgrade(const QString fullpath, unsigned int upgrade_frame_length);

    bool send_cmd_low_consumption(uint sampleRate);

    bool send_cmd_id_modbus_getsignal(unsigned char dev_addr,unsigned short& signal);

    bool send_cmd_id_getdistancemm(unsigned short& distance);

    bool send_iic_querydistancecm(uint iicAddress);

    bool send_cmd_id_setiicaddr(uint curAddr,uint iicNewAddr);

    QString send_cmd_id_version_pro(bool &result);

    bool send_cmd_id_frame_rate_pro(uint16_t fps);

    int32_t max_dist_num;

    bool send_cmd_id_output_en_pro(E_Output_Enable_Type is_enabled);


    bool send_cmd_id_output_format_pro(E_OutputFormat_Type format);
    bool send_cmd_id_soft_reset_pro();
    bool send_cmd_id_save_settings_pro();


    bool send_cmd_id_restore_default_pro();

    bool send_cmd_id_uart_setio(uint ioMode, uint distVal, uint zoneVal, uint delayVal1, uint delayVal2);

    bool send_cmd_id_set_wiper_peroid(uint16_t periodMinutes);

	bool send_mobus_set_wiper_peroid(uint addr, uint periodMinutes);


    bool send_cmd_set_wipercycle(uint16_t cycleCnt);
	bool send_modbus_set_wipercycle(uint addr, uint cycleCnt);

    bool send_cmd_startwiper();

    bool send_cmd_id_getdistancecm_pro(unsigned short &distance);
    bool send_cmd_id_can_param_tfmini(int frameType, int baudRate, long canTxId, long canRxId);


    bool send_uart_oneframe(QByteArray cmd);

    bool send_mb_distandsignal(uint devAddr);


    bool send_analog_modeswitch(uint mode);

    bool send_analog_setdistmin(uint mindist);

    bool send_analog_setdistmax(uint maxdist);


    bool send_iic_getdistancecm(uint iicAddress, unsigned short &distance);
    bool send_iic_frame_rate(uint iicAddress, uint16_t fps);
    bool send_iic_output_format(uint iicAddress, E_OutputFormat_Type format);
    bool send_iic_low_consumption(uint iicAddress, uint sampleRate);
    bool send_iic_save_settings(uint iicAddress);

    bool send_iic_soft_reset(uint iicAddress);
    bool send_iic_restore_default(uint iicAddress);
    bool cmd_iic_pro_enter_settingmode(uint iicaddress);
    bool cmd_iic_pro_exit_settingmode(uint iicaddress);



    bool send_iic_save_settings_pro(uint iicAddr);
    bool send_iic_frame_rate_pro(uint iicAddr, uint16_t fps);
    bool send_iic_output_format_pro(uint iicAddress, E_OutputFormat_Type format);
    bool send_iic_getdistancecm_pro(uint iicAddr, unsigned short &distance);
    bool send_iic_querydistance_pro(uint iicAddress);


    bool send_iic_setaddr_pro(uint iicaddr, uint iicNewAddr);


    bool send_iic_setaddr(uint iicAddr, uint iicNewaddr);
private:

    bool read_ack(QByteArray ack);

    unsigned char calc_checksum(QByteArray &cmd);

    bool send_do_cmd_id_upgrade(unsigned int upgrade_frame_length, unsigned short index, QByteArray data, unsigned short frame_cnt);

    uint16_t crc16_calc(QByteArray data, uint32_t length);

private:


    QByteArray dataBuffer_;

    QByteArray dataHeader_;

    QVector<uint16_t> passDist_;
    QVector<uint16_t> passAmp_;
    QVector<QString> passRxFrame_;


    int numFrame_;

    int freq_;



    QTime periodTest_;

    QTime emitPeriod_;

    bool cmd_uart_pro_enter_settingmode();


    bool cmd_uart_pro_exit_settingmode();

signals:

    void signalRun();

    void signalBaudrateChanged();

    void signalSerialCmdSend(QByteArray cmd);

    void signalOpen(QString _serialSelected, int _baudRate, bool &_result);

    void signalDistance(QVector<uint16_t> _dist, QVector<uint16_t> _amp, const StringVector& _hexStrArr,int _freq);

    void signalDataRcv(QByteArray cmdRx);

    void signalDataContractRcv(QByteArray cmdRx);

    void signalClose(bool &_result);

     void  signalReset(bool &_result);

     void signalResetPro(bool &_result);

     void  signalIICReset(uint iicAddress, bool &_result);

     void  signalIICResetPro(bool &_result);

    void signalSampleTrig(unsigned short &_dist, bool &_result);

    void signalOutputFormat(unsigned char _format, bool &_result);

    void signalOutputFormatPro(unsigned char _format, bool &_result);

    void signalIICOutputFormat(uint iicAddress, unsigned char _format, bool &_result);

    void signalIICOutputFormatPro(uint iicAddress, unsigned char _format, bool &_result);


    void signalBaudrate(unsigned int _baudrate, bool &_result);

    void signalBaudratePro(unsigned int _baudrate, bool &_result);

    void signalOutputEnabled(unsigned char _enabled, bool &_result);

    void signalOutputEnabledPro(unsigned char _enabled, bool &_result);

    void signalChecksumEnabled(unsigned char _enabled, bool &_result);

    void signalRestoreFactory(bool &_result);

    void signalRestoreFactoryPro(bool &_result);


    void signalIICRestoreFactory(uint iicAddress,bool &_result);

    void signalIICRestoreFactoryPro(uint iicAddress,bool &_result);


    void signalSaveSetting(bool &_result);

    void signalSaveSettingPro(bool &_result);


    void signalIICSaveSetting(uint iicAddress,bool &_result);

    void signalIICSaveSettingPro(uint iicAddress,bool &_result);

    void signalTransType(unsigned char _type, bool &_result);

    void signalUpgrade(const QString fullpath, unsigned int upgrade_frame_length);

    void signalGainAdjustEnabled(unsigned char _enabled, bool &_result);

    void signalCanSndId(unsigned int _id, bool &_result);

     void signalCanParamTFmini(int frameType,int baudRate,long canTxId,long canRxId, bool &_result);

    void signalCanRcvId(unsigned int _id, bool &_result);

    void signalCanBaudrate(unsigned int _baudrate, bool &_result);

    void signalVersion(unsigned char * _version, bool &_result);

    void signalVersionPro(unsigned char * _version, bool &_result);

    void signalFullVersion(unsigned char * _version, bool &_result);

    void signalFrameRate(unsigned int _fps, bool &_result);

    void signalIICFrameRate(uint iicAddress, unsigned int _fps, bool &_result);

    void signalFrameRatePro(unsigned int _fps, bool &_result);

    void signalIICFrameRatePro(uint iicAddress,unsigned int _fps, bool &_result);

    void signalSetWiperPeriodPro(uint periodMinutes, bool& result);

	void signalModbusSetWiperPeriodPro(uint addr, uint periodMinutes, bool& result);

    void signalSetWiperCyclePro(uint cycleCnt, bool& result);

	void signalModbusSetWiperCycle(uint addr,uint cycleCnt, bool& result);

    void signalStartWiperPro(bool& result);

	void signalModbusStartWiperPro(uint addr, bool& result);

    void signalCustom(unsigned char _custom, bool &_result);

    void signalCANFrameType(unsigned char _type, bool &_result);

    void signalIOThreshold(unsigned short _v1, unsigned short _v2, bool &_result);

    void signalIOValue(unsigned char _v, bool &_result);

    void signalIODelay(unsigned short _v1, unsigned short _v2, bool &_result);

    void signalFogFeatureEnabled(unsigned char _enabled, bool &_result);

    void signalOffset(unsigned short _ofs, bool &_result);

    void sigUpdateProcessbar(float);

    void signalUpgradeResult(bool);

    void signalMapTypeSet(unsigned char _enabled, bool &_result);

    void signal_mb_baudrage_set(unsigned char dev_addr, unsigned int baudrate, bool &result);

    void signal_mb_dev_addr_set(unsigned char dev_addr, unsigned char mb_dev_addr, bool &result);

    void signal_mb_get_dist(unsigned char dev_addr, unsigned short &dist, bool &result);

    bool signal_mb_getdistandsigal(uint devAddr, bool &result);

    void signal_mb_modbus_disable(unsigned char dev_addr, bool &result);

    void signal_mb_save_config(unsigned char dev_addr, bool &result);

    void signal_modbus_enable(bool &result);

    void signal_modbus_dev_addr_set(unsigned char dev_addr, bool &result);

    void signalLowConsimption(uint sampleRate, bool &result );

     void signalIICLowConsimption(uint iicAddress, uint sampleRate, bool &result );

    void signal_mb_get_signalquality(unsigned char dev_addr, unsigned short& signal, bool &result);


    void signal_getdistancemm(unsigned short& distance, bool &result);

    void signal_iic_querydistancecm(uint iicAddress,bool &result);

    void signal_iic_querydistancecm_pro(uint iicAddress,bool &result);

     void signal_iic_getdistancecm(uint iicAddress, unsigned short& distance, bool &result);

    void signal_getdistancecm_pro(unsigned short& distance, bool &result);

    void signal_iic_getdistancecm_pro(uint iicAddr, unsigned short& distance, bool &result);

    void signal_setiicaddr(uint iicaddr,uint iicNewAddress, bool &result);

    void signal_setiicaddr_pro(uint iicaddr,uint iicNewAddr, bool &result);

    void signal_uart_setio(uint ioMode, uint distVal, uint zoneVal, uint delayVal1, uint delayVal2, bool &result);

    void signal_uart_sendoneframe(QByteArray cmd,bool& result);


    void signal_analog_modeswitch(uint mode, bool &result);

    void signal_analog_setdistmin(uint mindist, bool &result);

    void signal_analog_setdistmax(uint maxdist, bool &result);


private slots:

    // 执行雷达指令

    void slot_do_cmd_id_soft_reset(bool &result);

    void slot_do_cmd_id_sample_trig(unsigned short &dist, bool &result);

    void slot_do_cmd_id_sample_freq(unsigned int freq, bool &result);

    void slot_do_cmd_id_output_format(unsigned char format, bool &result);

    void slot_do_cmd_id_baud_rate(unsigned int baudrate, bool &result);

     void slot_do_cmd_id_baud_rate_pro(unsigned int baudrate, bool &result);

    void slot_do_cmd_id_output_en(unsigned char is_enabled, bool &result);

    void slot_do_cmd_id_frame_checksum_en(unsigned char is_enabled, bool &result);

    void slot_do_cmd_id_restore_default(bool &result);

    void slot_do_cmd_id_save_settings(bool &result);

    void slot_do_cmd_id_trans_type_config(unsigned char trans, bool &result);

    void slot_do_cmd_id_upgrade(const QString fullpath, unsigned int upgrade_frame_length);

    void slot_do_cmd_id_gain_adjust_enable(unsigned char enable, bool &result);

    void slot_do_cmd_id_can_snd_id(unsigned int value, bool &result);

    void slot_do_cmd_id_can_rcv_id(unsigned int value, bool &result);

    void slot_do_cmd_id_can_baudrate(unsigned int value, bool &result);

    void slot_do_cmd_id_version(unsigned char *ba_version, bool &result);

    void slot_do_cmd_id_full_version(unsigned char *ba_version, bool &result);

    void slot_do_cmd_id_frame_rate(unsigned int fps, bool &result);

    void slot_do_cmd_id_frame_rate_pro(unsigned int fps, bool &result);

    void slot_do_cmd_id_Custom_choise(unsigned char custom, bool &result);

    void slot_do_cmd_id_can_frame_type_config(unsigned char type, bool &result);

    void slot_do_cmd_id_dist_io_threshold(unsigned short value1, unsigned short value2, bool &result);

    void slot_do_cmd_id_near_io_value(unsigned char value, bool &result);

    void slot_do_cmd_id_io_delay(unsigned short value1, unsigned short value2, bool &result);

    void slot_do_cmd_id_fog_feature_enable(unsigned char enable, bool &result);

    void slot_do_cmd_id_offset_config(unsigned short value, bool &result);

    void slot_do_cmd_id_dac_map_type(unsigned char type, bool &result);

    void slot_do_cmd_id_modbus_enable(bool &result);

    void slot_do_cmd_id_modbus_dev_addr_set(unsigned char addr, bool &result);

    void slot_do_cmd_id_mb_baudrate_config(unsigned char dev_addr, unsigned int baudrate, bool &result);

    void slot_do_cmd_id_mb_dev_addr_config(unsigned char dev_addr, unsigned char mb_dev_addr, bool &result);

    void slot_do_cmd_id_mb_get_dist(unsigned char dev_addr, unsigned short &dist, bool &result);

    void slot_do_cmd_id_mb_modbus_disable(unsigned char dev_addr, bool &result);

    void slot_do_cmd_id_mb_save_config(unsigned char dev_addr, bool &result);

    void slot_do_cmd_id_lowconsumption(uint sampleRate, bool &result);

    void slot_do_cmd_id_mb_get_signal(unsigned char dev_addr, unsigned short &signal, bool &result);

    //iic
    void slot_do_cmd_iic_setaddr(uint iic_addr,uint iic_newAddress, bool &result);





    void slot_do_cmd_id_version_pro(unsigned char *ba_version, bool &result);

    void slot_do_cmd_id_output_en_pro(unsigned char is_enabled, bool &result);

    void slot_do_cmd_id_output_format_pro(unsigned char format, bool &result);

    void slot_do_cmd_id_soft_reset_pro(bool &result);
    void slot_do_cmd_id_save_settings_pro(bool &result);
    void slot_do_cmd_id_restore_default_pro(bool &result);
    void slot_do_cmd_uart_setio(uint ioMode, uint distVal, uint zoneVal, uint delayVal1, uint delayVal2, bool &result);

    void slot_setwiperperiod_pro(uint wiperPeriod, bool &result);

	void slot_setwiperperiod_modbus(uint dev_addr, uint wiperPeriod, bool & result);

	void slot_setwipercycle_modbus(uint dev_addr, uint wiperPeriod, bool & result);


    void slot_setwipercycle_pro(uint wiperPeriod, bool &result);


	void slot_startwiper_modbus(uint dev_addr, bool & result);

	void slot_startwiper_pro(bool &result);


    void slot_iic_setaddr_pro(uint iic_addr,uint newiicAddr, bool &result);



    void slot_can_paramTFmini(int frameType, int baudRate, long canTxId, long canRxId, bool &_result);


    void slot_uart_sendoneframe(QByteArray cmd, bool &result);

    int serialPortWrite(QByteArray cmd);


    void slot_mb_get_distandsignal(uint dev_addr, bool &result);


    void slot_analog_modeswitch(uint mode, bool &result);

    void slot_analog_setdistmin(uint mindist, bool &result);

    void slot_analog_setdistmax(uint maxdist, bool &result);


    void slot_iic_getdistcm(uint iicAddress, unsigned short &dist, bool &result);


    void slot_iic_querydistancecm(uint iicAddress,bool &result);

    void slot_iic_frame_rate(uint iicAddress, unsigned int fps, bool &result);

    void slot_iic_output_format(uint iicAddress, unsigned char format, bool &result);
    void slot_iic_lowconsumption(uint iicAddress, uint sampleRate, bool &result);

    void slot_iic_save_settings(uint iicAddress, bool &result);
    void slot_iic_soft_reset(uint iicAddress, bool &result);
    void slot_iic_restore_default(uint nAddress, bool &result);


    void slot_iic_save_settings_pro(uint iicAddr, bool &result);

    void slot_iic_frame_rate_pro(uint iicAddress, unsigned int fps, bool &result);

    void slot_iic_output_format_pro(uint iicAddress, unsigned char format, bool &result);


    void slot_iic_getdist_9bytecm_pro(uint iicAddr, unsigned short &dist, bool &result);


    void slot_iic_querydistancecm_pro(uint iicAddress, bool &result);





public slots:

    void slotOpen(QString _serialSelected, int _baudRate, bool &_result);

    void slotClose(bool &_result);

    void slotReceiver();

    void slotCalcFreq();



protected slots:




};

#endif // RECEIVER_H

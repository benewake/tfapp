/********************************************************************************
** Form generated from reading UI file 'productselectdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRODUCTSELECTDIALOG_H
#define UI_PRODUCTSELECTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProductSelectDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QLabel *label;
    QComboBox *cbProductModal;
    QLabel *label_2;
    QComboBox *cbCommContract;
    QPushButton *btnCancel;
    QPushButton *btnOK;
    QPushButton *btnClose;

    void setupUi(QDialog *ProductSelectDialog)
    {
        if (ProductSelectDialog->objectName().isEmpty())
            ProductSelectDialog->setObjectName(QString::fromUtf8("ProductSelectDialog"));
        ProductSelectDialog->resize(616, 386);
        verticalLayout = new QVBoxLayout(ProductSelectDialog);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(ProductSelectDialog);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(70, 120, 54, 12));
        cbProductModal = new QComboBox(frame);
        cbProductModal->setObjectName(QString::fromUtf8("cbProductModal"));
        cbProductModal->setGeometry(QRect(130, 110, 131, 31));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(300, 120, 54, 12));
        cbCommContract = new QComboBox(frame);
        cbCommContract->setObjectName(QString::fromUtf8("cbCommContract"));
        cbCommContract->setGeometry(QRect(360, 110, 131, 31));
        btnCancel = new QPushButton(frame);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));
        btnCancel->setGeometry(QRect(160, 260, 101, 31));
        btnOK = new QPushButton(frame);
        btnOK->setObjectName(QString::fromUtf8("btnOK"));
        btnOK->setGeometry(QRect(350, 260, 101, 31));
        btnClose = new QPushButton(frame);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));
        btnClose->setGeometry(QRect(570, 10, 32, 32));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnClose->sizePolicy().hasHeightForWidth());
        btnClose->setSizePolicy(sizePolicy);
        btnClose->setMinimumSize(QSize(32, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnClose->setIcon(icon);
        btnClose->setIconSize(QSize(28, 28));
        btnClose->setFlat(true);

        verticalLayout->addWidget(frame);


        retranslateUi(ProductSelectDialog);

        QMetaObject::connectSlotsByName(ProductSelectDialog);
    } // setupUi

    void retranslateUi(QDialog *ProductSelectDialog)
    {
        ProductSelectDialog->setWindowTitle(QCoreApplication::translate("ProductSelectDialog", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("ProductSelectDialog", "\344\272\247\345\223\201\345\236\213\345\217\267", nullptr));
        label_2->setText(QCoreApplication::translate("ProductSelectDialog", "\346\216\245\345\217\243\345\215\217\350\256\256", nullptr));
        btnCancel->setText(QCoreApplication::translate("ProductSelectDialog", "\345\217\226\346\266\210", nullptr));
        btnOK->setText(QCoreApplication::translate("ProductSelectDialog", "\347\241\256\350\256\244", nullptr));
        btnClose->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ProductSelectDialog: public Ui_ProductSelectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRODUCTSELECTDIALOG_H

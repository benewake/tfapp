#include "widgetsteeringmodbus.h"
#include "ui_widgetsteeringmodbus.h"

#include <QMessageBox>

WidgetSteeringModbus::WidgetSteeringModbus(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetSteeringModbus)
{
    ui->setupUi(this);
}

WidgetSteeringModbus::~WidgetSteeringModbus()
{
    delete ui;
}

void WidgetSteeringModbus::SetUartRecerver(serialportio *tReceiver,QString productModal)
{
    this->sensorRecver_ = tReceiver;
    this->mProductModal = productModal;
}


bool WidgetSteeringModbus::CheckComState()
{
    if(sensorRecver_==nullptr)
        return false;

    if(!sensorRecver_->isOpen())
    {
        QMessageBox::warning(NULL, "Error", tr(u8"未连接Com口"));
        return false;
    }

    return true;
}

void WidgetSteeringModbus::on_btnModbusEnable_clicked()
{
    if(!CheckComState()) return;
    bool isEnable = true;
    if(!ui->cbModbusContract->currentIndex()==0){
        if(sensorRecver_->send_cmd_id_modbus_enable())
        {
          QMessageBox::information(NULL, tr(u8"提示"), tr(u8"modbus启用成功！"));
        }else
        {
         QMessageBox::critical(NULL, tr(u8"提示"), tr(u8"modbus启用失败！"));
        }
    }else
    {
        //关闭的时候是modbus协议

        if(sensorRecver_->send_cmd_id_mb_modbus_disable(0x01))
        {
          QMessageBox::information(NULL, tr(u8"成功"), tr(u8"modbus启用成功！"));
        }else
        {
         QMessageBox::critical(NULL, tr(u8"失败"), tr(u8"modbus启用失败！"));
        }
    }
}

void WidgetSteeringModbus::on_btnModbusAddress_clicked()
{
    if(!CheckComState()) return;
    uint modbusAddress = ui->etAddress->text().toUShort();
    uint devAddress = 0x01;
    if(sensorRecver_->send_cmd_id_mb_dev_addr_config(devAddress,modbusAddress))
    {
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"modbus地址设置成功！"));
    }else
    {
     QMessageBox::critical(NULL, tr(u8"提示"), tr(u8"modbus地址设置失败！"));
    }
}

void WidgetSteeringModbus::on_btnBaudrate_clicked()
{
    if(!CheckComState()) return;

    uint Bauadrate = ui->cbBaudrate->currentText().toUInt();

	uint modbusAddress = ui->etAddress->text().toUShort();

    if(sensorRecver_->send_cmd_id_mb_baudrate_config(modbusAddress, Bauadrate))
    {
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"modbus波特率设置成功！"));
    }else
    {
     QMessageBox::critical(NULL, tr(u8"提示"), tr(u8"modbus波特率设置失败！"));
    }
}

void WidgetSteeringModbus::on_btnOutFreq_clicked()
{
    if(!CheckComState()) return;
    ushort fps = ui->cbFrameFreq->currentText().toUShort();
	uint modbusAddress = ui->etAddress->text().toUShort();
    if(sensorRecver_->send_cmd_id_modbus_set_fps(modbusAddress, fps))
    {
      QMessageBox::information(NULL, tr(u8"提示"), tr(u8"modbus帧率设置成功！"));
    }else
    {
     QMessageBox::critical(NULL, tr(u8"提示"), tr(u8"modbus帧率设置失败！"));
    }
}

void WidgetSteeringModbus::on_btnGetDistance_clicked()
{
    if(!CheckComState()) return;
    unsigned short dist;
	uint modbusAddress = ui->etAddress->text().toUShort();
    if(sensorRecver_->send_cmd_id_mb_get_dist(modbusAddress, dist))
    {
        ui->lbCurDistance->setText(tr(u8"当前距离")+QString::number(dist));
    }else{
         ui->lbCurDistance->setText(tr(u8"距离获取失败"));
    }
}

void WidgetSteeringModbus::on_btnSignalQuality_clicked()
{
    if(!CheckComState()) return;
    unsigned short signal;
	uint modbusAddress = ui->etAddress->text().toUShort();
    if(sensorRecver_->send_cmd_id_modbus_getsignal(modbusAddress,signal))
    {
         ui->lbCurSignal->setText(tr(u8"信号强度:")+signal);
    }else{
         ui->lbCurSignal->setText(tr(u8"信号获取失败"));
    }
}

void WidgetSteeringModbus::on_btnSaveConfig_clicked()
{
    if(!CheckComState()) return;
     unsigned short dist;
	 uint modbusAddress = ui->etAddress->text().toUShort();
     if(sensorRecver_->send_cmd_id_mb_save_config(modbusAddress))
     {
         ui->lbSaveConfig->setText(tr(u8"配置保存成功"));
     }else{
          ui->lbSaveConfig->setText(tr(u8"配置保存失败"));
     }
}

void WidgetSteeringModbus::on_btnWiperPeriod_clicked()
{
    if(!CheckComState()) return;

    uint wiperPeriod = ui->etWiperPeriod->text().toUInt();

    if(ui->cbWiperPeriodUnit->currentIndex()==1)
    {
          wiperPeriod = 60 * wiperPeriod;
    }

	uint modbusAddress = ui->etAddress->text().toUShort();

    if(sensorRecver_->send_mobus_set_wiper_peroid(modbusAddress,wiperPeriod))
    {
       qDebug("雨刷周期设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷周期设定成功"));

    }else{
        qDebug("雨刷周期设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷周期设定失败"));
    }
}

void WidgetSteeringModbus::on_btnRoundtripCnt_clicked()
{
    if(!CheckComState()) return;

    uint cycleCnt = ui->etCycleCnt->text().toUInt();
	uint modbusAddress = ui->etAddress->text().toUShort();

    if(cycleCnt>10)
    {
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷往返次数不能超过10次！"));
       return;
    }

    if(sensorRecver_->send_modbus_set_wipercycle(modbusAddress,cycleCnt))
    {
       qDebug("雨刷往返次数设定成功！");
       QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷往返次数设定成功！"));

    }else{
        qDebug("雨刷往返次数设定失败！");
        QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷往返次数设定失败！"));
    }
}

void WidgetSteeringModbus::on_btnStartWiper_clicked()
{
    if(!CheckComState()) return;
	uint modbusAddress = ui->etAddress->text().toUShort();
    if(sensorRecver_->send_modbus_startwiper(modbusAddress))
    {
       qDebug("雨刷启动成功！");
       ui->lbStartWiper->setText(tr(u8"雨刷启动成功！"));
       //QMessageBox::information(NULL, tr(u8"提示"), tr(u8"雨刷启动成功！"));

    }else{
        qDebug("雨刷启动失败！");
         ui->lbStartWiper->setText(tr(u8"雨刷启动失败！"));
       // QMessageBox::warning(NULL, tr(u8"提示"), tr(u8"雨刷启动失败！"));
    }
}

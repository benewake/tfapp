#ifndef WIDGET420_H
#define WIDGET420_H

#include <QWidget>

#include "serialportio.h"



namespace Ui {
class Widget420;
}

class Widget420 : public QWidget
{
    Q_OBJECT

private:
    void InitDatas();
public:
    explicit Widget420(QWidget *parent = nullptr);
    ~Widget420();
    serialportio *sensorRecver_;

    QString mProductModal;


//将控件暴露出来，供上层窗体使用
    Ui::Widget420 *ui;
    bool CheckComState();
    void SetUartRecerver(serialportio* tReceiver,QString productModal);

private slots:
    void on_btnBaudrate_clicked();
    void on_btnGetVersion_clicked();
    void on_btnFrameFreq_clicked();
    void on_btnOutSwitch_clicked();
    void on_btnSaveConfig_clicked();
    void on_btnSystemReset_clicked();
    void on_btnLowConsumption_clicked();
    void on_btnFallbackToFactory_clicked();
    void on_btnOutputFormat_clicked();

    void on_btnSwitchMode_clicked();
    void on_btnMinValSet_clicked();
    void on_btnMaxValSet_clicked();
};

#endif // WIDGET420_H

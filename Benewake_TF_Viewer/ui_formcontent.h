/********************************************************************************
** Form generated from reading UI file 'formcontent.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMCONTENT_H
#define UI_FORMCONTENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FormContent
{
public:
    QGridLayout *gridLayout;
    QWidget *widgetLeft;
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QComboBox *cbComm;
    QLabel *lbComTitle;
    QLabel *label_2;
    QComboBox *cbBaudrate;
    QPushButton *btnConnTF;
    QLabel *label_3;
    QComboBox *cbProductModal;
    QLabel *label_4;
    QComboBox *cbCommContract;
    QLabel *lbCANChan;
    QComboBox *cbCanChan;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *layoutFuncPanel;
    QGroupBox *groupRecord;
    QLabel *label_8;
    QPushButton *btnSave;
    QPushButton *btnFileSel;
    QRadioButton *rbHex;
    QRadioButton *rbAscii;
    QCheckBox *ckTimeStamp;
    QLineEdit *etFileName;
    QGroupBox *groupBox;
    QLabel *label_9;
    QLabel *lbProgress;
    QPushButton *btnOpenFirmware;
    QPushButton *btnUpgrade;
    QCheckBox *ckBridge;
    QLabel *label_11;
    QLineEdit *etPath;
    QWidget *widget_2;
    QGridLayout *gridLayout_2;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lbDistance;
    QLabel *lbSignal;
    QLabel *lbFreq;
    QRadioButton *rbCN;
    QRadioButton *rbEN;
    QWidget *widget_4;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *layoutChart;
    QWidget *widget_5;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_4;
    QListWidget *listRxCmd;
    QWidget *widget_6;
    QLineEdit *etTxCmd;
    QLabel *label_5;
    QPushButton *btnSend;

    void setupUi(QWidget *FormContent)
    {
        if (FormContent->objectName().isEmpty())
            FormContent->setObjectName(QString::fromUtf8("FormContent"));
        FormContent->resize(1135, 865);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FormContent->sizePolicy().hasHeightForWidth());
        FormContent->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setPointSize(9);
        FormContent->setFont(font);
        gridLayout = new QGridLayout(FormContent);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(9, 9, 9, 9);
        widgetLeft = new QWidget(FormContent);
        widgetLeft->setObjectName(QString::fromUtf8("widgetLeft"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widgetLeft->sizePolicy().hasHeightForWidth());
        widgetLeft->setSizePolicy(sizePolicy1);
        widgetLeft->setMinimumSize(QSize(400, 600));
        verticalLayout = new QVBoxLayout(widgetLeft);
        verticalLayout->setSpacing(5);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(5, 2, 5, 5);
        widget = new QWidget(widgetLeft);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy2);
        widget->setMinimumSize(QSize(0, 120));
        cbComm = new QComboBox(widget);
        cbComm->setObjectName(QString::fromUtf8("cbComm"));
        cbComm->setGeometry(QRect(10, 20, 111, 31));
        lbComTitle = new QLabel(widget);
        lbComTitle->setObjectName(QString::fromUtf8("lbComTitle"));
        lbComTitle->setGeometry(QRect(10, 0, 111, 16));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(140, 0, 121, 16));
        cbBaudrate = new QComboBox(widget);
        cbBaudrate->setObjectName(QString::fromUtf8("cbBaudrate"));
        cbBaudrate->setGeometry(QRect(140, 20, 121, 31));
        btnConnTF = new QPushButton(widget);
        btnConnTF->setObjectName(QString::fromUtf8("btnConnTF"));
        btnConnTF->setGeometry(QRect(280, 20, 101, 31));
        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(140, 60, 121, 16));
        cbProductModal = new QComboBox(widget);
        cbProductModal->setObjectName(QString::fromUtf8("cbProductModal"));
        cbProductModal->setGeometry(QRect(10, 80, 111, 31));
        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 60, 111, 16));
        cbCommContract = new QComboBox(widget);
        cbCommContract->setObjectName(QString::fromUtf8("cbCommContract"));
        cbCommContract->setGeometry(QRect(140, 80, 121, 31));
        lbCANChan = new QLabel(widget);
        lbCANChan->setObjectName(QString::fromUtf8("lbCANChan"));
        lbCANChan->setGeometry(QRect(280, 60, 91, 16));
        cbCanChan = new QComboBox(widget);
        cbCanChan->addItem(QString());
        cbCanChan->addItem(QString());
        cbCanChan->setObjectName(QString::fromUtf8("cbCanChan"));
        cbCanChan->setGeometry(QRect(280, 80, 101, 31));

        verticalLayout->addWidget(widget);

        groupBox_2 = new QGroupBox(widgetLeft);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy3);
        groupBox_2->setMinimumSize(QSize(0, 400));
        groupBox_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, 0, -1, -1);
        layoutFuncPanel = new QVBoxLayout();
        layoutFuncPanel->setObjectName(QString::fromUtf8("layoutFuncPanel"));

        verticalLayout_2->addLayout(layoutFuncPanel);


        verticalLayout->addWidget(groupBox_2);

        groupRecord = new QGroupBox(widgetLeft);
        groupRecord->setObjectName(QString::fromUtf8("groupRecord"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(groupRecord->sizePolicy().hasHeightForWidth());
        groupRecord->setSizePolicy(sizePolicy4);
        groupRecord->setMinimumSize(QSize(0, 150));
        label_8 = new QLabel(groupRecord);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 40, 91, 16));
        btnSave = new QPushButton(groupRecord);
        btnSave->setObjectName(QString::fromUtf8("btnSave"));
        btnSave->setGeometry(QRect(30, 70, 131, 31));
        btnFileSel = new QPushButton(groupRecord);
        btnFileSel->setObjectName(QString::fromUtf8("btnFileSel"));
        btnFileSel->setGeometry(QRect(190, 70, 131, 31));
        rbHex = new QRadioButton(groupRecord);
        rbHex->setObjectName(QString::fromUtf8("rbHex"));
        rbHex->setGeometry(QRect(30, 120, 51, 16));
        rbHex->setChecked(true);
        rbAscii = new QRadioButton(groupRecord);
        rbAscii->setObjectName(QString::fromUtf8("rbAscii"));
        rbAscii->setGeometry(QRect(110, 120, 61, 21));
        ckTimeStamp = new QCheckBox(groupRecord);
        ckTimeStamp->setObjectName(QString::fromUtf8("ckTimeStamp"));
        ckTimeStamp->setGeometry(QRect(200, 120, 121, 21));
        etFileName = new QLineEdit(groupRecord);
        etFileName->setObjectName(QString::fromUtf8("etFileName"));
        etFileName->setGeometry(QRect(110, 30, 271, 30));
        sizePolicy4.setHeightForWidth(etFileName->sizePolicy().hasHeightForWidth());
        etFileName->setSizePolicy(sizePolicy4);
        etFileName->setMinimumSize(QSize(0, 30));
        etFileName->setMaximumSize(QSize(300, 30));

        verticalLayout->addWidget(groupRecord);

        groupBox = new QGroupBox(widgetLeft);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy4.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy4);
        groupBox->setMinimumSize(QSize(0, 150));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(13, 25, 71, 21));
        lbProgress = new QLabel(groupBox);
        lbProgress->setObjectName(QString::fromUtf8("lbProgress"));
        lbProgress->setGeometry(QRect(290, 25, 81, 21));
        btnOpenFirmware = new QPushButton(groupBox);
        btnOpenFirmware->setObjectName(QString::fromUtf8("btnOpenFirmware"));
        btnOpenFirmware->setGeometry(QRect(30, 70, 131, 31));
        btnUpgrade = new QPushButton(groupBox);
        btnUpgrade->setObjectName(QString::fromUtf8("btnUpgrade"));
        btnUpgrade->setGeometry(QRect(190, 70, 81, 31));
        ckBridge = new QCheckBox(groupBox);
        ckBridge->setObjectName(QString::fromUtf8("ckBridge"));
        ckBridge->setGeometry(QRect(290, 70, 71, 31));
        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(30, 110, 341, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Source Han Sans Normal"));
        font1.setPointSize(8);
        label_11->setFont(font1);
        etPath = new QLineEdit(groupBox);
        etPath->setObjectName(QString::fromUtf8("etPath"));
        etPath->setGeometry(QRect(100, 20, 181, 30));
        etPath->setMinimumSize(QSize(0, 30));

        verticalLayout->addWidget(groupBox);


        gridLayout->addWidget(widgetLeft, 0, 0, 1, 1);

        widget_2 = new QWidget(FormContent);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\346\200\235\346\272\220\351\273\221\344\275\223"));
        font2.setPointSize(10);
        widget_2->setFont(font2);
        gridLayout_2 = new QGridLayout(widget_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        widget_3 = new QWidget(widget_2);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        sizePolicy4.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy4);
        widget_3->setMinimumSize(QSize(0, 40));
        horizontalLayout_3 = new QHBoxLayout(widget_3);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lbDistance = new QLabel(widget_3);
        lbDistance->setObjectName(QString::fromUtf8("lbDistance"));
        lbDistance->setFont(font);

        horizontalLayout_3->addWidget(lbDistance);

        lbSignal = new QLabel(widget_3);
        lbSignal->setObjectName(QString::fromUtf8("lbSignal"));
        lbSignal->setFont(font);

        horizontalLayout_3->addWidget(lbSignal);

        lbFreq = new QLabel(widget_3);
        lbFreq->setObjectName(QString::fromUtf8("lbFreq"));
        lbFreq->setFont(font);

        horizontalLayout_3->addWidget(lbFreq);

        rbCN = new QRadioButton(widget_3);
        rbCN->setObjectName(QString::fromUtf8("rbCN"));
        QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(rbCN->sizePolicy().hasHeightForWidth());
        rbCN->setSizePolicy(sizePolicy5);
        rbCN->setFont(font);
        rbCN->setLayoutDirection(Qt::RightToLeft);
        rbCN->setChecked(true);

        horizontalLayout_3->addWidget(rbCN);

        rbEN = new QRadioButton(widget_3);
        rbEN->setObjectName(QString::fromUtf8("rbEN"));
        sizePolicy5.setHeightForWidth(rbEN->sizePolicy().hasHeightForWidth());
        rbEN->setSizePolicy(sizePolicy5);
        rbEN->setFont(font);
        rbEN->setLayoutDirection(Qt::RightToLeft);

        horizontalLayout_3->addWidget(rbEN);


        gridLayout_2->addWidget(widget_3, 0, 0, 1, 1);

        widget_4 = new QWidget(widget_2);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        sizePolicy3.setHeightForWidth(widget_4->sizePolicy().hasHeightForWidth());
        widget_4->setSizePolicy(sizePolicy3);
        verticalLayout_6 = new QVBoxLayout(widget_4);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        layoutChart = new QVBoxLayout();
        layoutChart->setObjectName(QString::fromUtf8("layoutChart"));

        verticalLayout_6->addLayout(layoutChart);


        gridLayout_2->addWidget(widget_4, 1, 0, 1, 1);

        widget_5 = new QWidget(widget_2);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        sizePolicy4.setHeightForWidth(widget_5->sizePolicy().hasHeightForWidth());
        widget_5->setSizePolicy(sizePolicy4);
        widget_5->setMinimumSize(QSize(0, 200));
        horizontalLayout = new QHBoxLayout(widget_5);
        horizontalLayout->setSpacing(9);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(9, 9, 9, 9);
        groupBox_4 = new QGroupBox(widget_5);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy6);
        groupBox_4->setMinimumSize(QSize(0, 200));
        groupBox_4->setFont(font);
        groupBox_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        horizontalLayout_4 = new QHBoxLayout(groupBox_4);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        listRxCmd = new QListWidget(groupBox_4);
        listRxCmd->setObjectName(QString::fromUtf8("listRxCmd"));
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(listRxCmd->sizePolicy().hasHeightForWidth());
        listRxCmd->setSizePolicy(sizePolicy7);
        listRxCmd->setMinimumSize(QSize(0, 100));

        horizontalLayout_4->addWidget(listRxCmd);

        widget_6 = new QWidget(groupBox_4);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        QSizePolicy sizePolicy8(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(widget_6->sizePolicy().hasHeightForWidth());
        widget_6->setSizePolicy(sizePolicy8);
        widget_6->setMinimumSize(QSize(300, 0));
        etTxCmd = new QLineEdit(widget_6);
        etTxCmd->setObjectName(QString::fromUtf8("etTxCmd"));
        etTxCmd->setGeometry(QRect(10, 30, 281, 31));
        label_5 = new QLabel(widget_6);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 10, 281, 16));
        btnSend = new QPushButton(widget_6);
        btnSend->setObjectName(QString::fromUtf8("btnSend"));
        btnSend->setGeometry(QRect(170, 80, 111, 31));

        horizontalLayout_4->addWidget(widget_6);


        horizontalLayout->addWidget(groupBox_4);


        gridLayout_2->addWidget(widget_5, 2, 0, 1, 1);


        gridLayout->addWidget(widget_2, 0, 1, 1, 1);


        retranslateUi(FormContent);

        QMetaObject::connectSlotsByName(FormContent);
    } // setupUi

    void retranslateUi(QWidget *FormContent)
    {
        FormContent->setWindowTitle(QCoreApplication::translate("FormContent", "Form", nullptr));
        lbComTitle->setText(QCoreApplication::translate("FormContent", "\351\200\232\344\277\241\347\253\257\345\217\243", nullptr));
        label_2->setText(QCoreApplication::translate("FormContent", "\346\263\242\347\211\271\347\216\207", nullptr));
        btnConnTF->setText(QCoreApplication::translate("FormContent", "\350\277\236\346\216\245", nullptr));
        label_3->setText(QCoreApplication::translate("FormContent", "\346\216\245\345\217\243\345\215\217\350\256\256", nullptr));
        label_4->setText(QCoreApplication::translate("FormContent", "\344\272\247\345\223\201\345\236\213\345\217\267", nullptr));
        lbCANChan->setText(QCoreApplication::translate("FormContent", "CAN\351\200\232\351\201\223", nullptr));
        cbCanChan->setItemText(0, QCoreApplication::translate("FormContent", "CAN1", nullptr));
        cbCanChan->setItemText(1, QCoreApplication::translate("FormContent", "CAN2", nullptr));

        groupBox_2->setTitle(QCoreApplication::translate("FormContent", "\345\217\202\346\225\260\350\256\276\347\275\256\345\214\272", nullptr));
        groupRecord->setTitle(QCoreApplication::translate("FormContent", "\346\225\260\346\215\256\351\207\207\345\275\225\345\214\272", nullptr));
        label_8->setText(QCoreApplication::translate("FormContent", "\346\226\207\344\273\266\345\220\215\347\247\260\357\274\232", nullptr));
        btnSave->setText(QCoreApplication::translate("FormContent", "\350\256\260\345\275\225", nullptr));
        btnFileSel->setText(QCoreApplication::translate("FormContent", "\351\200\211\346\213\251\346\226\207\344\273\266", nullptr));
        rbHex->setText(QCoreApplication::translate("FormContent", "Hex", nullptr));
        rbAscii->setText(QCoreApplication::translate("FormContent", "ASCII", nullptr));
        ckTimeStamp->setText(QCoreApplication::translate("FormContent", "\346\227\266\351\227\264\346\210\263", nullptr));
        groupBox->setTitle(QCoreApplication::translate("FormContent", "\345\233\272\344\273\266\345\215\207\347\272\247\345\214\272", nullptr));
        label_9->setText(QCoreApplication::translate("FormContent", "\346\226\207\344\273\266\350\267\257\345\276\204\357\274\232", nullptr));
        lbProgress->setText(QCoreApplication::translate("FormContent", "\345\267\262\345\256\214\346\210\2200%", nullptr));
        btnOpenFirmware->setText(QCoreApplication::translate("FormContent", "\346\211\223\345\274\200\346\234\200\346\226\260\347\211\210\345\233\272\344\273\266", nullptr));
        btnUpgrade->setText(QCoreApplication::translate("FormContent", "\345\215\207\347\272\247", nullptr));
        ckBridge->setText(QCoreApplication::translate("FormContent", "Bridge", nullptr));
        label_11->setText(QCoreApplication::translate("FormContent", "\346\263\250\346\204\217\357\274\232\350\257\267\345\260\206\345\233\272\344\273\266\346\226\207\344\273\266\344\277\235\345\255\230\350\207\263\345\205\250\350\213\261\346\226\207\350\267\257\345\276\204\344\270\213", nullptr));
        lbDistance->setText(QCoreApplication::translate("FormContent", "\346\265\213\351\207\217\350\267\235\347\246\273 \357\274\232", nullptr));
        lbSignal->setText(QCoreApplication::translate("FormContent", "\344\277\241\345\217\267\345\274\272\345\272\246 \357\274\232", nullptr));
        lbFreq->setText(QCoreApplication::translate("FormContent", "\351\242\221\347\216\207 \357\274\232", nullptr));
        rbCN->setText(QCoreApplication::translate("FormContent", "\344\270\255\346\226\207", nullptr));
        rbEN->setText(QCoreApplication::translate("FormContent", "English", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("FormContent", "\351\200\232\344\277\241\350\256\260\345\275\225\345\214\272", nullptr));
        label_5->setText(QCoreApplication::translate("FormContent", "\350\257\267\350\276\223\345\205\245\351\233\267\350\276\276\346\214\207\344\273\244", nullptr));
        btnSend->setText(QCoreApplication::translate("FormContent", "\345\217\221\351\200\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FormContent: public Ui_FormContent {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMCONTENT_H

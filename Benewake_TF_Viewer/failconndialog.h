#ifndef FAILCONNDIALOG_H
#define FAILCONNDIALOG_H

#include <QDialog>

namespace Ui {
class FailConnDialog;
}

class FailConnDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FailConnDialog(QWidget *parent = nullptr);
    ~FailConnDialog();

private:
    Ui::FailConnDialog *ui;

private slots:

    void on_btnClose_clicked();



};

#endif // DIALOG_H
